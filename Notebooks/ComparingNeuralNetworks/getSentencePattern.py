from pycorenlp import StanfordCoreNLP
from collections import defaultdict
from collections import Counter
import json


PATTERN_FILE = 'in-sent.filtered.tsv'
DATA_FILE = 'practice_data_3CTask.tsv'

corenlp = StanfordCoreNLP('http://localhost:12345')


# -----------------------------------------------


CUR_LABEL = None

def process_sent(text, get_deps=False):
    output = None
    for i in range(1, 5):
        try:
            output = json.loads(corenlp.annotate(text, properties={'annotators': 'tokenize,ssplit,pos,lemma,depparse' }), strict=False)
            break
        except ValueError:
            pass

    if output is None:
        if get_deps:
            return [], {}
        else:
            return []
    
    sentences = output['sentences']

    # In the event we see multiple sentences
    if len(sentences) > 1:
        pass
        #print 'SAW MULTI-SENTENCE: ', text
        #for sent in sentences:
            #print '\t', to_str(sent['tokens'])
        #sentences = sentences[:1]
        #raise BaseException('ughh too many sentences???')
    sentence = sentences[0]
    # this is what we really want
    tokens = sentence['tokens']

    for token in tokens:
        # Fix the issues with possessives, which get assigned to their noun form
        if token['word'] == 'our':
            token['lemma'] = 'our';
        elif token['word'] == 'my':
            token['lemma'] = 'my';
        elif token['word'] == 'their':
            token['lemma'] = 'their';
        elif token['word'] == 'his':
            token['lemma'] = 'his';
        elif token['word'] == 'her':
            token['lemma'] = 'her';
        # Also fix issues with parens...
        elif token['word'] == '-LRB-':
            token['word'] = '('
        elif token['word'] == '-RRB-':
            token['word'] = ')'
        elif token['word'] == '-LSB-':
            token['word'] = '['
        elif token['word'] == '-RSB-':
            token['word'] = ']'



    # Process the dependencies to find the arguments
    deps = sentence['basic-dependencies']
    for dep in deps:
        #print '%s :: %s -> %s' % (dep['dep'], tokens[dep['governor']-1]['word'], tokens[dep['dependent']-1]['word'])

        if dep['dep'] == 'nsubj':
            tokens[dep['dependent']-1]['ArgType'] = 'subj'
            tokens[dep['governor']-1]['ArgType']  = 'verb'
        elif dep['dep'] == 'dobj':
            tokens[dep['dependent']-1]['ArgType'] = 'dobj'            
        else:
            tokens[dep['dependent']-1]['ArgType'] = None

    #print json.dumps(tokens)

    
    # Break the sentence into clausal regions (dependencies clauses, etc)
    #print '\n', text
    segment(tokens, deps)

    if get_deps:
        return tokens, deps
    else:
        return tokens


def segment(tokens, dep_tree):
    tree = {}

    # Build the tree
    for dep in dep_tree:
        if dep['dep'] == 'ROOT':
            tree['ROOT'] = []
            tree['ROOT'].append(dep['dependent'])
            if dep['dependent'] not in tree:
                tree[dep['dependent']] = []
        else:
            gov_id = dep['governor']
            dep_id = dep['dependent']
            if dep['dep'] == 'punct':
                if dep_id == len(tokens):
                    continue

            if gov_id not in tree:
                tree[gov_id] = []
            if dep_id not in tree:
                tree[dep_id] = []

            tree[gov_id].append(dep_id)
        

    # Find the clausal roots of the tree
    roots = []        
    for dep in dep_tree:
        dep_type = dep['dep']
        if dep_type == 'ROOT' or dep_type == 'advcl' \
                or dep_type == 'ccomp' or dep_type.startswith('acl:') \
                or dep_type == 'list' or dep_type == 'parataxis':
            # Check for the length in case this has no dependent ?!?!
            dep_id = dep['dependent']
            #print '%s -> %s' % (dep_type, str(dep_id))
            if isinstance(dep_id, int):
                roots.append(dep_id)
        # Look for coordinating phrases linked by a verb
        elif dep_type == 'conj' or dep_type == 'cc':
            gov_id = dep['governor']
            dep_id = dep['dependent']
            
            if (tokens[dep_id-1]['pos'][0] == 'V' or tokens[dep_id-1]['pos'] == 'IN') \
                    and (tokens[gov_id-1]['pos'][0] == 'V' \
                             or tokens[gov_id-1]['pos'] == 'IN'):
                #print 'case2: ', dep['dependent']
                #print '%s -> %s' % (dep_type, str(dep['dependent']))
                roots.append(dep['dependent'])
    
    # Walk the tree from the roots, following each link
    #print 'roots: ', roots
    for root in roots:

        #print 'walking %s' % (str(root))
        children = []
        # Sometimes ARC papers have horrifically long sentences, which are not
        # actually from research papers (e.g., the proceedings introduction).
        # In this case, we just skip them and say the span is the entire
        # sentence
        if len(tokens) >= 500:

            span = (0, len(tokens))
            for t in tokens:
                t['segment_span'] = span

            continue

        walk(tree, root, children, roots)
        children.append(root)
        children.sort()          

        if len(children) == 0:
            continue
        
        #print 'Root %d had children: %s' % (root, str(children))

        span = (children[0]-1, children[-1])
        
        for i in range(children[0]-1, children[-1]):
            tokens[i]['segment_span'] = span

        # Find the root and see if it has a Modal
        root_id = root

        # Mark the verb tense
        if tokens[root_id-1]['pos'][0] == 'V':
            tokens[root_id-1]['is_root'] = True

        if tokens[root_id-1]['pos'][0] == 'V':
            # Use the POS tag tenses
            tokens[root_id-1]['tense'] = tokens[root_id-1]['pos'][-1]

        for dep in dep_tree:
            if dep['dep'] == 'aux' and dep['governor'] == root_id:
                tokens[root_id-1]['has_aux'] = True
            if dep['dep'] == 'auxpass' and dep['governor'] == root_id:
                tokens[root_id-1]['is_pass'] = True

        


def walk(tree, cur, seen, exclude):
    for child in tree[cur]:
        if child in exclude:
            continue
        seen.append(child)
        walk(tree, child, seen, exclude)


def get_custom_pattern_features(citance, pre_sents, post_sents, cite_index, debug=None, prefix=None):
    features = Counter() # {}
    global CUR_LABEL
    global FEATURE_FIRING_COUNTS

    #
    # Get the features for the citance
    #
    for (feature_name, patterns) in CUSTOM_IN_CITANCE_PATTERNS.items():
        if prefix is not None:
            feature_name = prefix + feature_name
        for pattern in patterns:
            pat_index = find(pattern, citance, None, debug=debug, feature=feature_name)
            if pat_index < 0:
                continue
            if debug is not None:
                print('found %s in %s' % (pattern, debug))

            # If the pattern happens after the citation
            if cite_index < pat_index:
                offset = pat_index - cite_index
            # Otherwise, it happens before, so take into account its length
            else:
                offset = (pat_index-len(pattern)) - cite_index                

            features[feature_name] += 1
            if CUR_LABEL is not None:
                FEATURE_FIRING_COUNTS[(' '.join(pattern), feature_name, CUR_LABEL.split("-")[0])] += 1

    for pre_sent in pre_sents:
        for (feature_name, patterns) in CUSTOM_PRE_CITANCE_PATTERNS.items():
            if prefix is not None:
                feature_name = prefix + feature_name
            for pattern in patterns:
                pat_index = find(pattern, pre_sent, None, debug=debug, feature=feature_name)
                if pat_index < 0:
                    continue
                if debug is not None:
                    print('found %s in %s' % (pattern, debug))

                # If the pattern happens after the citation
                if cite_index < pat_index:
                    offset = pat_index - cite_index
            # Otherwise, it happens before, so take into account its length
                else:
                    offset = (pat_index-len(pattern)) - cite_index                

                features[feature_name] += 1
                if CUR_LABEL is not None:
                    FEATURE_FIRING_COUNTS[(' '.join(pattern), feature_name, CUR_LABEL.split("-")[0])] += 1
                    

    for post_sent in post_sents:
        for (feature_name, patterns) in CUSTOM_POST_CITANCE_PATTERNS.items():
            if prefix is not None:
                feature_name = prefix + feature_name
            for pattern in patterns:
                pat_index = find(pattern, post_sent, None, debug=debug, feature=feature_name)
                if pat_index < 0:
                    continue
                if debug is not None:
                    print('found %s in %s' % (pattern, debug))

                # If the pattern happens after the citation
                if cite_index < pat_index:
                    offset = pat_index - cite_index
            # Otherwise, it happens before, so take into account its length
                else:
                    offset = (pat_index-len(pattern)) - cite_index                

                features[feature_name] += 1
                if CUR_LABEL is not None:
                    FEATURE_FIRING_COUNTS[(' '.join(pattern), feature_name, CUR_LABEL.split("-")[0])] += 1




    return features

def load_patterns(filename, p_dict, label):
    with open(filename) as f:
        class_counts = Counter()
        for line in f:
            # if not '@' in line:
            #     continue
            cols = line.split("\t")
            pattern = cols[0].replace("-lrb-", "(").replace('-rrb-', ')')
            clazz = cols[1]
            # if clazz == 'Background':
            #     continue
            class_counts[clazz] += 1
            p_dict[clazz + '_' + label + '_' + str(class_counts[clazz])] \
                = [pattern.split()]
            #p_dict[clazz + '_' + label].append(pattern.split())

CUSTOM_IN_CITANCE_PATTERNS   = defaultdict(list)
CUSTOM_PRE_CITANCE_PATTERNS  = defaultdict(list)
CUSTOM_POST_CITANCE_PATTERNS = defaultdict(list)

#load_patterns('pattern-type-counts.in-citance.filtered.tsv', \
#                  CUSTOM_IN_CITANCE_PATTERNS, 'in_cite')
#load_patterns('pattern-type-counts.preceding.filtered.tsv', \
#                  CUSTOM_PRE_CITANCE_PATTERNS, 'pre_cite')
#load_patterns('pattern-type-counts.following.filtered.tsv', \
#                  CUSTOM_POST_CITANCE_PATTERNS, 'post_cite')

load_patterns(PATTERN_FILE, \
                  CUSTOM_IN_CITANCE_PATTERNS, 'in_cite')
# load_patterns('resources/patterns/before.filtered.tsv', \
#                   CUSTOM_PRE_CITANCE_PATTERNS, 'pre_cite')
# load_patterns('resources/patterns/after.filtered.tsv', \
#                   CUSTOM_POST_CITANCE_PATTERNS, 'post_cite')

def find(pattern, sentence, must_have_subj_value, debug=None, feature=None):
    pat_len = len(pattern)

    #if debug is not None:
    #    print json.dumps(sentence)

    if debug is not None:
        print('\n\ntesting %s (%s) against "%s" (must be subj? %s)' % (pattern, feature, debug, must_have_subj_value))



    # For each position in the sentence
    for i in range(0, (len(sentence) - pat_len) + 1):
        
        match = True
        is_subj = False        
        # This is the adjustment to the sentence's token offset based on finding
        # a MWE match in a lexicon
        k = 0 


        if debug is not None:
            print('starting search at ' + sentence[i]['word'])

        for j in range(0, pat_len):

            if debug is not None:
                print('%d:%d:%d -> "%s" in "%s"?' % (i, j, k, sentence[i+j+k]['lemma'], pattern[j]))

            # Check that we won't search outside the sentence length due to
            # finding a MWE lexicon entry at the end of the sentence
            if i+j+k >= len(sentence):
                if debug is not None:
                    print('moved beyond end of sentence :(')
                match = False
                break

            # print '%d %s' % (i+j+k, sentence[i+j+k]['ArgType'])
            if sentence[i+j+k]['ArgType'] == 'subj':
                is_subj = True;

            pi =  pattern[j]
            if debug is not None:
                print('Testing %d/%d: %s' % (j+1, pat_len, pi))

            # If this is a category that we have to look up
            if pi[0] == '@':
                label = pi[1:]
                if debug is not None:
                    print('Checking if "%s" is in %s' % (sentence[i+j+k]['lemma'], label))
                lexicon = None
                required_pos = None
                if label in ALL_CONCEPT_LEXICONS:
                    lexicon = ALL_CONCEPT_LEXICONS[label]
                elif label in ALL_ACTION_LEXICONS:
                    lexicon = ALL_ACTION_LEXICONS[label]
                    required_pos = 'V'

                if lexicon is None:
                    raise BaseException(("unknown lexicon ref: '%s' in %s, %s" % (label, feature, pattern)))
                
                (is_match, matched_phrased_length) = is_in_lexicon(lexicon, sentence, i+j+k, required_pos=required_pos)

                #print 'found %s (%d) in %s? %s (%d)' % (sentence[i+j+k]['lemma'], i+j+k, label, is_match, matched_phrased_length)

                if not is_match:
                    match = False
                    break            
                else:
                    if debug is not None:
                        print('YAY:: "%s" is in set %s in %s' % (sentence[i+j+k]['lemma'], label, debug))

                # If we did find a match, recognize that some phrases are
                # multi-word expressions, so we may need to skip ahead more than
                # one token.  Note that we were already going to skip one token
                # anyway, so substract 1 from the phrase length
                k += (matched_phrased_length - 1)
                    
                #if not sentence[i+j+k]['lemma'] not in lexicon:
                #    if debug is not None:
                #        print '"%s" is not in set %s in %s' % (sentence[i+j+k]['lemma'], label, debug)
                #    match = False
                #    break
                #else:
                #    if debug is not None:
                
            elif pi == 'SELFCITATION':
                if debug is not None:
                    print('Checking if "%s" is %s' % (sentence[i+j+k]['pos'][0], pi[1]))

                if sentence[i+j+k]['word'] != pi:
                    if debug is not None:
                        print('"%s" is not a %s in %s' % (sentence[i+j+k]['lemma'], pi, debug))
                    match = False
                    break
                else:
                    if debug is not None:
                        print('YAY:: "%s" is a %s in %s' % (sentence[i+j+k]['lemma'], pi, debug))

            elif pi == 'CITATION':
                if debug is not None:
                    print('Checking if "%s" is %s' % (sentence[i+j+k]['pos'][0], pi[1]))

                if not sentence[i+j+k]['word'].endswith(pi):
                    if debug is not None:
                        print('"%s" is not a %s in %s' % (sentence[i+j+k]['lemma'], pi, debug))
                    match = False
                    break
                else:
                    if debug is not None:
                        print('YAY:: "%s" is a %s in %s' % (sentence[i+j+k]['lemma'], pi, debug))

    
            # Not sure if this is entirely right...
            elif pi == 'CREF':
                if sentence[i+j+k]['pos'] != 'CD' or sentence[i+j+k]['word'] != 'CREF':
                    match = False
                    break
                
            # If this is POS-match
            elif pi[0] == '#':
                if debug is not None:
                    print('Checking if "%s" is  %s' % (sentence[i+j+k]['pos'][0], pi[1]))
                # NOTE: we compare only the coarsest POS tag level (N/V/J)
                #
                # NOTE Check for weird POS-tagging issues with verbal adjectives
                if sentence[i+j+k]['pos'][0] != pi[1] and not (pi[1] == 'J' and sentence[i+j+k]['pos'] == 'VBN'):
                    match = False
                    if debug is not None:
                        print('"%s" is not %s in %s' % (sentence[i+j+k]['pos'][0], pi[1], debug))
                        break
                else:
                    if debug is not None:
                        print('"YAY:: %s" is %s in %s' % (sentence[i+j+k]['pos'][0], pi[1], debug))

            # Otherwise, we have to match the word
            else:
                if debug is not None:
                    print('Checking if "%s" is %s' % (sentence[i+j+k]['lemma'], pi))
                if sentence[i+j+k]['lemma'] != pi:
                    if debug is not None:
                        print('"%s" is not %s in %s' % (sentence[i+j+k]['lemma'], pi, debug))
                    match = False
                    break
                else:
                    if debug is not None:
                        print('YAY:: "%s" is %s in %s' % (sentence[i+j+k]['lemma'], pi, debug))       

        if match and (must_have_subj_value is not None) and (is_subj is not must_have_subj_value):
            if debug is not None:
                print('needed a subject for %s but this isn\'t one (%s != %s)' % (feature, is_subj, must_have_subj_value))
            continue


        # TODO: confirm we can skip 'j' items so i += j
        if match:
            if debug is not None:
                print('match!\n\n')
            return i
        else:
            if debug is not None:
                print('no match (%d, %d, %d)\n\n' % (i, j, k))

    if debug is not None:
        print('\n\n')

    return -1


ALL_ACTION_LEXICONS = {
    "AFFECT": ["afford", "believe", "decide", "feel", "hope", "imagine", "regard", "trust", "think"], 

    "ARGUMENTATION": ["agree", "accept", "advocate", "argue", "claim", "conclude", "comment", "defend", "embrace", "hypothesize", "imply", "insist", "posit", "postulate", "reason", "recommend", "speculate", "stipulate", "suspect"],

    "AWARE": ["be unaware", "be familiar with", "be aware", "be not aware", "know of"],

    "BETTER_SOLUTION": ["boost", "enhance", "defeat", "improve", "go beyond", "perform better", "outperform", "outweigh", "surpass"],
    
    "CHANGE": ["adapt", "adjust", "augment", "combine", "change", "decrease", "elaborate", "expand", "expand on", "extend", "derive", "incorporate", "increase", "manipulate", "modify", "optimize", "optimise", "refine", "render", "replace", "revise", "substitute", "tailor", "upgrade"], 
         
    "COMPARISON": ["compare", "compete", "evaluate", "test"],
         
    "DENOTATION": ["be", "denote", "represent" ],

    "INSPIRATION": ["inspire", "motivate" ],

    "AGREE": ["agree with", "side with" ],

    "CONTINUE": ["adopt", "base", "be base on", 'base on', "derive from", "originate in",  "borrow", "build on", "follow", "following", "originate from", "originate in", 'start from', 'proceed from'],

    "CONTRAST": ["be different from", "be distinct from", "conflict", "contrast", "clash", "differ from", "distinguish", "differentiate", "disagree", "disagreeing", "dissent", "oppose"],

    "FUTURE_INTEREST": [ "be interest in", "plan on", "plan to", "expect to", "intend to", "hope to"],

    "HEDGING_MODALS": ["could", "might", "may", "should" ],

    "FUTURE_MODALS": ["will", "going to" ],

    "SHOULD": ["should" ],

    "INCREASE": ["increase", "grow", "intensify", "build up", "explode" ],

    "INTEREST": ["aim", "ask", "address", "attempt", "be concern", "be interest", "be motivat", "concern", "concern", "concern", "consider", "concentrate on", "explore", "focus", "intend to", "like to", "look at how", "pursue", "seek", "study", "try", "target", "want", "wish", "wonder"],

    "NEED": ["be dependent on", "be reliant on", "depend on", "lack", "need", "necessitate", "require", "rely on"],

    "PRESENTATION": ["describe", "discuss", "give", "introduce", "note", "notice", "point out", "present", "propose", "put forward", "recapitulate", "remark", "report", "say", "show", "sketch", "state", "suggest", "talk about"], 

    "PROBLEM": ["abound", "aggravate", "arise", "be cursed", "be incapable of", "be force to", "be limite to", "be problematic", "be restrict to", "be trouble", "be unable to", "contradict", "damage", "degrade", "degenerate", "fail", "fall prey", "fall short", "force", "force", "hinder", "impair", "impede", "inhibit", "misclassify", "misjudge", "mistake", "misuse", "neglect", "obscure", "overestimate", "over-estimate", "overfit", "over-fit", "overgeneralize", "over-generalize", "overgeneralise", "over-generalise", "overgenerate", "over-generate", "overlook", "pose", "plague", "preclude", "prevent", "remain", "resort to", "restrain", "run into", "settle for", "spoil", "suffer from", "threaten", "thwart", "underestimate", "under-estimate", "undergenerate", "under-generate", "violate", "waste", "worsen"], 
        
    "RESEARCH": ["apply", "analyze", "analyse", "build", "calculate", "categorize", "categorise", "characterize", "characterise", "choose", "check", "classify", "collect", "compose", "compute", "conduct", "confirm", "construct", "count", "define", "delineate", "design", "detect", "determine", "equate", "estimate", "examine", "expect", "formalize", "formalise", "formulate", "gather", "identify", "implement", "indicate", "inspect", "integrate", "interpret", "investigate", "isolate", "maximize", "maximise", "measure", "minimize", "minimise", "observe", "predict", "realize", "realise", "reconfirm", "revalidate", "simulate", "select", "specify", "test", "verify", "work on"], 

    "SEE": [ "see", "view", "treat", "consider" ],

    "SIMILAR": ["bear comparison", "be analogous to", "be alike", "be related to", "be closely relate to", "be reminiscent of", "be the same as", "be similar to", "be in a similar vein to", "have much in common with", "have a lot in common with", "pattern with", "resemble"],

    "SOLUTION": ["accomplish", "account for", "achieve", "apply to", "answer", "alleviate", "allow for", "allow", "allow", "avoid", "benefit", "capture", "clarify", "circumvent", "contribute", "cope with", "cover", "cure", "deal with", "demonstrate", "develop", "devise", "discover", "elucidate", "escape", "explain", "fix", "gain", "go a long way", "guarantee", "handle", "help", "implement", "justify", "lend itself", "make progress", "manage", "mend", "mitigate", "model", "obtain", "offer", "overcome", "perform", "preserve", "prove", "provide", "realize", "realise", "rectify", "refrain from", "remedy", "resolve", "reveal", "scale up", "sidestep", "solve", "succeed", "tackle", "take care of", "take into account", "treat", "warrant", "work well", "yield"],

    "TEXTSTRUCTURE": ["begin by", "illustrate", "conclude by", "organize", "organise", "outline", "return to", "review", "start by", "structure", "summarize", "summarise", "turn to"], 
         
    "USE": ["apply", "employ", "use", "make use", "utilize", "implement", 'resort to']
    }
# Split the patterns into lists of things to match
for feature_name in list(ALL_ACTION_LEXICONS.keys()):
    tokenized = []
    patterns = ALL_ACTION_LEXICONS[feature_name]
    for pattern in patterns:
        tokenized.append(pattern.split())
    ALL_ACTION_LEXICONS[feature_name] = tokenized


ALL_CONCEPT_LEXICONS = {
    "NEGATION": ["no", "not", "nor", "non", "neither", "none", "never", "aren't", "can't", "cannot", "hadn't", "hasn't", "haven't", "isn't", "didn't", "don't", "doesn't", "n't", "wasn't", "weren't", "nothing", "nobody", "less", "least", "little", "scant", "scarcely", "rarely", "hardly", "few", "rare", "unlikely"],
    "3RD_PERSON_PRONOUN_(NOM)": ["they", "he", "she", "theirs", "hers", "his"],
    "OTHERS_NOM": ["they", "he", "she", "theirs", "hers", "his"],
    "3RD_PERSON)PRONOUN_(ACC)": ["her", "him", "them"], 
    "OTHERS_ACC": ["her", "him", "them"], 
    "3RD_POSS_PRONOUN": ["their", "his", "her"],
    #"OTHERS_POSS": ["their", "his", "her"],
    "OTHERS_POSS": ["their", "his", "her", "they"],
    "3RD_PERSON_REFLEXIVE": ["themselves", "himself", "herself"],
    "1ST_PERSON_PRONOUN_(NOM)": ["we", "i", "ours", "mine"],
    "SELF_NOM": ["we", "i", "ours", "mine"],
    "1ST_PERSON_PRONOUN_(ACC)": ["us", "me"],
    "SELF_ACC": ["us", "me"],
    "1ST_POSS_PRONOUN": ["my", "our"],
    "SELF_POSS": ["my", "our"],
    "1ST_PERSON_REFLEXIVE ": ["ourselves", "myself"],
    "REFERENTIAL": ["this", "that", "those", "these"],
    "REFLEXIVE": ["itself ourselves", "myself", "themselves", "himself", "herself"],
    "QUESTION": ["?", "how", "why", "whether", "wonder"],
    "GIVEN": ["noted", "mentioned", "addressed", "illustrated", "described", "discussed", "given", "outlined", "presented", "proposed", "reported", "shown", "taken"],
    
    "PROFESSIONALS": ["collegues", "community", "computer scientists", "computational linguists", "discourse analysts", "expert", "investigators", "linguists", "logicians", "philosophers", "psycholinguists", "psychologists", "researchers", "scholars", "semanticists", "scientists"],

    "DISCIPLINE": ["computerscience", "computer linguistics", "computational linguistics", "discourse analysis", "logics", "linguistics", "psychology", "psycholinguistics", "philosophy", "semantics", "lexical semantics", "several disciplines", "various disciplines"],
    
    "TEXT_NOUN": ["paragraph", "section", "subsection", "chapter"],
    
    "SIMILAR_NOUN": ["analogy", "similarity"],

    "SIMILAR_ADJ": ["similar", "comparable", "analogous", "kindred"],

    "COMPARISON_NOUN": ["accuracy", "baseline", "comparison", "competition", "evaluation", "inferiority", "measure", "measurement", "performance", "precision", "optimum", "recall", "superiority"],
    
    "CONTRAST_NOUN": ["contrast", "conflict", "clash", "clashes", "difference", "point of departure"],

    "AIM_NOUN": ["aim", "direction", "goal", "intention", "objective", "purpose", "task", "theme", "topic"],

    "ARGUMENTATION_NOUN": ["assumption", "belief", "hypothesis", "hypotheses", "claim", "conclusion", "confirmation", "opinion", "recommendation", "stipulation", "view"],

    "PROBLEM_NOUN": ["Achilles heel", "caveat", "challenge", "complication", "contradiction", "damage", "danger", "deadlock", "defect", "detriment", "difficulty", "dilemma", "disadvantage", "disregard", "doubt", "downside", "drawback", "error", "failure", "fault", "foil", "flaw", "handicap", "hindrance", "hurdle", "ill", "inflexibility", "impediment", "imperfection", "intractability", "inefficiency", "inadequacy", "inability", "lapse", "limitation", "malheur", "mishap", "mischance", "mistake", "obstacle", "oversight", "pitfall", "problem", "shortcoming", "threat", "trouble", "vulnerability", "absence", "dearth", "deprivation", "lack", "loss", "fraught", "proliferation", "spate"],

    "QUESTION_NOUN": ["question", "conundrum", "enigma", "paradox", "phenomena", "phenomenon", "puzzle", "riddle"],

    "SOLUTION_NOUN": ["answer", "accomplishment", "achievement", "advantage", "benefit", "breakthrough", "contribution", "explanation", "idea", "improvement", "innovation", "insight", "justification", "proposal", "proof", "remedy", "solution", "success", "triumph", "verification", "victory"],

    "INTEREST_NOUN": ["attention", "quest"],

    # Not sure if this one is used
    "RESEARCH_NOUN": ["evidence", "experiment", "finding", "progress", "observation", "outcome", "result"],

    "RESULT_NOUN": ["evidence", "experiment", "finding", "progress", "observation", "outcome", "result"],

    "METRIC_NOUN": ["bleu", "F-score", "F1-score", "F score", "F1 score", "precision", "recall", "accuracy", "correlation"],

    "CHANGE_NOUN": [ "adaptation", "enhancement", "extension", "generalization", "development", "modification", "refinement", "version", "variant", "variation"],

    "PRESENTATION_NOUN": ["article", "draft", "manuscript", "paper", "project", "report", "study"],
    
    "NEED_NOUN": ["necessity", "motivation"],

    "WORK_NOUN": ["account", "algorithm", "analysis", "analyses", "approach", "approaches", "application", "architecture", "characterization", "characterisation", "component", "design", "extension", "formalism", "formalization", "formalisation", "framework", "implementation", "investigation", "machinery", "method", "methodology", "model", "module", "moduls", "process", "procedure", "program", "prototype", "research", "researches", "strategy", "system", "technique", "theory", "tool", "treatment", "work"],

    "TRADITION_NOUN": ["acceptance", "community", "convention", "disciples", "disciplines", "folklore", "literature", "mainstream", "school", "tradition", "textbook"],

    "CHANGE_ADJ": ["alternate", "alternative"],
    
    "GOOD_ADJ": ["adequate", "advantageous", "appealing", "appropriate", "attractive", "automatic", "beneficial", "capable", "cheerful", "clean", "clear", "compact", "compelling", "competitive", "comprehensive", "consistent", "convenient", "convincing", "constructive", "correct", "desirable", "distinctive", "efficient", "effective", "elegant", "encouraging", "exact", "faultless", "favourable", "feasible", "flawless", "good", "helpful", "impeccable", "innovative", "insightful", "intensive", "meaningful", "neat", "perfect", "plausible", "positive", "polynomial", "powerful", "practical", "preferable", "precise", "principled", "promising", "pure", "realistic", "reasonable", "reliable", "right", "robust", "satisfactory", "simple", "sound", "successful", "sufficient", "systematic", "tractable", "usable", "useful", "valid", "unlimited", "well worked out", "well", "enough", "well-motivated"],
    
    "BAD_ADJ": ["absent", "ad-hoc", "adhoc", "ad hoc", "annoying", "ambiguous", "arbitrary", "awkward", "bad", "brittle", "brute-force", "brute force", "careless", "confounding", "contradictory", "defect", "defunct", "disturbing", "elusive", "erraneous", "expensive", "exponential", "false", "fallacious", "frustrating", "haphazard", "ill-defined", "imperfect", "impossible", "impractical", "imprecise", "inaccurate", "inadequate", "inappropriate", "incomplete", "incomprehensible", "inconclusive", "incorrect", "inelegant", "inefficient", "inexact", "infeasible", "infelicitous", "inflexible", "implausible", "inpracticable", "improper", "insufficient", "intractable", "invalid", "irrelevant", "labour-intensive", "laborintensive", "labour intensive", "labor intensive", "laborious", "limited-coverage", "limited coverage", "limited", "limiting", "meaningless", "modest", "misguided", "misleading", "nonexistent", "NP-hard", "NP-complete", "NP hard", "NP complete", "questionable", "pathological", "poor", "prone", "protracted", "restricted", "scarce", "simplistic", "suspect", "time-consuming", "time consuming", "toy", "unacceptable", "unaccounted for", "unaccounted-for", "unaccounted", "unattractive", "unavailable", "unavoidable", "unclear", "uncomfortable", "unexplained", "undecidable", "undesirable", "unfortunate", "uninnovative", "uninterpretable", "unjustified", "unmotivated", "unnatural", "unnecessary", "unorthodox", "unpleasant", "unpractical", "unprincipled", "unreliable", "unsatisfactory", "unsound", "unsuccessful", "unsuited", "unsystematic", "untractable", "unwanted", "unwelcome", "useless", "vulnerable", "weak", "wrong", "too", "overly", "only"],

    "BEFORE_ADJ": ["earlier", "initial", "past", "previous", "prior"],
    
    "CONTRAST_ADJ": ["different", "distinguishing", "contrary", "competing", "rival"],

    "CONTRAST_ADV": ["differently", "distinguishingly", "contrarily", "otherwise", "other than", "contrastingly", "imcompatibly", "on the other hand", ],

    "TRADITION_ADJ": ["better known", "better-known", "cited", "classic", "common", "conventional", "current", "customary", "established", "existing", "extant", "available", "favourite", "fashionable", "general", "obvious", "long-standing", "mainstream", "modern", "naive", "orthodox", "popular", "prevailing", "prevalent", "published", "quoted", "seminal", "standard", "textbook", "traditional", "trivial", "typical", "well-established", "well-known", "widelyassumed", "unanimous", "usual"],

    "MANY": ["a number of", "a body of", "a substantial number of", "a substantial body of", "most", "many", "several", "various"],

    "HELP_NOUN": ['help', 'aid', 'assistance', 'support' ],

    "SENSE_NOUN": ['sense', 'spirit', ],

    "GRAPHIC_NOUN": ['table', 'tab', 'figure', 'fig', 'example' ],
    
    "COMPARISON_ADJ": ["evaluative", "superior", "inferior", "optimal", "better", "best", "worse", "worst", "greater", "larger", "faster", "weaker", "stronger"],

    "PROBLEM_ADJ": ["demanding", "difficult", "hard", "non-trivial", "nontrivial"],
    
    "RESEARCH_ADJ": ["empirical", "experimental", "exploratory", "ongoing", "quantitative", "qualitative", "preliminary", "statistical", "underway"],

    "AWARE_ADJ": ["unnoticed", "understood", "unexplored"],

    "NEED_ADJ": ["necessary", "indispensable", "requisite"],

    "NEW_ADJ": ["new", "novel", "state-of-the-art", "state of the art", "leading-edge", "leading edge", "enhanced"],

    "FUTURE_ADJ": ["further", "future"],

    "HEDGE_ADJ": [ "possible", "potential", "conceivable", "viable"],
    
    "MAIN_ADJ": ["main", "key", "basic", "central", "crucial", "critical", "essential", "eventual", "fundamental", "great", "important", "key", "largest", "main", "major", "overall", "primary", "principle", "serious", "substantial", "ultimate"],

    "CURRENT_ADV": ["currently", "presently", "at present"],

    "TEMPORAL_ADV": ["finally", "briefly", "next"],

    "SPECULATION": [],

    "CONTRARY": [],

    "SUBJECTIVITY": [],

    "STARSEM_NEGATION": [  "contrary", "without", "n't", "none", "nor", "nothing", "nowhere", "refused", "nobody", "means", "never", "neither", "absence", "except", "rather", "no", "for", "fail", "not", "neglected", "less", "prevent", 
 ],

    'DOWNTONERS': [ 'almost', 'barely', 'hardly', 'merely', 'mildly', 'nearly', 'only', 'partially', 'partly', 'practically', 'scarcely', 'slightly', 'somewhat', ],

    'AMPLIFIERS': [ 'absolutely', 'altogether', 'completely', 'enormously', 'entirely', 'extremely', 'fully', 'greatly', 'highly', 'intensely', 'strongly', 'thoroughly', 'totally', 'utterly', 'very', ],

    
    'PUBLIC_VERBS': ['acknowledge', 'admit', 'agree', 'assert', 'claim', 'complain', 'declare', 'deny', 'explain', 'hint', 'insist', 'mention', 'proclaim', 'promise', 'protest', 'remark', 'reply', 'report', 'say', 'suggest', 'swear', 'write', ],
    
    'PRIVATE_VERBS': [ 'anticipate', 'assume', 'believe', 'conclude', 'decide', 'demonstrate', 'determine', 'discover', 'doubt', 'estimate', 'fear', 'feel', 'find', 'forget', 'guess', 'hear', 'hope', 'imagine', 'imply', 'indicate', 'infer', 'know', 'learn', 'mean', 'notice', 'prove', 'realize', 'recognize', 'remember', 'reveal', 'see', 'show', 'suppose', 'think', 'understand', ],
    
    'SUASIVE_VERBS': [ 'agree', 'arrange', 'ask', 'beg', 'command', 'decide', 'demand', 'grant', 'insist', 'instruct', 'ordain', 'pledge', 'pronounce', 'propose', 'recommend', 'request', 'stipulate', 'suggest', 'urge', ]


    }
# Split the patterns into lists of things to match
for feature_name in list(ALL_CONCEPT_LEXICONS.keys()):
    tokenized = []
    patterns = ALL_CONCEPT_LEXICONS[feature_name]
    for pattern in patterns:
        tokenized.append(pattern.split())
    ALL_CONCEPT_LEXICONS[feature_name] = tokenized

# def preprocess_sent(sent):
    

def is_in_lexicon(lexicon, sentence, si, ArgType=None, required_pos=None):
    for phrase in lexicon:

        # Can't match phrases that would extend beyond this sentence
        if len(phrase) + si > len(sentence):
            continue

        found = True
        found_arg = False

        for i, lemma in enumerate(phrase):
            # Check the word form too, just to prevent weird lemmatization
            # issues (usually for adjectives)
            if not (sentence[si+i]['lemma'] == lemma or sentence[si+i]['word'] == lemma) \
                    or not (required_pos is None or sentence[si+i]['pos'][0] == required_pos):
                found = False
                break
            if ArgType is not None and sentence[si+i]['ArgType'] == ArgType:
                found_arg = True
        if found and (ArgType is None or found_arg):
            #if len(phrase) > 1:
            #    print '~~~~~~Matched %s' % (' '.join(phrase))
            return (True, len(phrase))
    return (False, 0)


def countCases(feature, cue):
    count = 0
    for i in feature.keys():
        if i.startswith(cue):
            count += 1
    return count


def get_custom_pattern_features_wrapper(sent):
    sent = sent.replace('#AUTHOR_TAG', '#AUTHOR')
    processed_sent = process_sent(sent)
    for idx, token in enumerate(processed_sent):
        if token['originalText'] == '#AUTHOR':
            break

    feature = get_custom_pattern_features(processed_sent, [], [], idx)
    
    return feature

if __name__ == '__main__':

    import pandas as pd
    import numpy as np
    import matplotlib.pyplot as plt

    from sklearn.metrics import roc_curve, auc

    inFile = pd.read_csv(DATA_FILE, sep='\t')

    # Extracts all features
    feature = inFile['citation_context'].apply(get_custom_pattern_features_wrapper)


    types = ['Background', 'CompareOrContrast', 'Prior', 'Future', 'Motivation', 'Uses']
    fig, axs = plt.subplots(len(types))

    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')

    for i in range(len(types)):
        # Convert each feature into a number (count the cases)
        score = feature.apply(countCases, args=(types[i], ))
        label = inFile['citation_class_label'] == i
        fpr, tpr, _ = roc_curve(label, score)

        # plt.figure()
        plt1 = axs[i]
        lw = 2
        plt1.plot(fpr, tpr, color='darkorange',
                lw=lw, label='ROC curve for %s' % types[i])
        plt1.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')

        
        plt1.legend(loc="lower right")
    plt.show()
