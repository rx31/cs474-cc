# Environment

To run `getSentencePattern`, you need to 

- Install `pycorenlp` (`pip install pycorenlp`) 
- Download and unzip <http://nlp.stanford.edu/software/stanford-corenlp-full-2015-12-09.zip>
- Execute the following command in the unzipped folder: `java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -port 12345 -timeout 15000`


