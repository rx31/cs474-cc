#%%
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import math
import sklearn
import torch
import tensorflow as tf
#%%
dataframe = pd.read_csv ("practice_data_3CTask.tsv", sep = '\t', engine='python')
##DEFAULT ENGINE is C, which cannot handle all characters
#%%
class_labels=dataframe['citation_class_label'].copy()
influence_labels=dataframe['citation_influence_label'].copy()
#%%
#class_labels.value_counts().plot(kind='bar')
#%%
#influence_labels.value_counts().plot(kind='bar')
#%%
##THIS splits string by whitespace, removes words with less than 4 length, likely to be helper words.
def filterstring(string):
    string = ''.join([i for i in string if not i.isdigit()])
    string=string.replace(';', ' ')
    string=string.replace('(', ' ')
    string=string.replace(')', ' ')
    string=string.replace(',', ' ')
    string=string.replace('[', ' ')
    string=string.replace(']', ' ')
    string=string.replace(':', ' ')
    string=string.replace('.', ' ')
    string=string.replace('\'', ' ')
    string=string.replace('\"', ' ')
    strarray=string.replace('-',' ').split()
    returnarray=strarray.copy()
    for i in strarray:
        if len(i)<=4:
            returnarray.remove(i)
    return returnarray
#%%
# POSITION OF THE AUTHOR TAG
def authtagpos(arr):
    if ('#AUTHOR_TAG' not in arr):
        print (arr)
        return 0.5
    pos=arr.index('#AUTHOR_TAG')
    length=len(arr)
    return ((pos+1)/length)
#%%
def unfilterstring(string):
    string = ''.join([i for i in string if not i.isdigit()])
    string=string.replace(';', ' ')
    string=string.replace('(', ' ')
    string=string.replace(')', ' ')
    string=string.replace(',', ' ')
    string=string.replace('[', ' ')
    string=string.replace(']', ' ')
    string=string.replace(':', ' ')
    string=string.replace('.', ' ')
    string=string.replace('\'', ' ')
    string=string.replace('\"', ' ')
    strarray=string.replace('-',' ').split()
    return strarray
#%%
cdf=dataframe.copy()
#%%
for index, row in cdf.iterrows():
    citing_title=row['citing_title']
    citing_array=filterstring(citing_title)
    citing_set=set(citing_array)
    
    cited_title=row['cited_title']
    cited_array=filterstring(cited_title)
    cited_set=set(cited_array)
    
    cit_cont=row['citation_context']
    cont_array=filterstring(cit_cont)
    cont_set=set(cont_array)
    #You can add 1/2/any other parameter to this.
    l=len(cont_set.intersection(citing_set))+1
    m=len(cont_set)+1
    n=len(citing_set)+1
    ans=(l*l)/(m*n)
    cdf.at[index, 'cont_citing']=ans
    
    l=len(cont_set.intersection(cited_set))+1
    m=len(cont_set)+1
    n=len(cited_set)+1
    ans=(l*l)/(m*n)
    cdf.at[index, 'cont_cited']=ans
    
    l=len(citing_set.intersection(cited_set))+1
    m=len(citing_set)+1
    n=len(cited_set)+1
    ans=(l*l)/(m*n)
    cdf.at[index, 'citing_cited']=ans 
    
    cit_cont=row['citation_context']
    ans=authtagpos(unfilterstring(cit_cont))
    cdf.at[index, 'pos_tag']=ans
#%%    
from sklearn import svm, datasets
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score
import scikitplot as skplt
import sklearn.metrics as metrics
#%%  
X=cdf.iloc[:,9:13]
##THIS DEPENDS ON WHAT YOU USE AS FEATURES
y=cdf.iloc[:, 8]
#%%
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2, random_state=44)
#%%
model_logistic = LogisticRegression() 
model_logistic.fit(X_train, y_train)
pred_logistic=model_logistic.predict(X_test)
print("Accuracy", metrics.accuracy_score(y_test, pred_logistic))
prob_logistic = model_logistic.predict_proba(X_test)[:,1]
print ("logistic auc: ", sklearn.metrics.roc_auc_score(y_test,prob_logistic))
fpr, tpr, threshold = metrics.roc_curve(y_test, prob_logistic)
roc_auc = metrics.auc(fpr, tpr)
plt.title('Receiver Operating Characteristic')
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.show()
#%%
knn = KNeighborsClassifier(n_neighbors = 10)
knn.fit(X_train,y_train)
pred_knn=knn.predict(X_test)
print("Accuracy", metrics.accuracy_score(y_test, pred_knn))
y_scores = knn.predict_proba(X_test)
print ("logistic auc: ", sklearn.metrics.roc_auc_score(y_test,y_scores[:,1]))
fpr, tpr, threshold = metrics.roc_curve(y_test, y_scores[:, 1])
roc_auc = metrics.auc(fpr, tpr)
plt.title('Receiver Operating Characteristic')
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of kNN')
plt.show()
##Adding the last column in X ruins KNN AUC.
#%%
#NEURAL NETWORK APPROACH
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix
#%%
'''
OVERFIT model:
The best accuracy out of 120 models is for
solver                        lbfgs
activation_function            tanh
hidden_layers          (11, 11, 11)
accuracy                      0.866
loss                       0.250737
Name: 27, dtype: object
'''
#%%
# NOW I AM EXPERIMENTING WITH lbfgs, tanh/relu, varying layers.
#%%
scaler = StandardScaler()
comparison_dict = {}
solvers = ['lbfgs', 'adam', 'sgd']
activation_functions = ['tanh', 'relu', 'identity', 'logistic']
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2, random_state=44)
scaler.fit(X_train)
X_train=scaler.transform(X_train)
scaler.fit(X_test)
X_test=scaler.transform(X_test)
hidden_layer_sizes = [(2,), (3,), (5,), (2, 2,), (3, 2,), (5, 2,), (5, 3,), (5, 5,)]
#DATA IS DEFINITELY COMPLEX, highest is lbfgs solver, tanh activation, (5,5,) layer.
print("Comparing models...")
count = 1
for solver in solvers:
    for ac in activation_functions:
        for hidden_layer_config in hidden_layer_sizes:
            print("On model #%d" % (count))
            clf = MLPClassifier(solver=solver, alpha=1e-5, hidden_layer_sizes=hidden_layer_config,
                                random_state=1, max_iter=15000, activation=ac)
            clf.fit(X_train, y_train)
            comparison_dict[count] = {"solver": solver, "activation_function": ac,
                                      "hidden_layers": hidden_layer_config, "accuracy": clf.score(X_test,y_test), "loss": clf.loss_}
            print ("Accuracy", comparison_dict[count]['accuracy'], "Loss", comparison_dict[count]["loss"])
            count += 1
comparison_df = pd.DataFrame(comparison_dict).T
comparison_df['accuracy'] = pd.to_numeric(comparison_df['accuracy'])
max_acc_index = comparison_df['accuracy'].idxmax()
row = comparison_df.loc[max_acc_index]
print("The best accuracy out of %d models is for" % (count - 1))
print(row)
comparison_df.to_excel('InfluenceComparisonDict.xlsx')
#%%
'''
The best accuracy out of 96 models is for
solver                    lbfgs
activation_function        tanh
hidden_layers              (5,)
accuracy                  0.635
loss                   0.644846
Name: 3, dtype: object
'''
#%%
for index, row in cdf.iterrows():
    if row['citation_class_label']==0:
        cdf.at[index,'iszero']=1
    else:
        cdf.at[index,'iszero']=0
#%%  
X=cdf.iloc[:,9:13]
##THIS DEPENDS ON WHAT YOU USE AS FEATURES
y=cdf.iloc[:,13]
#%%
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2, random_state=44)
#%%
model_logistic = LogisticRegression() 
model_logistic.fit(X_train, y_train)
pred_logistic=model_logistic.predict(X_test)
print("Accuracy", metrics.accuracy_score(y_test, pred_logistic))
prob_logistic = model_logistic.predict_proba(X_test)[:,1]
print ("logistic auc: ", sklearn.metrics.roc_auc_score(y_test,prob_logistic))
fpr, tpr, threshold = metrics.roc_curve(y_test, prob_logistic)
roc_auc = metrics.auc(fpr, tpr)
plt.title('Receiver Operating Characteristic')
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.show()
#%%
knn = KNeighborsClassifier(n_neighbors = 10)
knn.fit(X_train,y_train)
pred_knn=knn.predict(X_test)
print("Accuracy", metrics.accuracy_score(y_test, pred_knn))
y_scores = knn.predict_proba(X_test)
print ("logistic auc: ", sklearn.metrics.roc_auc_score(y_test,y_scores[:,1]))
fpr, tpr, threshold = metrics.roc_curve(y_test, y_scores[:, 1])
roc_auc = metrics.auc(fpr, tpr)
plt.title('Receiver Operating Characteristic')
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of kNN')
plt.show()
#%%
scaler = StandardScaler()
comparison_dict = {}
solvers = ['lbfgs', 'adam', 'sgd']
activation_functions = ['tanh', 'relu', 'identity', 'logistic']
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2, random_state=44)
scaler.fit(X_train)
X_train=scaler.transform(X_train)
scaler.fit(X_test)
X_test=scaler.transform(X_test)
hidden_layer_sizes = [(2,), (3,), (5,), (2, 2,), (3, 2,), (5, 2,), (5, 3,), (5, 5,)]
#DATA IS DEFINITELY COMPLEX, highest is lbfgs solver, tanh activation, (5,5,) layer.
print("Comparing models...")
count = 1
for solver in solvers:
    for ac in activation_functions:
        for hidden_layer_config in hidden_layer_sizes:
            print("On model #%d" % (count))
            clf = MLPClassifier(solver=solver, alpha=1e-5, hidden_layer_sizes=hidden_layer_config,
                                random_state=1, max_iter=15000, activation=ac)
            clf.fit(X_train, y_train)
            comparison_dict[count] = {"solver": solver, "activation_function": ac,
                                      "hidden_layers": hidden_layer_config, "accuracy": clf.score(X_test,y_test), "loss": clf.loss_}
            print ("Accuracy", comparison_dict[count]['accuracy'], "Loss", comparison_dict[count]["loss"])
            count += 1
comparison_df = pd.DataFrame(comparison_dict).T
comparison_df['accuracy'] = pd.to_numeric(comparison_df['accuracy'])
max_acc_index = comparison_df['accuracy'].idxmax()
row = comparison_df.loc[max_acc_index]
print("The best accuracy out of %d models is for" % (count - 1))
print(row)
comparison_df.to_excel('ClassZeroComparisonDict.xlsx')
#%%
'''
The best accuracy out of 96 models is for
solver                    lbfgs
activation_function        tanh
hidden_layers            (5, 5)
accuracy                    0.6
loss                   0.579316
Name: 8, dtype: object
'''
#%%






