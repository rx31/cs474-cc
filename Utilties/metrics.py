import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.metrics import roc_curve, auc, RocCurveDisplay, f1_score
sns.set_style("whitegrid")
sns.set_context("talk")

'''
API for plotting standardized metrics for reporting. Pretty simple stuff but just to keep us all
on the same page as well as to not waste time coding them up ourselves. 

NOTE: For all models, this takes in predicted scores, NOT just the classification. 
This means for sklearn models you need to call model.predict_probaba(X).
For pytorch models you need to pass in the full softmax output.
'''

def multiclass_metrics(y_true, y_pred, paramdict=None, title="", output_text=False, filepath="/metric_outputs/"):
    '''
    :param y_true: A (Nx1) vector with the true class prediction [0,1,2,3,4,5]
    :param y_pred: A (Nx6) matrix with the classifier scores for each class.
    :param output_text: a boolean if you want to output the f1, macrof1, and auc scores to a textfile
    :param filepath: the filepath to which the output text will go
    :return: nothing, just generates plots
    '''
    titles = ["Background", "Compares/Contrasts", "Extension", "Future", "Motivation", "Uses"]
    fpr = []
    tpr = []
    aucs = []
    f1scores = []
    accs = []
    for i in range(6):
        tests = np.array(y_true == i, dtype="int")
        scores = np.array([y_pred[j][i] for j in range(len(y_pred))], dtype = np.float64)
        preds = np.array([np.argmax(y_pred[j]) == i for j in range(len(y_pred))], dtype="int")

        fpri, tpri, _ = roc_curve(tests, scores)
        fpr.append(fpri)
        tpr.append(tpri)
        aucs.append(auc(fpr[i], tpr[i]))
        f1scores.append(f1_score(tests, preds))
        acc = sum(preds == tests)/len(preds)
        accs.append(acc)
    macrof1 = np.mean(f1scores)
    accuracy = sum(np.argmax(y_pred) == y_true)/len(y_true)
    if output_text:
        f = open(filepath+"multiclass.txt", "a")
        print(f"{title}----------------", file=f)
        if paramdict:
            print(paramdict, file=f)
        for i in range(6):
            print(titles[i] + " Roc-Auc: " + str(aucs[i]), file=f)
            print(titles[i] + " F1 Score: " + str(f1scores[i]), file=f)
            print(titles[i] + " 1-All Accuracy: " + str(accs[i]), file=f)
        print("Macro F1 Score: " + str(macrof1), file=f)
        print("Total Accuracy: " + str(accuracy), file=f)
        print("----------------", file=f)
        f.close()

    fig, axs = plt.subplots(2,3, figsize=(20,10))
    plt.subplots_adjust(hspace=0.5, wspace=0.5)

    for i in range(6):
        ax = axs.flat[i]
        ax.set_xlabel("False Positive Rate")
        ax.set_ylabel("True Positive Rate")
        ax.set_title(f"{title} ROC for {titles[i]}")
        ax.plot(fpr[i], tpr[i], linestyle="--", marker=".", markersize=15, label=f"F1: {f1scores[i]}")
        ax.plot([0,1],[0,1], linestyle = "--", c = "k")
        ax.legend()
    plt.show()

def binary_metrics(y_true, y_pred, paramdict=None, title="", output_text=False, filepath="/metric_outputs/"):
    '''
    :param y_true: an (Nx1) vector of the true labels for each sample.
    :param y_pred: an (Nx1) vector of output probabilities for the POSITIVE class, which in our case
        is influential.
    :param output_text: a boolean if you want to output the f1, macrof1, and auc scores to a textfile
    :param filepath: the filepath to which the output text will go
    :return: nothing, just generates plots
    '''
    fpr, tpr, thresholds = roc_curve(y_true, y_pred)
    f1 = f1_score(y_true, np.array([y_pred[j] >= 0.5 for j in range(len(y_pred))], dtype="int"))
    aucs = auc(fpr, tpr)
    acc = sum((y_pred >= 0.5) == y_true) / len(y_true)

    if output_text:
        f = open(filepath+"binaryclass.txt", "a")
        print(f"{title}----------------", file=f)
        if paramdict:
            print(paramdict, file=f)
        print(f"ROC-AUC: {aucs}", file=f)
        print(f"F1 Score: {f1}", file=f)
        print(f"Accuracy: {acc}", file=f)
        print("----------------", file=f)
        f.close()

    plt.figure(figsize=(10, 6))
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.title(f"{title} ROC for Influential vs Incidential")
    plt.plot(fpr, tpr, linestyle="--", marker=".", markersize=15,label=f"F1 Score: {f1}")
    plt.plot([0, 1], [0, 1], linestyle="--", c="k")
    plt.legend()
    plt.show()