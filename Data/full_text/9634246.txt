 1
Identification of damaged proteins in human serum using modified Ehrlich’s 
reagent to target protein-bound pyrroles 
Fiona M. Campbell, Garry J. Rucklidge, Martin D. Reid, Louise Cantlay and Simon P. 
Robins 
University of Aberdeen Rowett Institute of Nutrition and Health, Aberdeen AB21 9SB, UK. 
 
 
corresponding author  
Dr Fiona Campbell 
University of Aberdeen  
Rowett Institute of Nutrition and Health 
Greenburn Road, Bucksburn 
Aberdeen AB21 9SB 
Scotland, UK. 
  
email: fiona.campbell@abdn.ac.uk 
Telephone: +44 -1224-712751     
 
 
Short Title: Detecting damaged proteins with ER-B 
Category Assignment: Article 
 2
<<<
Abstract 
Protein-bound pyrroles are a sign of oxidative damage. Here we report a specific method for 
detecting pyrrole-containing proteins using biotin-labeled Ehrlich’s reagent (ER-B). After 
treatment of either human serum or isolated human serum proteins with various oxidising 
agents, damaged, biotin-labeled components could be detected by blotting.  Combining the 
use of ER-B with proteomic techniques allowed human serum proteins susceptible to 
oxidative damage to be detected and then identified by LC/MS/MS. Identification of such 
proteins in different human conditions such as obesity, diabetes and cardiovascular disease 
should lead to the discovery of new biomarkers and the development of specific assays to 
monitor health status. 
>>>
Key Words: protein oxidation, protein-bound pyrroles, modified Ehrlich’s reagent, detection 
of damaged proteins, proteomics  
 
 
 
 
 
 
 
 
 
 
 
 
 3
<<<
Introduction 
Pyrroles are not present in newly-formed proteins but occur when lipid oxidation 
products such as 4,5-Epoxy-2-alkenals, e.g. 4,5(E)-epoxy-2(E)-heptenal (EH) which is 
produced from oxidation of n − 3 polyunsaturated fatty acids, react with free amino groups 
such as lysine residues on proteins [1;2]. Sugars such as glucose can also react non-
enzymaticly with free amino groups. This non-enzymic glycosylation is known as the 
Maillard reaction and is well known in food science. Both the Maillard reaction and lipid 
peroxidation follow similar reaction pathways, producing carbonyl derivatives which then 
form advanced glycation end products (AGE) or advanced lipid peroxidation end products 
(ALE) by means of carbonyl-amine reactions and aldol condensations forming protein cross-
links [3]. Pyrrole cross-links have been identified in long lived proteins such as lens 
crystallins and skin collagen and implicated in the stiffening of arteries and joints associated 
with aging [4;5]. Increased (carboxyalkyl) pyrrole immunoreactivity was detected in plasma 
from patients with renal failure and artherosclerosis compared with healthy volunteers [1]. 
AGE and ALE increasingly accumulate during aging and in chronic diseases [6], suggesting 
that detecting pyrroles in proteins should be a good way to develop biomarkers for early stage 
disease. 
Pyrroles in proteins can be measured spectophotometrically using Ehrlich’s reagent (p-
dimethylamino benzaldehyde) [7]. This method has been used to detect pyrroles in tail tendon 
collagen of streptozotocin-diabetic rats [8;9] and in human plasma proteins following 
treatment with hydrogen peroxide [10] but these assays lack sufficient sensitivity for wide 
application. This paper describes a proteomic technique to identify serum proteins susceptible 
to oxidative and glycation damage using a modified Ehrlich’s reagent, first used to 
investigate pyrrole cross-links in bone collagen [11]. The application of this methodology 
 4
will aid in the discovery of novel biomarkers and further the aim of developing specific 
assays to monitor health status.  
>>>
<<<
Methods
Materials and Methods 
Synthesis of biotinylated Ehrlich’s reagent (ER-B) 
Biotinylated Ehrlich’s reagent was synthesised as described previously [11] with minor 
modifications. Briefly N-Methyl-N-proprionic acid-4-amino benzaldehyde (100 mg) was 
dissolved in 5ml of dimethyl formamide: dichloromethane (1:5 v/v) and 300 mg of 1-Ethyl-3-
(3-dimethylaminopropyl)carbodiimide (EDC; Sigma-Aldrich, UK) was added and the 
solution was stirred at room temperature for 5 min. Biotin pentyl-amine (50 mg; Thermo 
Fischer, UK) was dissolved in 0.4 ml of methanol and added slowly to the above mixture 
giving a molar ratio of 3:1 for N-Methyl-N-proprionic acid-4-amino benzaldehyde to biotin 
pentyl-amine. After stirring at room temperature for 8 h. the mixture was concentrated to a 
volume of 2 ml using a rotary evaporator and then 18 ml of 2% v/v tifluoroacetic acid (TFA) 
was added to bring the volume to 20 ml. The ER-B was then purified by HPLC by applying 
aliquots (2.5 ml) of the mixture to 8 separate chromatographic runs using a preparative C-18 
VYDAC column (0.9 x 25 cm) pumped at 4 ml min-1. The buffers used were 0.1% TFA 
(buffer A) and 70% acetonitrile, 0.1% TFA (buffer B). The gradient applied was 25 % buffer 
B for 5 min followed by a linear increase to 70% buffer B over 40 min and the eluent was 
monitored at 330 nm to detect biotin. The chromatogram (Fig.1, left panel) showed two 
major peaks, with the peak eluting after 15 min corresponding to the unreacted acid. The 
second major component eluting around 25 min was analyzed by MALDI TOFF mass 
spectrometry and showed [M+H] of 518.2 and [M+Na] of 540.2, (Fig. 1, right panel) 
corresponding to the calculated M r of 517.7 for ER-B.  The peak containing the ER-B was 
 5
pooled from all 8 runs, freeze dried and stored at -20°C. The yield estimated gravimetrically 
was >80%. 
Oxidation of Proteins and Reaction with ER-B 
 Albumin from human serum (essentially fatty acid free) (HSA), human ApoA1 and 
serum from human male AB plasma were obtained from Sigma-Aldrich (UK) and were 
treated with different oxidising agents to produce pyrroles. Solutions of 1 mg/ml HSA, 1 
mg/ml Apo A1 and a 1:1 dilution of human serum in 0.9  M NaCl, 50 mM sodium phosphate, 
pH 7.2 (PBS) were incubated in the presence of different concentrations of 4,5(E)-epoxy-
2(E)-heptenal, (EH) as described previously for bovine serum albumin [2]. EH (0.25 µl), 
synthesised as described by Zamora & Hidalgo [12;13], was added to 0.5 ml of PBS and 
incubated with shaking for 10 min at 45 °C, then spun and portions of the supernatant were 
added to protein and serum samples. Unless otherwise stated, EH treatment comprised adding 
10 µl of the supernatant per ml of sample and incubating at 37 °C for 16 h.  HSA and human 
serum were oxidised in a FeSO4 (200mg/L) and H2O2 (100mg/L) mixture for 30 min at room 
temperature. To produce non-enzymic glycation, protein and serum samples were incubated 
in the presence of 10 mM glucose or 10 mM ribose for 7 d at 37 °C. Control samples of HSA 
and serum were incubated under identical conditions where the oxidising reagents were 
replaced with PBS. The protein and serum samples were passed through Hi Trap desalting 
columns (GE Biosciences, UK) and concentrated using Amicon Centricon concentrators 
(Millipore, UK). Protein concentrations were determined using the Pierce 660 protein assay 
(Thermo Fischer, UK) following the manufacturer’s recommended protocol. 
 The protein and serum samples were reacted with ER-B to label the pyrroles on the 
proteins as described [11]. Briefly ER-B was dissolved in methanol to give a 10 mg/ml 
solution which was diluted 1:4 with 3.5 M HCl and 50 µl was added to 450 µl of protein 
sample, to give a final reagent concentration of 0.039 mM. The samples were incubated for 
 6
15 min at 45 °C. The reaction was stopped by adding 50 µl of 2 M NaOH and excess non-
protein-bound ER-B was removed from the samples using Hi Trap desalting columns. 
SDS-PAGE and Blotting 
Following treatment with ER-B, protein samples were separated by electrophoresis 
using precast Criterion XT gels. All Blue Precision Protein™ Standards (Bio-Rad) were run 
alongside the samples.  Following electrophoresis the gels were either stained using Simply 
Blue Safe Stain (Invitrogen, UK) to visualise the proteins or the proteins were transferred 
onto PVDF membranes. Transfer buffer was 25 mM Tris; 192 mM glycine pH 8.3; and 20% 
(v/v) methanol. Following protein transfer the membranes were blocked using Western 
Blocker solution (Sigma-Aldrich, UK), The membranes were then incubated for 1 h with 
Extra Avidin peroxidase (Sigma-Aldrich, UK) 1/5000 in TTBS (0.1 % v/v Tween 20 in 100 
mM Tris–HCl, 150 mM NaCl ) pH 7.4 washed with TTBS three times and then developed 
using the Opti-4CN Substrate Kit (Bio-Rad,  UK). 
 
Two-dimensional gel electrophoresis (2DE) 
Human serum samples which had been reacted with ER-B were run in 2 DE. Bio-Rad 
immobilized pH gradient (IPG) strips (pH 3–10) were used for the separation of the proteins 
in the first dimension. Strips were rehydrated in 340 µl of rehydration buffer (7M urea; 2M 
thiourea; 4% w/v CHAPS; 2% w/v Biolyte; and 50mM DTT ) containing 300µg of protein 
sample at 20 °C for 1 h without applied voltage in a Bio-Rad IEF cell. Then focusing was 
carried out as detailed previously[14]. 
Following completion of the first dimension IPG strips were incubated in fresh 
equilibration buffer (6 M urea; 2% w/v SDS; 0.375 M Tris-HCl, pH 8.8; 20% v/v glycerol; 
and 130 mM DTT) for 10–15 min at room temperature before transfer to a second 
equilibration buffer (6 M urea; 2% w/v SDS; 0.375 M Tris-HCl, pH 8.8; 20% v/v glycerol; 
 7
and 135 mM iodoacetamide) for 10–15 min at room temperature. The strip was then applied 
to the top of an 18 × 18 cm gel cassette. Gels were run at 200V for 9.5 h or until the 
bromophenol blue had reached the bottom of the gel. After the second dimension run, the 
gels were fixed and stained with Coomassie Blue as described before [14]. 
Duplicate gels were run for blotting and the proteins were transferred from the 
polyacrylamide gels onto PVDF membranes at 0.8 mA/cm2 membrane for 3 h. Following 
transfer the ER-B labeled proteins were detected using Extra avadin peroxidase as described 
above for the 1D blots. 
 
Identification of Serum Proteins Tagged with ER-B 
Spots which aligned with those detected on the blots were excised from the duplicate 
SDS-PAGE gels. Gel plugs were trypsinized using the MassPrep Station (Waters, 
Micromass, UK) protocol. Identity of spots was analysed by LC/MS/MS essentially as 
described previously [15] with an ‘Ultimate’ nanoLC system (LC Packings, UK) using a C18 
PepMap 100 nanocolumn, 15 cm x 75 µm id, 3 µm, 100 Å (LC Packings). The MS was 
performed using a Q-Trap (Applied Biosystems/MDS Sciex, UK) triple quadrupole mass 
spectrometer fitted with a nanospray ion source. The total ion current (TIC) data were 
submitted for database searching using the MASCOT search engine (Matrix Science, UK) 
using the MSDB database. 
>>>
<<< 
Results 
Blotting to detect ER-B tagged proteins 
 In order to determine whether ER-B tagged proteins could be detected by blotting, 1D 
SDS-PAGE was carried out with purified protein and serum samples treated with different 
oxidising agents as described in the Methods Section. Fig. 2a and 2b show blots and the 
 8
corresponding Coomassie Blue stained gels of human serum albumin, Apo A1, and human 
serum. All of the treated samples show more intense bands in the blots compared to the 
control samples, indicative of increased formation of pyrroles in the treated samples. It is also 
notable that the gel of Apo A1 treated with EH shows the presence of two higher molecular 
weight bands not present in the control which are also detected in the western blot. This may 
be due to cross-links produced by the EH treatment. The bands for Apo A1 were fainter than 
those for HSA (Fig. 2b), possibly due to the lower proportion of lysine residues in Apo A1 
resulting in a smaller number of pyrroles per molecule of protein. HSA treated with various 
concentrations of ribose was blotted (Fig. 3a) and different protein loadings of HSA treated 
with EH (Fig. 3b). This shows increasing band intensity with higher concentrations of ribose 
and increased protein loading producing broader, darker bands.   
 
Proteomics to identify ER-B tagged proteins in human serum treated with oxidising agents 
 2DE and blotting was carried out as described in the Methods. All gels and blots were 
done in duplicate and spots corresponding to the blot were cut from two gels. Fig. 4 shows 
representative 2DE Coomassie-stained gels and corresponding blots of human serum samples 
treated with different reagents. Although there is little difference in appearance between the 
stained gels the blots show many more spots in the samples treated with the oxidising agents 
compared to the control blots. The spots which corresponded to those on the blots were cut 
and identified using mass spectrometry as described above.  Fig. 5 shows the identities of the 
spots cut from the gel A with reference to corresponding blot B for serum treated with EH.  
The identities of the protein spots as determined by LC/MS/MS are listed in Table 1.  In order 
to demonstrate the reproducibility of the protein pattern identified by blotting, 2DE blots 
were carried out with human serum samples prepared on four different days (Fig. 6). 
 
 9
>>>
<<<
Discussion
 
Discussion and Conclusions 
This study has demonstrated the feasibility of using a specially designed form of 
Ehrlich’s reagent to target pyrroles in oxidised proteins in human serum.  A wide range of 
mechanisms have been proposed to explain pyrrole formation in proteins, but the most 
common involves reaction with lipid oxidation products, including 4,5 epoxy-2-alkenals, 4-
hydroxy-2-alkenals, unsaturated epoxyoxo fatty acids and lipid hydroperoxides [3;16;17].  
The initial stages of many glycation reactions are also mediated by pyrrole intermediates[18] 
and these may progress further to AGE products that may constitute cross-links within and 
between proteins.  In the present study, the mechanisms of pyrrole formation in the isolated 
components have not yet been investigated, although further examination by mass 
spectrometry and other techniques will be used in future to provide information on the 
sources of protein damage.  
 Lipid oxidation products generally interact with nucleophilic amino acid residues in 
proteins, including lysine, histidine, free cysteine and arginine. The predominant protein 
labelled by the ER-B in human serum treated with oxidising agents was human serum 
albumin reflecting both its abundance in serum and its relatively high lysine content of 9.5% 
per mole [19]. Recent studies by mass spectrometry have characterized a number of 
compounds, including pyrroles, resulting from the reaction of proteins with oxidised linoleic 
acid [20]. Ehrlich’s reagent specifically reacts with pyrroles and not with other moieties 
present in proteins such as carbonyls: this reagent has been used in many studies to detect 
pyrroles [7;21;22]. For reaction with the modified Ehrlich’s reagent used in the present study, 
the only requirement is that either the 2 or the 5 position of the heterocyclic ring has to be 
available (see Fig. 7 for reaction scheme).  This would be the case, for example, where 
 10
monoalkylpyrrole derivatives of lysyl residues are formed [3], as well as for pyrrole-mediated 
cross-linking involving additional lysyl, cysteinyl or histidyl residues, such as histidino-
threosidine, a cross-link between lysine and histidine described by Dai et al. [23]. Earlier 
studies with model systems indicated that pyrrole-mediated cross-linking proceeds through an 
initial Michael addition mechanism and not by autoxidation of monoalkylpyrrole derivatives 
[24;25]. 
 In summary, this study has shown that pyrrolized proteins in human serum can be 
labeled specifically using a modified Ehrlich’s reagent and identified using proteomic 
techniques.  The aim of our future studies will be to apply these techniques to define patterns 
of damaged proteins which characterise disease states such as diabetes and obesity.  The 
detailed characterisation of such pyrrole adducts will provide mechanistic information on the 
type of damage that has occurred thus allowing the development of specific biomarkers and 
assays to monitor health status. 
>>>
Acknowledgements 
We are indebted to the Scottish Government Rural and Environment Research and Analysis 
Directorate and to Immunodiagnostic Systems Ltd, Boldon, UK for support 
<<< 
References 
 
 [1]  K.Kaur, R.G.Salomon, J.O'Neil, and H.F.Hoff, (Carboxyalkyl)pyrroles in human 
plasma and oxidized low-density lipoproteins, Chem.Res.Toxicol. 10 (1997) 1387-
1396. 
 [2]  F.J.Hidalgo and R.Zamora, Modification of bovine serum albumin structure following 
reaction with 4,5(E)-epoxy-2(E)-heptenal, Chem.Res.Toxicol. 13 (2000) 501-508. 
 [3]  F.J.Hidalgo and R.Zamora, Interplay between the maillard reaction and lipid 
peroxidation in biochemical systems, Ann.N.Y.Acad.Sci. 1043 (2005) 319-326. 
 [4]  V.M.Monnier and A.Cerami, Nonenzymatic browning in vivo: possible process for 
aging of long-lived proteins, Science 211 (1981) 491-493. 
 11
 [5]  E.B.Frye, T.P.Degenhardt, S.R.Thorpe, and J.W.Baynes, Role of the Maillard reaction 
in aging of tissue proteins. Advanced glycation end product-dependent increase in 
imidazolium cross-links in human lens proteins, J Biol Chem 273 (1998) 18714-18719. 
 [6]  I.Dalle-Donne, D.Giustarini, R.Colombo, R.Rossi, and A.Milzani, Protein 
carbonylation in human diseases, Trends Mol Med. 9 (2003) 169-176. 
 [7]  F.J.Hidalgo, M.Alaiz, and R.Zamora, A spectrophotometric method for the 
determination of proteins damaged by oxidized lipids, Anal.Biochem. 262 (1998) 129-
136. 
 [8]  M.Stefek, A.Gajdosik, A.Gajdosikova, and L.Krizanova, p-
Dimethylaminobenzaldehyde-reactive substances in tail tendon collagen of 
streptozotocin-diabetic rats: temporal relation to biomechanical properties and advanced 
glycation endproduct (AGE)-related fluorescence, Biochimica et Biophysica Acta 
(BBA) - Molecular Basis of Disease 1502 (2000) 398-404. 
 [9]  P.V.A.Babu, K.E.Sabitha, and C.S.Shyamaladevi, Effect of green tea extract on 
advanced glycation and cross-linking of tail tendon collagen in streptozotocin induced 
diabetic rats, Food and Chemical Toxicology 46 (2008) 280-285. 
[10]  F.Martinez-Cruz, J.M.Guerrero, and C.Osuna, Melatonin prevents the formation of 
pyrrolized proteins in human plasma induced by hydrogen peroxide, Neuroscience 
Letters 326 (2002) 147-150. 
[11]  J.D.Brady and S.P.Robins, Structural Characterization of Pyrrolic Cross-links in 
Collagen Using a Biotinylated Ehrlich's Reagent, J.Biol.Chem. 276 (2001) 18812-
18818. 
[12]  R.Zamora and F.J.Hidalgo, Phosphatidylethanolamine Modification by Oxidative Stress 
Product 4,5(E)-Epoxy-2(E)-heptenal, Chem.Res.Toxicol. 16 (2003) 1632-1641. 
[13]  R.Zamora and F.J.Hidalgo, Linoleic acid oxidation in the presence of amino compounds 
produces pyrroles by carbonyl amine reactions, Biochimica et Biophysica Acta (BBA) - 
Lipids and Lipid Metabolism 1258 (1995) 319-327. 
[14]  B.de Roos, I.Duivenvoorden, G.Rucklidge, M.Reid, K.Ross, R.J.Lamers, P.J.Voshol, 
L.M.Havekes, and B.Teusink, Response of apolipoprotein E 3 Leiden transgenic mice 
to dietary fatty acids: combining liver proteomics with physiological data, FASEB J. 19 
(2005) 813-815. 
[15]  B.de Roos, A.Geelen, K.Ross, G.Rucklidge, M.Reid, G.Duncan, M.Caslake, G.Horgan, 
and I.A.Brouwer, Identification of potential serum biomarkers of inflammation and 
lipid modulation that are altered by fish oil supplementation in healthy volunteers, 
Proteomics 8 (2008) 1965-1974. 
[16]  Z.F.Liu, P.E.Minkler, and L.M.Sayre, Mass spectroscopic characterization of protein 
modification by 4-Hydroxy-2-(E)-nonenal and 4-Oxo-2-(E)-nonenal, 
Chem.Res.Toxicol. 16 (2003) 901-911. 
[17]  Z.Liu and L.M.Sayre, Model Studies on the Modification of Proteins by Lipoxidation-
Derived 2-Hydroxyaldehydes, Chem.Res.Toxicol. 16 (2003) 232-241. 
 12
[18]  M.Portero-Otin, R.H.Nagaraj, and V.M.Monnier, Chromatographic evidence for 
pyrraline formation during protein glycation in vitro and in vivo, Biochimica et 
Biophysica Acta (BBA) - Protein Structure and Molecular Enzymology 1247 (1995) 
74-80. 
[19]  G.E.Austin, R.H.Mullins, and L.G.Morin, Non-enzymic glycation of individual plasma 
proteins in normoglycemic and hyperglycemic patients, Clin Chem 33 (1987) 2220-
2224. 
[20]  X.Zhu, X.Tang, V.E.Anderson, and L.M.Sayre, Mass Spectrometric Characterization of 
Protein Modification by the Products of Nonenzymatic Oxidation of Linoleic Acid, 
Chem.Res.Toxicol. (2009). 
[21]  F.J.Hidalgo, M.Alaiz, and R.Zamora, Pyrrolization and antioxidant function of proteins 
following oxidative stress, Chem.Res.Toxicol. 14 (2001) 582-588. 
[22]  P.A.Liddell, T.P.Forsyth, M.O.Senge, and K.M.Smith, Chemical Synthesis of A Gsa-
Pyrrole and Its Reaction with Ehrlich Reagent, Tetrahedron 49 (1993) 1343-1350. 
[23]  Z.Dai, I.Nemet, W.Shen, and V.M.Monnier, Isolation, purification and characterization 
of histidino-threosidine, a novel Maillard reaction protein crosslink from threose, lysine 
and histidine, Archives of Biochemistry and Biophysics 463 (2007) 78-88. 
[24]  G.Xu, Y.Liu, M.M.Kansal, and L.M.Sayre, Rapid Cross-Linking of Proteins by 4-
Ketoaldehydes and 4-Hydroxy-2-alkenals Does Not Arise from the Lysine-Derived 
Monoalkylpyrroles, Chem.Res.Toxicol. 12 (1999) 855-861. 
[25]  G.Xu and L.M.Sayre, Structural Elucidation of a 2:2 4-Ketoaldehyde Amine Adduct as 
a Model for Lysine-Directed Cross-Linking of Proteins by 4-Ketoaldehydes, 
Chem.Res.Toxicol. 12 (1999) 862-868. 
>>>
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 13
Figure Legends 
 
Figure 1: Preparation of Biotinylated Ehrlich’s Reagent (ER-B) 
  Left panel, typical chromatograph showing the purification of ER-B from the reaction 
mixture as described in the methods section. 
  Right panel, MS trace of purified ER-B  
 
Figure 2: Detection of pyrroles in human serum proteins by ER-B using western 
blotting  
  a: Left Panel, representative SDS PAGE of human serum albumin  (HSA) and Apo 
A1 following treatment with various oxidising or glycating agents and ER-B as 
described in the methods section. Right panel, blot of the same samples. 
  Lane: 1. All blue mwt markers, 2. HSA control, 3. HSA control 7 days 37oC, 4. HSA 
10mM glucose 7 days 37oC, 5. HSA 10mM ribose 7 days 37oC, 6. HSA ferric 
sulphate/H2O2, 7. HSA  EH, 8. Apo A1 control, 9. Apo A1  EH.  Protein loaded 10 
µg per lane. 
 
  b: Left panel, representative SDS PAGE  of human serum following  treatment with 
various oxidising or glycating agents and ER-B as described in the methods section. 
Right panel, blot of same samples. 
  1. mwt. markers 2. Control, 3. EH, 4.Fe SO4/H2O2, 5. 10mM ribose 7 days 37oC.  
Protein loaded 10 µg per lane. 
 
Figure 3: a: Representative SDS PAGE (left panel) and blot of HSA (right panel) 
incubated with different concentrations of ribose. 
 14
Lane: 1. 0, 2. 20 mM ribose, 3.100 mM ribose. HSA loaded 5 µg per lane, incubated 
for 18 hours at 37oC followed by treatment with ER-B as described in methods 
section. 
 
  b: Representative SDS PAGE (left panel) and blot (right panel) of HSA treated 
with EH. 
   Lane: 1. 0.25 µg, 2. 0.5 µg, 3. 1 µg, 4. 2.5 µg, 5. 5 µg, 6. 10 µg , 7. 15 µg 
 
Figure 4: Representative images of 2DE gels and blots of human serum showing ER-B 
labeled proteins. 
  Top: Coomassie-stained gels, Bottom: blots. Experimental procedures are as 
described in the methods section. 
 
Figure 5: Representative 2DE gel and blot of human serum treated with EH showing 
protein spots identified by LC/MS/MS.  
  Top: Coomassie-stained gel, Bottom: blot. Experimental procedures are as described 
in the methods section. 
  Numbered spots indicate protein spots identified by LC/MS/MS. Spots inside dotted 
lines have been identified as the same protein. See table 1 for protein identification. 
 
 
Figure 6: 2DE blots of Human Serum treated with EH prepared on 4 different days 
  Experimental procedures were as described in the methods section.  
 
 15
Figure 7: Reaction of Biotinylated Ehrlich’s Reagent(ER-B) with protein bound 
pyrroles.  
  Either the 2 or the 5 position of the heterocyclic ring of the protein bound pyrrole 
needs to be available for the reaction to take place. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 16
 
Figure 1 
 17
 
Figure 2 
 18
 
Figure 3 
 19
 
Figure 4 
 20
 
Figure 5 
 21
 
Figure 6 
 22
 
Figure 7 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 23
Table 1: Protein Identification by LC/MS/MS 
Spot Asscession No. Protein Peptides matched 
1 1AO6A Serum albumin, chain A -human 34 
2 TFHUP   Transferrin precursor-human 29 
3 C3HU Complement C3 precursor-human 8 
4 ITHU   Alpha-1-antitrypsin precursor-human 12 
5 Q6GTG1_HUMAN Vitamin D-binding protein-human 8 
6 GHHU Ig gamma-1 chain C region 5 
7 OMHU2 Alpha-1-acid glycoprotein 2 precursor 3 
8 Q6NSB4_HUMAN Haptoglobin 10 
9 LPHUA1     Apolipoprotein A-I precursor 5 
10 1B2WL Antibody v domain and c domain, chain L 6 
11 HPHU2 Haptoglobin precursor, allele 2 2 
 
