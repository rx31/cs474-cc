econstor
Make Your Publications Visible.
A Service of
zbw Leibniz-InformationszentrumWirtschaftLeibniz Information Centrefor Economics
Dietz, Simon
Working Paper
The treatment of risk and uncertainty in the US social
cost of carbon for regulatory impact analysis
Economics Discussion Papers, No. 2011-30
Provided in Cooperation with:
Kiel Institute for the World Economy (IfW)
Suggested Citation: Dietz, Simon (2011) : The treatment of risk and uncertainty in the US social
cost of carbon for regulatory impact analysis, Economics Discussion Papers, No. 2011-30, Kiel
Institute for the World Economy (IfW), Kiel
This Version is available at:
http://hdl.handle.net/10419/48829
Standard-Nutzungsbedingungen:
Die Dokumente auf EconStor dürfen zu eigenen wissenschaftlichen
Zwecken und zum Privatgebrauch gespeichert und kopiert werden.
Sie dürfen die Dokumente nicht für öffentliche oder kommerzielle
Zwecke vervielfältigen, öffentlich ausstellen, öffentlich zugänglich
machen, vertreiben oder anderweitig nutzen.
Sofern die Verfasser die Dokumente unter Open-Content-Lizenzen
(insbesondere CC-Lizenzen) zur Verfügung gestellt haben sollten,
gelten abweichend von diesen Nutzungsbedingungen die in der dort
genannten Lizenz gewährten Nutzungsrechte.
Terms of use:
Documents in EconStor may be saved and copied for your
personal and scholarly purposes.
You are not to copy documents for public or commercial
purposes, to exhibit the documents publicly, to make them
publicly available on the internet, or to distribute or otherwise
use the documents in public.
If the documents have been made available under an Open
Content Licence (especially Creative Commons Licences), you
may exercise further usage rights as specified in the indicated
licence.
  http://creativecommons.org/licenses/by-nc/2.0/de/deed.en
www.econstor.eu
The Treatment of Risk and Uncertainty in the US Social 
Cost of Carbon for Regulatory Impact Analysis 
Simon Dietz 
Grantham Research Institute on Climate Change and the Environment, 
and Department of Geography and Environment,  
London School of Economics and Political Science (LSE) 
<<<
Abstract
This note considers the treatment of risk and uncertainty in the recently established 
‘social cost of carbon’ (SCC) for analysis of federal regulations in the United States. It argues 
that the analysis of the US Interagency Working Group on Social Cost of Carbon did not go far 
enough into the tail of low-probability, high-impact scenarios, and, via its approach to 
discounting, it mis-estimated climate risk, possibly hugely. Given the uncertainty about 
estimating the SCC, the note concludes by arguing that there is in fact much to commend an 
approach whereby a quantitative, long-term emissions target is chosen, and the price of carbon 
for regulatory impact analysis is then based on estimates of the marginal cost of abatement to 
achieve that very target.  
>>>
Paper submitted to the special issue  
The Social Cost of Carbon  
JEL   Q54 
Keywords   Ambiguity; climate change; discounting; integrated assessment modelling; risk; 
social cost of carbon; uncertainty 
Correspondence   Simon Dietz, Grantham Research Institute on Climate Change and the 
Environment, and Department of Geography and Environment, London School of Economics 
and Political Science (LSE), Houghton Street, London WC2A 2AE, UK;  
e-mail: s.dietz@lse.ac.uk
 
I am grateful to Antony Millner and Martin Weitzman for commenting on an earlier draft, and 
to David Anthoff, Charles Griffiths and Chris Hope for answering various questions. All errors 
and views remain, however, my responsibility. The financial support of the Grantham 
Foundation for the Protection of the Environment, as well as the Centre for Climate Change 
Economics and Policy, which is funded by the UK’s Economic and Social Research Council 
and by Munich Re, is also acknowledged.  
 
Discussion Paper 
No. 2011-30 | August 5, 2011 | http://www.economics-ejournal.org/economics/discussionpapers/2011-30  
© Author(s) 2011. Licensed under a Creative Commons License - Attribution-NonCommercial 2.0 Germany
 
 3 
<<<
Introduction 
 
In this note I offer some comments on the recently established ‘social cost of carbon’ (SCC) for 
analysis of federal regulations in the United States (Interagency Working Group on Social Cost of 
Carbon 2010). While thorough in some respects, I argue that the analysis of the Interagency 
Working Group on Social Cost of Carbon did not go far enough into the tail of low-probability, 
high-impact scenarios. This immediately raises questions about the treatment of risk and 
uncertainty in benefit-cost analysis. I argue that, via their approach to discounting, the Interagency 
Working Group mis-estimated climate risk, possibly hugely, and I also show that recent insights 
from the theory of decision-making under uncertainty caution against simple averaging of the 
estimates of different models, something the Interagency Working Group relied on. Finally, 
drawing on experience in the UK, I argue that, given the uncertainty about estimating the SCC, 
there is much to commend an approach whereby a quantitative, long-term emissions target is 
chosen (partly based on what we know about the SCC), and the price of carbon for regulatory 
impact analysis is then based on estimates of the marginal cost of abatement to achieve that very 
target. By means of a disclaimer, I wilfully ignore some other important issues, such as how to 
weigh the impacts of US carbon dioxide (CO2) emissions in other countries. 
 
While the United States does not lead the world in making climate-change mitigation policies, it 
has been a pioneer in the use of benefit-cost analysis to inform the making of federal regulations 
more generally. The intention is that regulations are adopted only if they provide benefits in excess 
of their costs. Perhaps unsurprisingly, the reality of carrying out benefit-cost analysis of federal 
regulations falls short of best practice, and it does not appear to be having a significant impact on 
many regulatory decisions, except higher-profile cases (Hahn and Tetlock 2008). Nevertheless, the 
introduction of an SCC is a potentially significant step in the development of US climate-
mitigation policy, especially in the continuing absence of dedicated, overarching mitigation policy 
instruments whose costs to individuals and firms could more directly enter benefit-cost 
calculations in other policy areas. 
 
<<<
results
The damage function and the SCC 
 
The SCC is the present value of the impact of an additional tonne of CO2 emitted to the 
atmosphere. In order to estimate it, one needs to use a simulation model connecting emissions of 
 4 
CO2 with changes in individual utility and social welfare, expressed in terms of an equivalent 
change in consumption. It is very well known that such models, called ‘integrated assessment 
models’, face huge uncertainties. To its credit, the Interagency Working Group appears to have 
been well aware of the issue of uncertainty. First, it chose to use the three most prominent 
integrated assessment models, rather than opting for just one. Second, the models were submitted 
to Monte Carlo simulation methods, such that (some) uncertain parameters were treated as 
random variables, and the models eventually produced probability distribution functions for the 
SCC. Among these random variables, it is particularly noteworthy that the Working Group treated 
the climate sensitivity, i.e. the change in the global mean temperature in equilibrium 
accompanying a doubling in the atmospheric stock of CO2, as random, and specified a probability 
distribution with a large positive skew (p14-15). The climate sensitivity is known to be a very 
important parameter in estimating the SCC. 
 
However, arguably the Working Group did not go far enough in its exploration of the uncertainty 
about another crucial set of parameters in the models, namely those establishing the ‘damage 
function’ that links atmospheric temperature to economic impacts. The damage function, which 
may be sector-specific (as in the FUND model) or of a reduced form that aggregates across sectors 
(as in the DICE and PAGE models), is calibrated on more detailed impacts studies. Unfortunately, 
these studies give data points for low temperature changes only. To estimate the economic impact 
of larger temperature changes in the region of 5°C above pre-industrial and beyond, one simply 
extrapolates, making an assumption about functional form on which there is almost no data basis.  
 
Recent work by, among others, Martin Weitzman (2010) and Frank Ackerman and colleagues 
(2010) has questioned the prevailing assumptions made about functional form. To take the DICE 
model as an example (Nordhaus and Boyer 2000; Nordhaus 2008), it can easily be shown that the 
assumption of a quadratic relationship between damages and temperature, together with the 
modellers’ specific coefficient values, implies that global warming can reach more than 18°C before 
the equivalent of 50% of global GDP is lost. This seems remarkable, since, for example, such 
temperatures are likely to test the limits of human physiology (Sherwood and Huber 2010). While 
the parameters of the damage function in PAGE are modelled as random, such that damages reach 
up to around 10% of global GDP when global warming reaches 5°C, it has equally been argued 
that 5°C constitutes an environmental transformation, being a larger change in global mean 
temperature than exists between the present day and the peak of the last ice age. Surely it is at least 
 5 
possible that climate damages will exceed 10% of global GDP upon 5°C warming? As far as FUND 
is concerned, Figure 1A of the Interagency Working Group shows that its more complex, sectorally 
disaggregated approach implies total damages are actually slowing as warming passes 5°C, and at 
8°C above pre-industrial they are only about 7% of GDP.2 
 
Clearly this begs the question of how much higher the SCC might be, if the damage function 
becomes steeper. A recent paper by Ackerman and Stanton (2011) attempts to answer it using the 
DICE model, applying a functional form proposed by Weitzman (2010). Furthermore, they also 
question the damage estimates of the models at low temperatures, drawing on work by Michael 
Hanemann (2008) that argues damages could also be significantly higher in this realm. Looking at 
these changes separately and together, they show that the SCC could be several times, even orders 
of magnitude, higher. This result is in fact corroborated by some recent analysis of my own. 
Combining steeply increasing damages with a positively skewed distribution on the climate 
sensitivity parameter, I otherwise replicated the analysis with PAGE for the Stern Review (Stern 
2007) to also find that the SCC could be hundreds of dollars higher than previously estimated 
(Dietz forthcoming). 
 
Discounting, risks to welfare, and uncertainty 
 
The consequence of such modelling is thus to increase the range of possible climate damages, 
specifically to increase the upper limit, and this in turn increases the importance of properly 
handling risk and uncertainty. I will define these two terms in the sense introduced by Knight 
(1921), whereby the distinction turns on whether states of nature can be assigned precise 
probabilities3 (risk) or not (uncertainty, or, as it is often known nowadays, ambiguity). Using this 
distinction, it has become fairly common practice to conduct risk analysis around the SCC by 
performing Monte Carlo simulation using one particular model. What is less readily 
acknowledged is that having multiple models, which are structurally different (as in this case) 
                                               
2 Some of the damage-function parameters in FUND were submitted to Monte Carlo simulation, but, while 
this informed estimation of the SCC, it was not used to generate a range or confidence interval around the 
aggregate damage function in Figure 1A. It is also worth noting that FUND’s damage functions are 
dependent on more factors than just temperature (e.g. income), and these vary along with temperature in the 
data presented in Figure 1A. 
3 These precise probabilities may be objective or subjective. 
 6 
and/or take different parameter distributions as their inputs (also true of this case), raises the issue 
of uncertainty.4  
 
First consider risk. Within each of the integrated assessment models, the Working Group opted to 
use Monte Carlo simulation in conjunction with a fairly standard discounted cash flow analysis, of 
the sort used routinely in financial appraisal of private investments and social benefit-cost analysis 
of public projects. Each draw of the Monte Carlo simulation produced a stream of forecast 
monetary damages from climate change into the future, which was discounted back to the present 
at an exogenous, constant rate (2.5, 3 or 5%). The mean/expectation of the resulting probability 
distribution of present values of climate damage was used to estimate the SCC. It will be useful to 
keep in mind that this approach is identical to calculating a single stream of expected damages, 
treating this single stream as if it is deterministic, and discounting. 
 
At first glance, this is an elegantly simple approach. Unfortunately it breaks down in the face of the 
sorts of large risk to future (welfare-equivalent) consumption prospects outlined in the previous 
section. To see why this is so, recall that, in making the final steps in an integrated assessment 
model from monetary climate damages to changes in social welfare, the transformation of 
consumption per capita into individual utility is non-linear, specifically the utility function is 
concave, because of the assumption that marginal utility is diminishing in rising consumption. In 
estimating the SCC, diminishing marginal utility plays at least two roles: it is a reason for 
discounting the future, if the future is forecast to be richer, and it implies risk aversion, and a 
consequent premium on individual willingness to pay to mitigate climate change, if climate 
change increases the spread on consumption prospects.5 Thus matters are somewhat complicated, 
and changes in the elasticity of the marginal utility of consumption have ambiguous effects in 
principle. 
 
However, what is clear when looking at a set of draws from a Monte Carlo simulation is that 
diminishing marginal utility results in relatively more weight being placed on outcomes (i.e. 
                                               
4 Having said that, even if we only had one model at our disposal, it would be natural to question whether 
the probabilities we have specified resolve all uncertainty, and the answer in the case of physical forecasts of 
future climate (and by implication economic forecasts) would have to be in the negative (Smith 2002). 
Ultimately, then, we cannot be sure how well our Monte Carlo simulations and our inter-model comparisons 
represent uncertainty in the real world. 
5 It is also a reason to place greater weight on climate impacts on poor regions. However, the Working 
Group’s analysis ignored this aspect. 
 7 
draws) in which consumption is low. Indeed, outcomes in which consumption is exceptionally low 
can come to practically dominate the calculus. In the context of integrated assessment models, 
outcomes in which consumption is exceptionally low are caused by catastrophic climate change, 
usually either due to high climate sensitivity and/or a steeply increasing damage function. This is 
why some have drawn the analogy between climate mitigation and insurance (e.g. Weitzman 
2009), since low-probability, high-impact scenarios drive overall willingness to pay for mitigation. 
 
The problem with the Working Group’s analysis is that, by imposing an exogenous, constant 
discount rate, they are very likely to have completely mis-estimated the effect of low-probability, 
catastrophic consumption losses, allied to risk aversion. The exogenous discount rate is calibrated 
on a particular assumption about the future rate of consumption growth, which is usually put in 
the region of 1.5-2%. This is true whether a ‘prescriptive’ approach is used to set the discount rate, 
in which the future rate of consumption growth must be explicitly estimated, or whether a 
‘descriptive’ approach is used, in which case observed market consumption interest rates are used, 
which of course depend on past rates of growth. The problem is that these rates of growth will be 
inconsistent with any scenarios in the Monte Carlo simulation where consumption does not grow 
as fast, or even falls. Moreover the discount rate is very sensitive to changes in consumption 
growth, because of diminishing marginal utility. 
 
To put all of this another way, when there is risk around consumption and risk aversion, the 
expected utility of consumption is less than the utility of expected consumption, and in the 
presence of catastrophic climate change, with only a small probability of occurring, this difference 
can be very large indeed. The Working Group would have been advised not to use discounted 
cash flow methods, but rather to directly estimate social welfare in each draw of the Monte Carlo 
simulation, and then calculate the mean or expectation. In terms of the discount rate, this implies 
that there would have been one discount rate for each and every simulation draw, where the 
discount rate was based on actual consumption growth, allied to assumptions about pure time 
preference and diminishing marginal utility. 
 
There remains the question of how to aggregate across models. It is quite natural to assume that 
the best way to do so is simply to average them, yet doing so requires at least two assumptions to 
be made, which may in practice be rather strong. The first is that the models should be assigned 
equal weight. This is a tricky issue to address, not least because, in forecasting economic outcomes 
 8 
centuries into the future, as a result of a climate system that has not been observed in the past, it is 
very difficult to validate the models. Furthermore, the various integrated assessment models have 
not been developed independently of each other. Faced with such difficulties, the Interagency 
Working Group appears to adhere to the principle of insufficient reason, assigning the models 
equal weight. With some trepidation, I will do likewise. 
 
In outlining the second assumption, note that simple averaging runs counter to most economic 
research on ambiguity, which shows that individuals are averse to ambiguity (as summarised in 
Camerer and Weber 1992). Loosely speaking, this means that they prefer courses of action with 
known probabilities to those with unknown probabilities. While there are competing models of 
ambiguity aversion, what a number of them share is the concept that, in the presence of ambiguity 
aversion, pessimistic models that yield lower estimates of expected utility from a course of action 
demand more of the decision-maker’s attention. In the present context, this means that the 
decision-maker focuses more on models yielding higher estimates of the SCC (see also Millner, 
Dietz and Heal 2010). It is important to stress that this weighting does not stem from a prior belief 
that one model is more likely to be correct in its forecast than another: in fact it will emerge even if 
the models are given equal weight. Rather, the weighting stems from the decision-maker’s 
preferences. The second assumption is then that the decision-maker is ambiguity-neutral. 
 
A formal analysis of the ambiguity-weighted SCC would thus be desirable, although it is clearly a 
considerable undertaking. And, in fact, once one opens up this line of argument, there is no reason 
just to look at differences between the three integrated assessment models: within the models, the 
various uncertain parameters could doubtless all be assigned multiple probability distributions. A 
comprehensive exercise such as this might be considered infeasible at the present time. 
Nevertheless, there is general theoretical support for placing particular focus on the models 
yielding the highest estimates of the SCC. 
 
Target-driven prices 
 
All of this might leave the reader to draw the understandably fatalistic conclusion that estimation 
of the SCC is a fool’s errand. I would not go nearly as far, but there is a sense in which setting a 
price of carbon for use in regulatory impact analysis could be made simpler. 
 9 
 
The key observation here is that uncertainty about the SCC is currently a great deal larger than 
uncertainty about the corresponding marginal abatement cost (MAC) of CO2. Using recent reviews 
of the literature, we concluded that the range of estimates of the present SCC was a factor of ten 
larger than the corresponding range of estimates of the present MAC (Dietz and Fankhauser 2010). 
Since recent research has stretched out the upper tail of SCC estimates, this ratio could now be 
even greater.6 Another observation is that all models, whether of the SCC or MAC or both, are 
imperfect, and few would disagree with the need to look to other forms of evidence in setting the 
stringency of climate policies. 
 
Given these two observations, it is possible to recommend an approach whereby a quantitative 
long-run emissions target is set, and insofar as prices are used to meet that target, they are based 
on the MAC, rather than the SCC (although clearly to avoid circularity the SCC needs to inform 
target setting). Doing so permits greater confidence that the target will be met, while the existence 
of a long-run quantity target in the first place can be supported by reasoning about the efficiency 
of price and quantity instruments under uncertainty (Stern 2007). To expand, the comparative 
efficiency of price and quantity instruments under uncertainty is known to depend in a general 
setting on the relative slopes of the marginal cost and benefit functions (Weitzman 1974). In the 
context of carbon abatement, this insight has generally been used to recommend price instruments 
in the short run, since the short-run marginal benefit function for a long-lived stock pollutant (e.g. 
CO2) is flat, compared with a short-run marginal cost function that is steep (Pizer 1999). However, 
in the long run, with the existence of possibly catastrophic climate change, the opposite seems to 
hold: marginal benefits are steeply increasing upon reaching some stock of CO2, while marginal 
costs are fairly flat. 
 
This target-driven approach to carbon pricing is now being followed in the UK (Department of 
Energy and Climate Change (DECC) 2008), after several years of (mixed) experience in using the 
SCC (beginning with Clarkson and Deyes 2002). The Obama administration has set a target of 
reducing US greenhouse gas emissions by 80% by 2050 (which admittedly is not legally binding, 
unlike the UK’s corresponding 80% target), so the question should be what price of carbon is 
                                               
6 Using Tol (2007), we set the maximum SCC today to $654/tCO2 for our comparison. The maximum SCC in 
2010 in Ackerman and Stanton (2011) is $893/tCO2. Unfortunately how the range of estimates of the MAC 
has evolved over the past few years is not known. 
 10 
required to deliver that target. The more consistent and robust source of that price is currently 
estimates of the MAC. 
<<<
References 
 
Ackerman, F. and E. A. Stanton (2011). Climate Risks and Carbon Prices: Revising the Social Cost 
of Carbon, Economics for Equity and the Environment. 
 
Ackerman, F., E. A. Stanton and R. Bueno (2010). "Fat tails, exponents, and extreme uncertainty: 
simulating catastrophe in DICE." Ecological Economics 69(8): 1657-1665. 
 
Camerer, C. and M. Weber (1992). "Recent development in modeling preferences: uncertainty and 
ambiguity." Journal of Risk and Uncertainty 5(4): 325-370. 
 
Clarkson, R. and K. Deyes (2002). Estimating the social cost of carbon emissions. Government 
Economic Service Working Paper 140. London, HM Treasury. 
 
Department of Energy and Climate Change (DECC) (2008). Carbon Valuation in UK Policy 
Appraisal: a Revised Approach. London, DECC. 
 
Dietz, S. (forthcoming). "High impact, low probability? An empirical analysis of risk in the 
economics of climate change." Climatic Change. 
 
Dietz, S. and S. Fankhauser (2010). "Environmental prices, uncertainty, and learning." Oxford 
Review of Economic Policy 26(2): 270-284. 
 
Hahn, R. W. and P. C. Tetlock (2008). "Has economic analysis improved regulatory decisions?" 
Journal of Economic Perspectives 22(1): 67-84. 
 
Hanemann, M. (2008). What is the economic cost of climate change? Berkeley, CA, University of 
California. 
 
Interagency Working Group on Social Cost of Carbon (2010). Appendix 15A. Social Cost of Carbon 
for Regulatory Impact Analysis under Executive Order 12866. Final Rule Technical Support 
Document (TSD): Energy Efficiency Program for Commercial and Industrial Equipment: Small 
Electric Motors. U.S. Department of Energy. Washington, DC, U.S. Department of Energy. 
 
Knight, F. H. (1921). Risk, Uncertainty and Profit. Boston, Houghton Mifflin. 
 
Millner, A., S. Dietz and G. Heal (2010). Ambiguity and climate policy. NBER Working Paper 
16050. Cambridge, MA, National Bureau of Economic Research. 
 
Nordhaus, W. D. (2008). A Question of Balance: Weighing the Options on Global Warming 
Policies. New Haven and London, Yale University Press. 
 
Nordhaus, W. D. and J. Boyer (2000). Warming the World: Economic Models of Global Warming. 
Cambridge, Mass., MIT Press. 
 11 
 
Sherwood, S. C. and M. Huber (2010). "An adaptability limit to climate change due to heat stress." 
Proceedings of the National Academy of Sciences May 3, 2010. 
 
Smith, L. A. (2002). "What might we learn from climate forecasts." Proceedings of the National 
Academy of Sciences 99: 2487-2492. 
 
Stern, N. (2007). The Economics of Climate Change: The Stern Review. Cambridge, UK, Cambridge 
University Press. 
 
Tol, R. S. J. (2007). The social cost of carbon: trends, outliers and catastrophes. E-conomics 
Discussion Papers 2007-44. 
 
Weitzman, M. L. (2009). "On modeling and interpreting the economics of catastrophic climate 
change." Review of Economics and Statistics 91(1): 1-19. 
 
Weitzman, M. L. (2010). GHG targets as insurance against catastrophic climate damages. 
Cambridge, MA, Department of Economics, Harvard University. 
 
 
 
  
 
 
 
 
Please note: 
You are most sincerely encouraged to participate in the open assessment of this 
discussion paper. You can do so by either recommending the paper or by posting your 
comments. 
 
Please go to: 
http://www.economics-ejournal.org/economics/discussionpapers/2011-30
 
The Editor 
 
 
 
 
© Author(s) 2011. Licensed under a Creative Commons License - Attribution-NonCommercial 2.0 Germany
