International Journal of Electrical and Computer Engineering (IJECE) 
Vol. 7, No. 2, April 2017, pp. 975~980 
ISSN: 2088-8708, DOI: 10.11591/ijece.v7i2.pp975-980      975 
  
Journal homepage: http://iaesjournal.com/online/index.php/IJECE 
5G Fixed Beam Switching on Microstrip Patch Antenna 
 
 
Low Ching Yu, Muhammad Ramlee Kamarudin 
Wireless Communication Centre (WCC), Faculty of Electrical Engineering, Universiti Teknologi Malaysia (UTM), 
Malaysia 
 
 
Article Info  ABSTRACT 
Article history: 
Received Jan 16, 2017 
Revised Mar 18, 2017 
Accepted Mar 30, 2017 

<<<
abstract 
5G technology is using millimeter-wave band to improve the wireless 
communication system.  However, narrow transmitter and receiver beams 
have caused the beam coverage area to be limited. Due to propagation 
limitations of mm wave band, beam forming technology with multi-beam 
based communication system, has been focused to overcome the problem. In 
this letter, a fixed beam switching method is introduced. By changing the 
switches, four different configurations of patch array antennas are designed 
to investigate their performances in terms of radiation patterns, beam forming 
angle, gain, half-power bandwidth and impedance bandwidth at 28 GHz 
operating frequency for 5G application. Mircostrip antenna is preferred due 
to its low profile, easy in feeding and array configurations. Three different 
beam directions had been formed at -15°, 0°, and 15° with half-power 
bandwidth of range 45˚ to 50˚.
>>>

Keyword: 
5G 
Beam forming 
Beam switching 
Microstrip patch antenna 
Copyright © 2017 Institute of Advanced Engineering and Science.  
All rights reserved. 
Corresponding Author: 
Low Ching Yu,  
Wireless Communication Centre (WCC),  
Faculty of Electrical Engineering,  
Universiti Teknologi Malaysia (UTM),  
81310 Skudai, Johor, Malaysia. 
Email: lowchingyu@outlook.my 
 
<<<
introduction
1. INTRODUCTION 
Wireless communication has undergone a tremendous evolution to meet the demand of high traffic 
capacity due to the drastic increasing usage of smartphones and mobile electronic devices. Begin from 0G 
technology since 1970s, followed by 1G,2G, and 3G, until today 4G technology, they show a great 
development on mobile technology from Radio Common Carrier (0G-RCC) to Long Term Evolution 
Advance (4G-LTEA). In order to meet the fast growing wireless data capacity demands due to increasing 
users of smartphones, high growth in web and streaming, 5G technology now are highly given attention and 
undergo a huge research. The 5G technology has huge data capabilities to support multi-Gbps data rates and 
has ability to gather unrestricted call volumes as well as infinite data broadcast within latest mobile 
technology [1]. Therefore millimeter-wave communication systems are required. 
Millimeter-wave communication systems using narrow beams at the transmitter and receiver, which 
suppress the interference of neighboring beams. The narrow beam also strongly reduces the angular spread of 
the incoming waves and the multipath components of millimeter waves to be limited. Therefore, by having 
the beam forming, beam-forming weights can be adjusted to a desired area or location [2]. There are two 
types of beam forming technology. The first one is fixed beam forming and another is adaptive beam  
forming [3]. Adaptive beam forming forms effective beams by adapting beam width and beam direction 
depending on the surrounding state of radio channels. It requires high hardware complexity and needs extra 
feedback mechanism for beam forming. While in fixed beam forming, beams with a fixed direction and 
width are generated. Therefore, switched beam forming is introduced as it is simpler and lesser operation 
system than adaptive beam forming. The concepts of beam steering or beam shifting are developed for 
scanning array antennas. Scanning array antennas were developed initially for aircraft, maritime and 
                ISSN: 2088-8708 
IJECE  Vol. 7, No. 2, April 2017 :  975 – 980 
976 
aeronautical applications. In terms of practical implementation, various technologies such as GPS, radio-
frequency identification (RFID), tracking, traffic control and collision avoidance radars and wireless local 
area network (W-LAN), have been employed in developing the indoor localization and scanning systems [4]. 
Modern wireless communication systems require a low profile, lightweight, high gain and simple 
structure antennas to ensure reliability, mobility, and high efficiency [5]. Therefore, mircostrip antenna is 
highly preferred due to its low profile, easy to fabricate and feed, and easy to use in the array or incorporate 
with other microstrip circuit elements [6].  
Patch antennas are used as simple and highly preferred in many applications. Circular polarizations, 
dual characteristics, dual frequency operation, frequency agility, broad bandwidth, feed line flexibility and 
beam scanning can be easily obtained from these patch antennas [7]. The patch can take any shape with 
rectangular and circular configurations are the most famous. The radiation pattern of a patch array antenna is 
fixed. However, by controlling the progressive phase difference between the elements, the maximum 
radiation can be shifted in any desired direction to form a scanning array [8]. This type of antenna is called a 
phase array antenna.  
In this letter, by controlling the switches, four different configurations of patch array antennas which 
operate at 28 GHz are designed and presented. The antennas are designed with beam shifted at +15˚ and -15˚ 
by consider the overall performances of the antenna in terms of gain, bandwidth, return loss, VSWR and side 
lobe level as well as beam shifted angle. 
 
 
<<<
methods

2. RESEARCH METHOD 
Figure 1(a), (b), (c) and (d) show the proposed antennas with four different configurations to 
compare on their performances. Each antenna basically makes up of three layers. The lower layer is a fully 
ground plane with copper that covers the rectangular shaped substrate. The middle substrate is Rogers 5880 
with dielectric constant, ε_r=2.2 and dielectric loss tangent, tan δ =0.0009, and a height of substrate, h=0. 
254mm. The thickness of the copper used is 0.017mm. The upper layer is the patch antenna. The rectangular 
patch has a size of 4.52 × 3.52mm (width × length). Distance between two patches is almost equivalent to λ/2 
(calculate from center-to-center of the patches). Quarter wave transformer with power division method is 
used for matching patch by a microstrip line with 50Ω input impedance. The operating frequency of all 
designed antennas is about 28 GHz, which is suitable for 5G applications. 
 
 
  
 
(a) 
 
(b) 
 
  
 
(c) 
 
(d) 
 
Figure 1. Four different configurations of patch array antennas designed. (a) Antenna 1, (b) Antenna 2,  
(c) Antenna 3, (d) Antenna 4 
 
IJECE  ISSN: 2088-8708  
 
5G Fixed Beam Switching on Microstrip Patch Antenna (Low Ching Yu) 
977 
Initially the patch antenna with a single radiating element was designed as shown in Figure 2. The 
calculations [8] on the dimensions of a single element of the rectangular patch antenna are shown below by 
Equations (1)-(7). Calculation of the patch width, W: 
 
  
 
   √
    
 
     (1) 
 
Substitute c=3    , fr=28GHz and   =2.2; W=4.24 mm. Calculation of Effective dielectric constant,     : 
 
     
    
 
 
    
 
*    
 
 
+
 
 
  (2) 
 
Substitute, W =4.24 mm and h =0.254; εeff =2.06. Calculation of the Effective length,     : 
 
     
 
   √    
   (3) 
 
Substitute     =2.06;     =3.73 mm. Calculation of the length extension,   : 
 
         
(        )(
 
 
      )
(          )(
 
 
    )
        (4) 
 
Substitute, W=4.24, h=0.254 and εeff=2.06; ΔL=0.13 mm. Calculation of the actual length of antenna, L: 
 
                  (5) 
 
Substitute     =3.73 mm, ΔL=0.13 mm; L= 3.47 mm. Calculation of the insert feed length,   : 
 
   
 
 
{     √
  
  
}        (6) 
 
   √               (7) 
 
where    is width of inset,    is 50 Ω transmission lines,    is characteristic impedance and     is input 
impedance at the edge of the patch antenna. The parameters of the antenna had been optimized so that the 
antenna can perform optimally as shown in Table 1. 
 
 
  
  
Figure 2. Dimension of designed antenna Figure 3. Switches on the designed antenna 
 
                ISSN: 2088-8708 
IJECE  Vol. 7, No. 2, April 2017 :  975 – 980 
978 
Table 1. Optimized parameters of designed antenna 
Parameter Value (mm) 
W 4.52 
L 3.52 
   0.79 
Wh 0.79 
Wf 0.79 
 
 
From the Figure 3, it can be seen that there are four locations of switches. By manipulating the 
switches, four different configurations of patch array antennas were designed as shown in Figure 1. In this 
letter, two radiating elements of patch array antennas are presented. The different configurations of designed 
antennas are comparing their performance in terms of gain, bandwidth and beam shift angle. 
 
 
<<<
results

3. RESULTS AND DISCUSSION 
The patch array antennas operate at 28 GHz is designed and optimized using software CST 
Microwave Studio. Simulated results of S-parameter designed antennas are compared with measured results 
given by the performance network analyzer. Simulated far-field radiation patterns for each different 
configuration of designing antennas are shown as well. 
Figure 4 presents the measured and simulated reflection coefficient results       of the fabricated 
antennas. Two different configurations of antennas designed (b) and (c) have an almost similar curve of       
due to both designed antennas are only opposite in the shape of the patch. The difference in the measured and 
simulated results is mainly caused by the shift in the resonant frequencies [9]. This frequency shift is due to 
fabrication tolerance on the insertion loss of Cu microstrip during etching and gravure processes. The 
reflection coefficients for all designed antennas are about -20 dB with some frequency shifting except for 
Antenna 4 which has the value of -12 dB. This is due to the structure of Antenna 4 causes significant mutual 
coupling between two radiating patches, influencing the return loss of the radiating elements [10]. 
 
 
  
 
(a) 
 
(b) 
  
  
 
(c) 
 
(d) 
  
Figure 4. Measured and simulated return loss       on four different configurations of patch array antennas 
designed, (a) Antenna 1, (b) Antenna 2, (c) Antenna 3, (d) Antenna 4 
IJECE  ISSN: 2088-8708  
 
5G Fixed Beam Switching on Microstrip Patch Antenna (Low Ching Yu) 
979 
Figure 5 shows simulated far-field for four different designed antennas. It can be seen that there is 
no beam shifting in Antenna (a) and Antenna (d) as there is no phase shift difference between two radiating 
patches. However, the main beam of the Antenna (b) and Antenna (c) have shifted by 15˚. This is because the 
two radiating patch elements radiate at different rate due to the existence of phase shift difference. As a 
summary, manipulating the electrical length of the feeders can provide a phase difference between two 
patches of antennas. 
 
 
  
 
(a) 
 
(b) 
  
  
 
(c) 
 
(d) 
  
Figure 5. Simulated far-field cutting on the H-plane, (a) Antenna 1, (b) Antenna 2, (c) Antenna 3,  
(d)  Antenna 4 
 
 
Table 2. Comparison of performances of four different configurations designed antennas 
Antenna Gain/ dB 
Half Power BW 
Angle 
Simulated BW/ 
GHz 
Measured BW/ GHz 
Beam’s Shift 
Angle 
1 10.9 47.5° 0.92 0.96 0˚ 
2 10.4 45.9° 1.61 1.55 -15˚ 
3 10.4 45.9° 1.60 1.30 15˚ 
4 9.54 49.7° 0.51 0.60 0˚ 
 
 
Table 2 shows the summary on the performances of the four units design antennas. Antenna 1 has 
the highest gain of 10.9 dB while Antenna 4 has the lowest gain of 9.54 dB. Antenna 2 and Antenna 3 have 
same gain of 10.4 dB as both structures are just opposite to each other. The simulated bandwidth (BW) and 
measured bandwidth show a good agreement as there is only slightly different in the taken reading. However, 
Antenna 4 has very low bandwidth with a simulated bandwidth of 0.51 dB and measured bandwidth of 0.60 
dB. The low bandwidth is due to the strong mutual coupling between the elements that degrade the overall 
performance of the antenna array [11]. All the designed antennas have wide half power bandwidth of range 
45˚ to 50˚. 
 
<<<
discussion
4. CONCLUSION 
In this letter, the beam switching method with one input port to connect with two radiating elements 
phased arrays antenna were designed, fabricated, and analysed. The beam of the linear antenna array can be 
steered only in one plane. The simulated results showed that both Antenna 2 and 3 provide switched range 
from -15°to 15° in the H-plane, while Antenna 1 and 4 project the main beam at the broadside in the H-plane. 
                ISSN: 2088-8708 
IJECE  Vol. 7, No. 2, April 2017 :  975 – 980 
980 
ACKNOWLEDGEMENTS 
The authors would like to thank the Ministry of Education Malaysia and Universiti Teknologi 
Malaysia for supporting this work under HiCOE grant (4J211) and RUG grants (Vote 11H59 and 03G33). 
 
<<<
REFERENCES
REFERENCES 
[1] R. G. S. Rao and R. Sai, “5G – Introduction & Future of Mobile Broadband Communication Redefined,” 
International Journal of Electronics, Communication & Instrumentation Engineering Research and Development 
(IJECIERD), vol/issue: 3(4), pp. 119–124, 2013. 
[2] Z. Pi and F. Khan, “An introduction to millimeter-wave mobile broadband systems,” IEEE Communications 
Magazine, vol/issue: 49(6), pp. 101–107, 2011. 
[3] J. Bae and Y. S. Choi, “System Capacity Enhancement of MmWave Based 5G Mobile Communication System,” 
2015 International Conference on Information and Communication Technology Convergence (ICTC), pp. 291–294, 
2015. 
[4] E. Engineering, et al., “A Wideband Frequency Scanning Microstrip Antenna Array with Low Profile,” 
Proceedings 2013 International Conference on Mechatronic Sciences, Electric Engineering and Computer (MEC), 
vol/issue: 1(d), pp. 3081–3084, 2013. 
[5] Y. S. H. Khraisat and M. M. Olaimat, “Comparison between rectangular and triangular patch antennas array,” in 
2012 19th International Conference on Telecommunications, ICT 2012, 2012. 
[6] P. Kumar, et al., “Microstrip Patch Antenna for 2.4GHZ Wireless Applications,” International Journal of 
Engineering Trends and Technology, vol/issue: 4(8), pp. 3544–3547, 2013. 
[7] Y. S. H. Khraisat, “Design of 4 elements rectangular microstrip patch antenna with high gain for 2.4 GHz 
applications,” Modern Applied Science, vol/issue: 6(1), pp. 68–74, 2012. 
[8] C. A. Balanis, “Antenna Theory Analysis and Design,” John Wiley & Sons Inc. Publication, 2005. 
[9] C. Slot, “An Integrated Diversity Antenna Based on Dual-Feed,” IEEE Antennas and Wireless Propagation Letters, 
vol. 13, pp. 301–304, 2014. 
[10] D. S. D. Sun, et al., “Application of Novel Cavity-Backed Proximity-Coupled Microstrip Patch Antenna to Design 
Broadband Conformal Phased Array,” IEEE Antennas and Wireless Propagation Letters, vol. 9, pp. 2178876, 
2010. 
[11] P. Upadhyay, et al., “Design of Microstrip Patch Antenna Array for WLAN Application,” vol/issue: 2(1), pp. 
2008–2010, 2012. 
 
 
BIOGRAPHIES OF AUTHORS 
 
 
He received first degree in 2015 from Universiti Teknologi Malaysia in the field of Electrical & 
Electronics Engineering. Currently, he is doing his Master Research in Wireless Communication 
Centre (WCC), Universiti Teknologi Malaysia. His research interest is phased array antenna 
design for 5G applications. 
 
Muhammad Ramlee Kamarudin obtained his first degree from Universiti Teknologi Malaysia 
(UTM), Malaysia, with Honours, majoring in Electrical and Telecommunication Engineering, 
graduating in 2003. He then joined UTM as a lecturer at the Wireless Communication Centre 
(WCC), Faculty of Electrical Engineering,UTM. He has been awarded a scholarship from the 
government of Malaysia to further study in the United Kingdom. He received the MSc in 
communication engineering in 2004 and the PhD in 2007 from University of Birmingham, UK. 
Dr. Ramlee is an author of a book chapter for book entitled “Antennas and Propagation for Body-
Centric Wireless Communications”, an IEEE magazine article, and a number of technical papers 
including journals and conferences on antennas performance and design for on-body 
communication channels and antenna diversity. His research interests include antenna design for 
wireless on-body communications, RF and microwave communication systems, and antenna 
diversity and reconfigurable. Currently, Dr. Ramlee is a member of IEEE, team member of 
Wireless Communication Centre (WCC) and supervising a number of PhD, Master and 
Undergraduate students. 
 
