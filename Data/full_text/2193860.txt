ar
X
iv
:1
10
5.
12
76
v2
  [
co
nd
-m
at.
mt
rl-
sc
i] 
 2 
Ju
l 2
01
1
Structural investigation of LaAlO3 up to 63 GPa
Mael Guennou1, Pierre Bouvier1,2, Gaston Garbarino2, Jens
Kreisel1
1Laboratoire des Mate´riaux et du Ge´nie Physique, CNRS, Grenoble Institute of
Technology, MINATEC, 3 parvis Louis Ne´el, 38016 Grenoble, France
2European Synchrotron Radiation Facility (ESRF), BP 220, 6 Rue Jules Horowitz,
38043 Grenoble Cedex, France
E-mail: mael.guennou@supaero.org, pierre.bouvier@grenoble-inp.fr

<<<
ABSTRACT:

We report a high-pressure single-crystal synchrotron x-ray diffraction on
a LaAlO3 single crystal. The transition from rhombohedral to cubic at 14.8 GPa is
confirmed by the loss of the superstructure reflections, whose intensity shows a linear
pressure dependence, characteristic of a second-order transition. The crystal remains
cubic up to 63 GPa, the highest pressure reached, which provides a confirmation over
a very large pressure range of the general rules for the evolution of distortions of
perovskites under pressure. We give the parameters of Birch-Murnaghan equations
of state in the low- and high-pressure phases and discuss the evolution of the bulk
modulus.

>>>

PACS numbers: 81.40.Vw 07.35.+k 63.70.+h 81.30.Bx
Submitted to: J. Phys.: Condens. Matter

<<<
INTRODUCTION: 

In the past, much progress in the understanding of properties of ferroic ABO3
perovskites, such as ferroelasticity or ferroelectricity, and their related phase
transitions has been achieved through temperature-, or chemical composition-dependent
investigations. The use of pressure has been much rarer and was mostly limited to
pressures below 10 GPa due to experimental difficulties which have now been overcome
for a number of years by the use of diamond-anvil cells. One of the attracting properties
of the external parameter high-pressure is its character of a ”cleaner” variable, since
it acts only on interatomic distances [1]. Furthermore, external pressure leads to
otherwise unachievable reductions of volume and chemical bond lengths. Despite the
now accessible investigation of phase transitions into the very high pressure regime,
experimental investigations of prototype ferroic perovskites above 50 GPa remain still
scarce, with some notable exceptions among ferroelectrics (e.g. KNbO3 [2], PbTiO3 [3]),
relaxor ferroelectrics (PbZn1/3Nb2/3O3 [4]), ferroelastics (SrTiO3 [5]). Explorations of
pressure-temperature or pressure-substitution phase diagrams remain even rarer [6].
Structural investigation of LaAlO3 up to 63 GPa 2
Lanthanum aluminate (LaAlO3 – LAO) is a prototype compound for soft-mode
driven antiferrodistorsive phase transitions. At ambient conditions, LAO crystallizes in
the R3c space group, as a result of the condensation of a soft mode at the R point of the
Brillouin zone boundary. At ambient conditions, the rhombohedral cell has the lattice
parameters aR = 5.357 A˚ and αR = 60.12
◦, or in the hexagonal setting aH = 5.366 A˚ and
cH = 13.109 A˚ [7]. In this structure, the oxygen octahedra are rotated along the [111]C
direction of the parent cubic cell. The rhombohedral distortion can be described by this
single tilt angle. Following early structural studies [8, 9], the temperature-induced phase
transition at 813 K has been studied in great details (e.g. [10] and references within).
On the theoretical side, the parameters of a Landau potential were fitted to available
experimental data [11]. The pressure-induced rhombohedral to cubic transition of LAO
was revealed by a powder Raman spectroscopy and synchrotron diffraction study [12].
It was at that time the first exception to the so-far general rule stating that tilt angles
in antiferrodistorsive perovskites should increase under pressure [13]. This example has
motivated theoretical work to explain this behaviour and formulate new rules and models
to predict qualitatively and quantitavely the evolution of distortions (tilt angles) under
pressure [14, 15, 16, 17]. The evolution of the tilt angles in LAO itself was investigated
by single crystal diffraction under hydrostatic stress up to 8 GPa [7] and non-hydrostatic
stress [18].
However, several aspects of the high-pressure phase transition remain unclear.
From their single-crystal diffraction study, Zhao et al [7] have extrapolated a transition
pressure of 17.6 GPa, significantly higher than the value found by powder diffraction in
[12]. The reason for this difference remains unclear. Moreover, the ab-initio calculations
in [19] have predicted a volume drop at the pressure-induced transition of ∆V = 1 % at
15.4 GPa which was not seen in the powder diffraction study and requires experimental
confirmation. In the light of these previous studies, the purpose of this high-pressure
diffraction experiment was (i) to determine the transition pressure on a single crystal,
(ii) to check the evolution of the volume and the order of the transition and finally (iii)
to extend the structural study and search for further phase transitions up to the so far
unreached 60 GPa pressure range.
>>>

<<<
METHODS: 

LAO single crystals were high-quality [001]C-oriented substrates purchased from
CrysTech GmbH and polished to a thickness of less than 10 µm. The crystal selected
had a lateral extension of 20 µm and exhibited at room conditions micron-sized domains
clearly visible under polarized light. The experiment was performed in diamond-anvil
cells (DAC). The diamonds have the Boehler-Almax design with a cullet of 250 µm. The
pressure chamber was sealed by a rhenium gasket pre-indented to a thickness of about
40 µm. Helium was used as pressure-transmitting medium. A small ruby sphere was
loaded into the cell for pressure measurement using the classical fluorescence method.
The pressure scale used was the equation given by Jacobsen [20]. This equation assumes
Structural investigation of LaAlO3 up to 63 GPa 3
the general form of the classical pressure scale by Mao [21] where the coefficients have
been reevaluated from the equation of state of MgO measured in hydrostatic conditions
(in helium). Although the use of either of these pressure scale is indifferent within the
experimental uncertainties for moderate pressures (say less than 1 % between 0 and 10
GPa), the Mao equation underestimates the pressure by 3 GPa at 60 GPa [20]. In our
experiment, the uncertainty on the pressure is taken as the pressure difference before
and after the collection of diffraction patterns.
The x-ray diffraction experiment was performed on the ID27 beamline at the ESRF.
The beam was monochromatic with a wavelength of 0.3738 A˚ selected by an iodine K-
edge filter and focused to a beam size of about 3 µm. The patterns were collected in
the rotating crystal geometry on a CCD detector with −29◦ ≤ ω ≤ 29◦ in 1◦ steps.
A precise calibration of the detector parameters was performed with a reference LaB6
powder. The diffraction patterns from single crystal measurements were indexed with
a home-made program based on the Fit2D software [22]. The refinement of the lattice
constants from the peak positions (50 to 70 reflections for each run) was performed with
the program UnitCell [23]. The fitting of equations of state was performed with EoSFit
5.2 [24].
>>>

<<<
RESULTS: 

In the following, the reflection indices for the cubic or pseudo-cubic cell (hkl)C are given
for a cell doubled in the three directions with respect to the parent cubic cell.
3.1. Rhombohedral to cubic phase transition
The evolution of the rhombohedral distortion can in principle be followed from the
evolution of the lattice constants aH and cH. As the crystal goes from rhombohedral to
cubic, the ratio cH/(aH
√
6) goes to 1. However, the very small rhombohedral distortion
(∆d/d ≈ 2.10−3 at ambient conditions) was below the resolution of our experiment,
and the peak splittings expected for our polydomain crystal could not be observed.
The comparison of peak widths, e.g. the doublet (640)C → (1 2 10)H + (232)H vs. the
singlet (600)C → (036)H, does show a line narrowing due to the evolution toward a
cubic structure upon increasing pressure (not shown here), but this does not allow the
calculation of the rhombohedral lattice constants, and remains a poor way to determine a
transition pressure. Therefore, we only determined pseudo-cubic lattice constants in the
rhombohedral phase (given in table 1), and turned to the observation of superstructure
reflections for the study of the phase transition.
In distorted perovksites, superstructure reflections arise from tilts of the octahedra
(or antiferroelectric cation shifts). Following Glazer’s notations [25, 26, 27], the tilt
system in LAO is noted a−a−a− and gives rise to superstructure reflections that can
be indexed in the doubled cubic cell with the general form (hkl)C where h, k and l
are odd integers and one index at least is different from the others. Note that for this
Structural investigation of LaAlO3 up to 63 GPa 4
particular tilt system, the presence of a domain structure is indifferent for the study of
the superstructure intensities: all domains give rise to superstructure reflections at the
same spots in reciprocal space, within our experimental resolution. The superstructure
intensity therefore sums up contributions from all the domains present in the crystal
and is not affected by the domain structure, nor a change in domain volume ratio. The
intensity of the superstructure scales like the square of the tilt angle; they remain very
weak, of the order of 1/1000 compared to the intensity of principal Bragg peaks. An
example of a diffraction pattern is given in figure 1 for illustration.
In our diffraction patterns, all expected sets of superstructures (311)C , (331)C,
(531)C etc. were observed. However, only the reflections of the first set were intense
enough to be followed with reasonable accuracy. Their average intensities, normalized
with respect to the intensity of neighbouring Bragg peaks are given in figure 2. The
intensity decreases linearly and cancels out at a critical pressure Pc = 14.8(4) GPa when
extrapolated to higher pressure. This transition pressure is consistent with the previous
powder study [12], as well as the value calculated by ab-initio [19] (15.4 GPa). There
remains however a significant difference with the 17.6 GPa extrapolated from the single
crystal data given in [7]. This is likely due to the uncertainty in the extrapolation to
the transition pressure, their experimental data ranging only up to 8 GPa.
After the transition, the crystal remains cubic up to the highest pressure
investigated (63 GPa). This deserves a comment in the framework of the general rules for
predicting the evolution of distortion of perovksites under pressure [14, 15]. According to
these rules, the change in tilt angles under pressure is determined by the compressibility
ratio of the AO12 and BO6 polyhedra, which are the building blocks of the perovskite
structure. It was found that for most A3+B3+O3, such as rare-earth aluminates, the
tilt angles should decrease under pressure, or, stated in terms of vibrational properties,
that associated phonon modes should soften. This implicitely suggests that a crystal
such as LAO should remain cubic after its transition. However, the validity limit of
this assumption, or whether a new distortion might occur at much higher pressure has
not been verified or discussed. Our experiment shows that the assumption of a cubic
structure remains valid over a pressure range of 40 GPa for a compression V/V (Pc) of
86 %, which is, to the best of our knowledge, the widest compression range over which
this has been verified in perovskites.
3.2. Compressibility and equations of state
We focus first on the equation of state for the cubic phase. We used a third-order
Birch-Murnaghan equation of state, with the transition pressure Pc = 14.8 GPa as a
reference pressure. This calculation yielded K(Pc) = 265.3(2.6) GPa, K
′(Pc) = 3.4(1)
and V (Pc) = 51.163(13) A˚
3 for a weighted χ2 = 0.35. In the rhombohedral phase, a
second-order Birch-Murnaghan equation of state was found to model adequately the
experimental data. Choosing again the transition pressure as a reference, we find
V (Pc) = 51.189(15) A˚
3 and K(Pc) = 255.2(1.4) GPa, K
′(Pc) = 4 being constrained
Structural investigation of LaAlO3 up to 63 GPa 5
by the equation. The differences between measured and calculated pressures remain
within experimental uncertainties for both refinements. In order to compare our data
with previous studies, we have also calculated the parameters V0, K0 and K
′
0 for both
the low-pressure and the high-pressure phases. This can be done either by extrapolating
our equations of state, or making new refinements to the data with 1 bar as a reference
pressure. The two procedures are not equivalent but different tests have shown that
both methods yield in that case similar results given our experimental uncertainties.
We report the outcome of the second method together with data from the litterature in
table 2.
The evolution of the volume predicted by the equation of state in the cubic phase
is given in figure 3 and compared to the experimental values. In the rhombohedral
phase, the experimental volume clearly deviates from the equation of state, reflecting
the presence of a volume strain in the low-symmetry phase, which can be calculated
by ea = V/VEoS − 1, VEoS being the volume calculated from the cubic equation of state
and extrapolated down to ambient pressure, bearing in mind that the experimental
volume is a pseudo-cubic volume only. This pseudo volume strain is plotted in figure 3
(inset). The uncertainty is intrinsically large, as it results from uncertainties in both the
experimental volume and the extrapolation of the equation of state. Still, it shows the
linear pressure dependence expected for a second-order phase transition with a slope of
-3.10−4 GPa−1.
The comparison with previous studies calls for the following comments. First,
we do not confirm the 1 % volume jump at the transition predicted in [19]. Instead,
the smooth evolution of the volume is consistent with the second-order character of
the transition. Second, the jump of the bulk modulus at the transition is calculated as
∆K = 10(4) GPa. This is of the order of magnitude of the value predicted by the Landau
model proposed in [28]. Finally, we want to examine the bulk moduli in more details.
The agreement between the different values of K0 is very good, including the theoretical
calculations in [19] (table 2). However, the values of K ′0 determined from the high-
pressure diffraction experiments differ significantly, which has important implications
for the pressure evolution of K. In figure 4, we show the evolution of the bulk modulus
as predicted by the different models. The two equations of state given in [12, 7] are
seemingly in good agreement. However, one must bear in mind that the equation of
state in ref. [12] was determined from pseudo-cubic volume values whereas in ref. [7], the
equation of state is derived from volume values measured in the rhombohedral phase
only. As a result, the two equations of state cannot be compared, and the apparent
agreement at low pressures must be regarded as coincidental. On the other hand, there is
an important difference between our model and the model determined from the powder
diffraction data in [12], which can most probably be accounted for by the different
pressure-transmitting media used (N2 in [12] and He in this work). The experiment
reported in [7] on the contrary was performed in the usual 4:1 methanol-ethanol mixture
which, like helium, remains liquid in the 0–8 GPa pressure range and as such provides
very good hydrostatic conditions. Their equation of state for the rhomboedral phase
Structural investigation of LaAlO3 up to 63 GPa 6
together with our equation of state for the cubic phase should therefore provide a good
description of the bulk modulus over the full pressure range 0-63 GPa. However, the
inspection of the data shows that it is not the case: due to the unusually large value of
K ′0 = 8.9, the bulk modulus predicted by the equation of state from ref. [7] increases
very rapidly at low pressures and reaches 295 GPa at the transition. This is inconsistent
with our equation of state in the cubic phase, as it would lead to a thermodynamically
problematic negative ∆K at the transition. We have checked that different choices of
ruby pressure scales have only a marginal influence on the values and do not solve this
issue. The explanation thus remains an open question.
>>>

<<<
DISCUSSION:

4. Conclusion
We have described a high-pressure synchrotron single-crystal diffraction on a LaAlO3
single crystal. We have confirmed the transition from rhombohedral to cubic at Pc =
14.8 GPa through the observation of the superstructure intensities, whose linear pressure
dependence confirmed the second-order character of this transition. Remarkably, after
the transition, the cubic structure is stable up to 63 GPa. Last, we have determined
the parameters of a third-order Birch-Murnaghan equation of state for the cubic phase:
K(Pc) = 265.3(2.6) GPa, K
′(Pc) = 3.4(1) and V (Pc) = 51.163(13) A˚
3. However, open
questions regarding the compressibility in the rhombohedral phase remain.
Acknowledgments
We are grateful to the ESRF staff, especially M. Mezouar for allocation of inhouse
beamtime. Support from the French National Research Agency (ANR Blanc PROPER)
is acknowledged.
>>>

<<<
REFERENCES

[1] G. A. Samara. Pressure as a probe of the physics of ABO3 relaxor ferroelectrics. In AIP -
Conference Proceedings, volume 535, page 344, 2000.
[2] Ph. Pruzan, D. Gourdain, and J. C. Chervin. Vibrational dynamics and phase diagram of KNbO3
up to 30 GPa and from 20 to 500 K. Phase Transitions, 80(10):1103–1130, 2007.
[3] P.-E. Janolin, P. Bouvier, J. Kreisel, P. A. Thomas, I. A. Kornev, L. Bellaiche, W. Crichton,
M. Hanfland, and B. Dkhil. High-pressure effect on PbTiO3: An investigation by Raman and
x-ray scattering up to 63 GPa. Phys. Rev. Lett., 101(23):237601, December 2008.
[4] P.-E. Janolin, B. Dkhil, P. Bouvier, J. Kreisel, and P. A. Thomas. Pressure instabilities up to 46
GPa in the relaxor ferroelectric PbZn1/3Nb2/3O3. Phys. Rev. B, 73(9):094128, March 2006.
[5] Mael Guennou, Pierre Bouvier, Jens Kreisel, and Denis Machon. Pressure-temperature phase
diagram of SrTiO3 up to 53 GPa. Phys. Rev. B, 81(5):054115, February 2010.
[6] Jens Kreisel, Beatriz Noheda, and Brahim Dkhil. Phase transitions and ferroelectrics: revival and
the future in the field. Phase Transitions, 82(9):633–661, 2009.
[7] J. Zhao, N. L. Ross, and R. J. Angel. Polyhedral control of the rhombohedral to cubic phase
transition in LaAlO3 perovskite. Journal of Physics: Condensed Matter, 16(47):8763, 2004.
[8] S. Geller and V. B. Bala. Crystallographic studies of perovskite-like compounds. II. Rare earth
aluminates. Acta Crystallographica, 9(12):1019–1025, Dec 1956.
Structural investigation of LaAlO3 up to 63 GPa 7
[9] J. F. Scott. Raman study of trigonal-cubic phase transitions in rare-earth aluminates. Phys. Rev.,
183(3):823, July 1969.
[10] S. A. Hayward, F. D. Morrison, S. A. T. Redfern, E. K. H. Salje, J. F. Scott, K. S. Knight,
S. Tarantino, A. M. Glazer, V. Shuvaeva, P. Daniel, M. Zhang, and M. A. Carpenter.
Transformation processes in LaAlO3: Neutron diffraction, dielectric, thermal, optical, and
Raman studies. Phys. Rev. B, 72(5):054110, August 2005.
[11] Michael A Carpenter, S V Sinogeikin, and J D Bass. Elastic relaxations associated with the
Pm3 m R3 c transition in LaAlO3: II. mechanisms of static and dynamical softening. J. Phys.:
Condens. Matter, 22(3):035404, 2010.
[12] Pierre Bouvier and Jens Kreisel. Pressure-induced phase transition in LaAlO3. J. Phys.: Condens.
Matter, 14(15):3981, 2002.
[13] G. A. Samara, T. Sakudo, and K. Yoshimitsu. Important generalization concerning the role of
competing forces in displacive phase transitions. Phys. Rev. Lett., 35:1767, 1975.
[14] J. Zhao, N. L. Ross, and R. J. Angel. New view of the high-pressure behaviour of GdFeO3-type
perovskites. Acta Crystallographica Section B, 60(3):263–271, Jun 2004.
[15] R. J. Angel, J. Zhao, and N. L. Ross. General rules for predicting phase transitions in perovskites
due to octahedral tilting. Phys. Rev. Lett., 95(2):025503, July 2005.
[16] T. Tohei, A. Kuwabara, T. Yamamoto, F. Oba, and I. Tanaka. General rule for displacive phase
transitions in perovskite compounds revisited by first principles calculations. Phys. Rev. Lett.,
94(3):035502, January 2005.
[17] J. Zhao, N. L. Ross, and R. J. Angel. Estimation of polyhedral compressibilities and structural
evolution of GdFeO3-type perovskites at high pressures. Acta Crystallographica Section B,
62(3):431–439, Jun 2006.
[18] Jing Zhao, Ross J Angel, and Nancy L Ross. The structural variation of rhombohedral LaAlO3
perovskite under non-hydrostatic stress fields in a diamond-anvil cell. Journal of Physics:
Condensed Matter, 23(17):175901, 2011.
[19] Xin Luo and Biao Wang. Structural and elastic properties of LaAlO3 from first-principles
calculations. Journal of Applied Physics, 104(7):073518, 2008.
[20] Steven D. Jacobsen, Christopher M. Holl, Kimberly A. Adams, Rebecca A. Fischer, Emily S.
Martin, Craig R. Bina, Jung-Fu Lin, Vitali B. Prakapenka, Atsushi Kubo, and Przemyslaw
Dera. Compression of single-crystal magnesium oxide to 118 GPa and a ruby pressure gauge
for helium pressure media. American Mineralogist, 93(11-12):1823–1828, 2008.
[21] H. K. Mao, J. Xu, and P. M. Bell. Calibration of the ruby pressure gauge to 800 kbar under
quasi-hydrostatic conditions. J. Geophys. Res., 91(B5):4673–4676, 1986.
[22] A. P. Hammersley, S. O. Svensson, M. Hanfland, A. N. Fitch, and D. Hausermann. Two-
dimensional detector software: From real detector to idealised image or two-theta scan. High
Pressure Research, 14(4):235–248, 1996.
[23] T.J.B. Holland and S.A.T. Redfern. Unit cell refinement from powder diffraction data: the use of
regression diagnostics. Mineral. Mag., 61:65–77, 1997.
[24] Ross J Angel. High-pressure and high-temperature crystal chemistry, volume 41 of MSA Reviews
in Mineralogy and Geochemistry, pages 35–59. Mineralogical Society of America, Washington,
DC, 2000.
[25] A. M. Glazer. The classification of tilted octahedra in perovskites. Acta Crystallogr. B, 28:3384,
1972.
[26] A. M. Glazer. Simple ways of determining perovskite structures. Acta Crystallogr. A, 31:756,
1975.
[27] R. H. Mitchell. Perovskites: Modern and ancient. Almaz Press, Ontario, Canada, 2002.
[28] M. A. Carpenter, S. V. Sinogeikin, J. D. Bass, D. L. Lakshtanov, and S. D. Jacobsen. Elastic
relaxations associated with the Pm3 m R3 c transition in LaAlO3: I. single crystal elastic moduli
at room temperature. J. Phys.: Condens. Matter, 22(3):035403, 2010.
Structural investigation of LaAlO3 up to 63 GPa 8
200
400
220
420
42-2
440
44-2
24-2
240
311
31-1
331
33-1
13-1
-13-1
Figure 1. Extract of a diffraction pattern. The indices are given for the doubled cubic
unit cell. The superstructure reflections (in red), barely visible, are taken here from
an overexposed pattern for clarity.
0 2 4 6 8 10 12 14 16
0
1
2
3
 
 
S
up
er
st
ru
ct
ur
e 
in
te
ns
iti
es
 (a
rb
. u
ni
ts
)
Pressure (GPa)
14.8 GPa
Figure 2. Evolution of normalized superstructure intensities (311)C with increasing
pressure. The values are calculated as the average of the intensities of all observed
reflections and the error bars reflect their dispersion. For the last point at 12 GPa,
only one reflection could be measured.
Structural investigation of LaAlO3 up to 63 GPa 9
Table 1. Lattice parameters as a function of pressure. In the rhombohedral phase,
the pseudo-cubic lattice constant results directly from a fit of the peak positions in the
cubic system.
Rhombohedral phase Cubic phase
P (GPa) apc (A˚) P (GPa) a (A˚)
0.33(1) 3.7938(2) 14.02(13) 3.7164(2)
0.81(5) 3.7905(2) 15.35(10) 3.7099(2)
1.63(10) 3.7851(2) 16.55(13) 3.7043(2)
2.41(10) 3.7803(2) 19.32(18) 3.6913(2)
3.28(15) 3.7753(2) 22.16(4) 3.6796(2)
4.14(9) 3.7705(2) 24.81(14) 3.6686(2)
5.00(13) 3.7655(2) 28.34(10) 3.6546(2)
6.04(12) 3.7594(2) 31.47(13) 3.6423(2)
7.11(15) 3.7534(2) 34.53(12) 3.6310(2)
8.24(14) 3.7470(2) 37.57(1) 3.6204(2)
9.55(11) 3.7400(2) 40.42(10) 3.6099(2)
10.72(17) 3.7335(2) 43.80(12) 3.5984(2)
11.95(12) 3.7270(2) 47.04(11) 3.5877(2)
12.63(16) 3.7234(2) 50.13(9) 3.5779(2)
53.05(10) 3.5690(2)
56.36(2) 3.5589(2)
59.58(9) 3.5498(3)
63.05(20) 3.5401(3)
Table 2. Parameters of the equation of state from this work compared to the values
determined in previous studies. For the ab-initio calculations in [19], values obtained
by the local density approximation (LDA) and the generalized gradient approximation
(GGA) are both given.
Cubic phase Rhombohedral phase
K0 (GPa) K
′
0 K0 (GPa) K
′
0
Ref. [19] LDA 188 196
GGA 199 186
This work XRD 215(4) 3.6(1) 196(2) 4
Ref. [12] XRD 190(5) 7.2(4)
Ref. [7] XRD 177(4) 8.9(1.6)
Ref. [28] Brillouin 196
Structural investigation of LaAlO3 up to 63 GPa 10
0 10 20 30 40 50 60
0.85
0.90
0.95
1.00
1.05
0 2 4 6 8 10 12 14 16
0.1
0.2
0.3
0.4
0.5
 
 
V/
V(
P c
)
Pressure (GPa)
 Pseudo-cubic volume
 Cubic volume
 Equation of state:
         Pc = 14.8 GPa
         K(Pc) = 265.3(2.6) GPa
         K'(Pc) = 3.4(1)
         V(Pc) = 51.163(13) Å
3
 
 V
ol
um
e 
st
ra
in
 (%
)
 
slope = -3.10-4 GPa-1
Figure 3. Evolution of the relative volume V/V (Pc). In the rhombohedral phase
only a pseudo-cubic volume could be determined. The line is a third-order Birch-
Murnaghan equation of state with parameters fitted from the data in the cubic phase.
(Inset) Pseudo-volume strain in the rhombohedral phase calculated as V/VEoS − 1,
where V is the experimental pseudo-cubic volume and VEoS is the volume given by the
extrapolation of the equation of state for the cubic phase in the stability region of the
rhombohedral phase.
0 5 10 15 20 25 30
160
180
200
220
240
260
280
300
320
340
 
 
Bu
lk
 m
od
ul
us
 K
 (G
Pa
)
Pressure (GPa)
 Bouvier and Kreisel (2002)
 Zhao et al. (2004)
 This work
14.8 GPa
Figure 4. Pressure-dependance of the bulk modulus K as predicted by the equations
of state proposed by Bouvier and Kreisel [12], Zhao et al [7] and in this work.
>>>
