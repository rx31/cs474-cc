Politecnico di Torino
Porto Institutional Repository
[Proceeding] From a Robotic Vacuum Cleaner to Robot Companion
Original Citation:
Maria Luce Lupetti;Stefano Rosa;Gabriele Ermacora (2015). From a Robotic Vacuum Cleaner to
Robot Companion. In: HRI’15, the Tenth Annual ACM/IEEE International Conference on Human-
Robot Interaction, Portland, Oregon, USA, March 02-02, 2015. pp. 119-120
Availability:
This version is available at : http://porto.polito.it/2602192/ since: April 2015
Publisher:
IEEE Computer Society
Published version:
DOI:10.1145/2701973.2702004
Terms of use:
This article is made available under terms and conditions applicable to Open Access Policy Article
("Public - All rights reserved") , as described at http://porto.polito.it/terms_and_conditions.
html
Porto, the institutional repository of the Politecnico di Torino, is provided by the University Library
and the IT-Services. The aim is to enable open access to all the world. Please share with us how
this access benefits you. Your story matters.
(Article begins on next page)
From a Robotic Vacuum Cleaner to a Robot Companion: 
Acceptance and Engagement in Domestic Environments 
Maria Luce Lupetti 
DIGEP Politecnico di Torino 
Corso Duca degli Abruzzi 24 
Turin, Italy 
maria.lupetti@polito.it 
Stefano Rosa 
DAUIN Politecnico di Torino 
Corso Duca degli Abruzzi 24 
Turin, Italy 
stefano.rosa@polito.it 
Gabriele Ermacora 
DIMEAS Politecnico di Torino 
Corso Duca degli Abruzzi 24 
Turin, Italy 
gabriele.ermacora@polito.it 
 

<<<
ABSTRACT: 

This paper shows preliminary results of the project DR4GHE 
(Domestic Robot 4 Gaming Health and Eco-sustainability). The 
main purpose is to develop a Robot Companion for domestic 
applications able to advise and suggest good practices to users. 
Interaction and engagement with the user are introduced providing 
to the Robot Vacuum Cleaner RVC an additional intelligence and 
leveraging the existing level of acceptance. Morphological 
aspects, in addition to behavioral traits, assume a key role in the 
perceptual transition of the RVC from object to subject. Human-
robot interaction takes place on two levels: direct interaction, in 
particular with visual and sound signals; and mediated interaction, 
through a GUI for smartphone and tablets. 

>>>

Categories and Subject Descriptors 
H.5.2 [Information Interfaces and Presentation]: User Interfaces---
Prototyping, User-Centered Design, Graphical User Interface 
(GUI); 
General Terms 
Measurement, Design, Experimentation, Security, Human Factors. 
Keywords 
Edutainment; Mobile robots; Human Robot Interaction; Robot 
companion; Sustainability; Situational Awareness. 

<<<
INTRODUCTION: 

Today we are witnessing the diffusion of a large number of small 
robots that started to populate service environments like 
workplaces, public spaces and, especially, homes.	
  Mobile robotics 
in domestic environment is quite diffused and consists primarily 
of cleaning robots, such as robotic vacuum cleaner, mopping 
robots or floor scrubbing robots. These types of robots are, then, 
crucial to understand how people perceive robots [4] and accept 
them in daily life. Recent studies [4] highlight common habits in 
Robotic Vacuum Cleaners owners, e.g. helping children to crawl, 
conversing, entertaining pets, watching for fun and, above all, 
naming the robot [4]. Indeed naming, in other words the act of 
giving a name declares the fact that the robot is perceived as a 
subject instead of an object. This high level of acceptance arises 
from the fact that users have low expectations towards RVC, due 
to the fact that is not perceived as a robot but rather as an 
household appliance [6]. High level of acceptance of these robots 
collides with the small cognitive capabilities. As a matter of fact, 
they are usually, able to move autonomously and avoid obstacles 
but do not have interaction and companioning capabilities. The 
presence of RVC in domestic environments is, anyway, extremely 
important because it, also, paves the way to the acceptance of 
other robots, as it is explained in [4], a survey conducted on 379 
Roomba owners, the presence of RVC in domestic environments 
is the first step towards a wider acceptation of robots in daily life. 
2. THE IDEA 
 
Fig. 1: Synthetic scheme of the HRI implementation 
The intent is to develop a robot companion, able to inform users 
and suggest them good practices for daily life improvement. This 
robot is able to detect environmental information like: humidity, 
temperature, electromagnetic pollution, smoke, CO2, benzene, 
various air pollutants and gas. Due to the potential offered by the 
current and perspective presence of RVC into home environment, 
this robot is developed as a parasite [2] in shape and behavior: it 
is physically attached to (clung) the RVC of which exploit the 
ability of movement. It also take advantages of the high level of 
acceptance that robotic vacuum cleaner already has, enhancing it 
as a robot companion. The human-robot interaction takes place on 
two levels: direct interaction, consisting of visual and sound 
signals; and mediated interaction, through a GUI for smartphone 
or tablet, and a wearable key (beacon) that allows the robot to 
recognize the presence of the user in the house. The robot, 
therefore, assumes the role of a mentor [1]: instead of taking 
actions in the environment, it advises the user, feature that makes 
it highly acceptable by the user [5]. 
 
Permission to make digital or hard copies of part or all of this work for 
personal or classroom use is granted without fee provided that copies 
are not made or distributed for profit or commercial advantage and that 
copies bear this notice and the full citation on the first page. Copyrights 
for third-party components of this work must be honored. For all other 
uses, contact the Owner/Author.  
Copyright is held by the owner/author(s). 
HRI'15 Extended Abstracts, March 2-5, 2015, Portland, OR, USA.  
ACM 978-1-4503-3318-4/15/03. 
http://dx.doi.org/10.1145/2701973.2702004 
119

>>>

<<<
METHOD: 

3. METHOD 
In order to enable transaction from object to subject, namely from 
RVC to robot companion, the research activity focuses on product 
design and GUI development. The product design defines both 
the practical aspect of carrying a parasite robot on a RVC and 
morphological aspects that, in addition to behaviors [3], improve 
acceptance.  The graphical user interface (GUI) is a mediator of 
the human-robot interaction (HRI) and consists, mainly, of 
notifications and visualizations of all the data collected by the 
robot. At the GUI prototyping phase, follows usability tests, the 
GUI is submitted to a group of users in order to identify the level 
of acceptance, appeal, effectiveness and decay of interest over 
time. The feedbacks obtained with the tests are the base for the 
subsequent developments of the GUI. The project includes also a 
preliminary phase that consists of a survey on technology and 
domestic environment, and a subsequent phase of hardware and 
software prototyping. 

>>>

<<<

RESULTS: 

4. PRELIMINARY RESULTS 
4.1 Survey 
The survey is composed by 25 questions and aims to collect data 
on three main issues: knowledge and appeal of robotic vacuum 
cleaner, interest in the quality of home environment and appeal 
and effectiveness of two representative GUI. The preliminary 
results from a sample of 56 people, show that just 3,5% of the 
people owns a RVC but almost 60% (mainly aged between 20 and 
40) would like to have one. Taking into account the data from a 
survey conducted in USA in 2008 [4], where most of Roomba’s 
owner were aged between 18 and 29, is possible to predict that 
there will be a large diffusion of these products among young 
people who now are interested in having one.  Concerning the 
interest in the quality of home environment, the survey provides 
data on how much people are worried about domestic data such as 
humidity, temperature, electromagnetic pollution, air pollutants or 
gas. The survey reveals that 91% of the people are quite or highly 
worried about the quality of home environment and the 82% 
would be willing to install an app (on smartphone or tablet) to 
improve the quality of home environment. In addition, only the 
5,3% is not interested in buying an item for the same purpose, but 
the 53% would buy it depending on the cost. Interestingly, the 
findings of the preliminary survey show that, although, the 
majority of the people expressed a medium-high level of concern 
about the quality of the home environment, the level of awareness 
about the presence of air pollutants in houses is quite low. In fact 
the 42% of people stated that they have no idea about the presence 
or absence of these pollutants in their home and the 23% declared 
that in their house none of these pollutants is present. About the 
two examples of GUI shown in the survey, around the 80% of 
people agree that the first example (from CubeSensors project) is 
more appealing, easier to read and more effective than the second. 
4.2 Hardware and software prototypingv 
The architecture of hardware prototype (fig 2) is composed by: a 
Raspberry PI board, an Asus XTion camera, an air quality sensor, 
a temperature and humidity sensor, an electromagnetic filed 
detector and a Wi-Fi adapter. In our application, in order to 
provide meaningful information about the monitored parameters, 
it is necessary to create a map of the environment and to estimate 
the position of the robot in the map during operation.  Most 
cleaning robots are usually not able to localize themselves. Some 
newer models, like Dyson 360 Eye, also perform localization 
using different sensors. Anyway, these robots do not provide 
external access to that information. Therefore, Localization and 
Mapping (SLAM) framework has been implemented. Only 
distance sensor is used, since access to these sensors is not 
available for most platforms. In order to perform SLAM, the 
gmapping package, available in the Robot Operating System 
(ROS), has been used.  An Asus XTion Pro RGB-D camera has 
been used to simulate a laser range finder. The robot trajectory is 
estimated using a scan matching algorithm. In this way it is 
possible to obtain a map of the environment and the position of 
the robot in the map while it is moving in the environment.  The 
robot continuously creates and updates a planar map of the home 
while it is moving.  As soon as the uncertainty about its own pose 
in the map falls below a certain threshold (that is, the robot is sure 
enough about the map and its own position in it), sensor 
measurements are attached to the relative position of the robot.  
 
>>>
Fig. 2: Architecture of hardware prototype 
 
<<<
REFERENCES:

[1] M. A. Goodrich and A. C. Schultz. 2007. Human-robot 
interaction: a survey. Found. Trends Human-Computer 
Interaction 1, 3 (January 2007), 203-275. 
DOI:http://dx.doi.org/10.1561/1100000005  
[2] S. Marini, 2007. Parasite architecture. Recycling strategies 
for the contemporary city. PhD Thesis, SCHOOL OF 
ADVANCED STUDIES Doctorate course in Conoscenza e 
progetto delle forme dell’insediamento cycle XIX.  
[3] M. L . Walters, D. S. Syrdal, K. Dauthenhahn, R. Boekhorst, 
K. L. Koay, 2008. Avoiding the uncanny valley: robot 
appearance, personality and consistency of behavior in an 
attention-seeking home scenario for a robot companion. 
Autonomous Robots, February 2008, Volume 24, Issue 2, pp 
159-178. DOI: http://dx.doi.org/10.1007/s10514-007-9058-3 
[4] A. Y. Sung, R. E. Grinter, H. I. Christensen, L. Guo, 2008. 
Housewives or technophiles?: understanding domestic robot 
owners. In Proceedings of the 3rd ACM/IEEE international 
conference on Human robot interaction (HRI '08). ACM, 
New York, NY, USA, 129-136. 
DOI:http://doi.acm.org/10.1145/1349822.1349840  
[5] J. Y. Sung, H. I. Christensen, R. E. Grinter, 2009. Sketching 
the Future: Assessing user needs for domestic robots. In 
Proceedings of the 18th IEEE International Symposium on 
Robot 
[6] F. Vaussard, J. Fink, V. Bauwens, P. Rétornaz, D. Hamel, P. 
Dillenbourg, and F. Mondada. 2014. Lessons learned from 
robotic vacuum cleaners entering the home ecosystem. 
Robot. Auton. Syst. 62, 3 (March 2014), 376-391. 
DOI:http://dx.doi.org/10.1016/j.robot.2013.09.014
 
120
>>>
