Knee Arthroscopy Cohort Southern
Denmark (KACS): protocol for
a prospective cohort study
Jonas Bloch Thorlund,1 Robin Christensen,1,2 Nis Nissen,3 Uffe Jørgensen,4
Jeppe Schjerning,5 Jens Christian Pørneki,6 Martin Englund,7,8
L Stefan Lohmander1,4,7
To cite: Thorlund JB,
Christensen R, Nissen N,
et al. Knee Arthroscopy
Cohort Southern Denmark
(KACS): protocol for
a prospective cohort study.
BMJ Open 2013;3:e003399.
doi:10.1136/bmjopen-2013-
003399
▸ Prepublication history and
additional material for this
paper is available online. To
view these files please visit
the journal online
(http://dx.doi.org/10.1136/
bmjopen-2013-003399).
Received 13 June 2013
Revised 22 August 2013
Accepted 30 August 2013
For numbered affiliations see
end of article.
Correspondence to
Dr Jonas Bloch Thorlund;
jthorlund@health.sdu.dk

<<<
ABSTRACT
Background: Meniscus surgery is a high-volume
surgery carried out on 1 million patients annually in the
USA. The procedure is conducted on an outpatient basis
and the patients leave the hospital a few hours after
surgery. A critical oversight of previous studies is their
failure to account for the type of meniscal tears.
Meniscus tears can be categorised as traumatic or non-
traumatic. Traumatic tears (TT) are usually observed in
younger, more active individuals in an otherwise ‘healthy’
meniscus and joint. Non-traumatic tears (NTT) (ie,
degenerative tears) are typically observed in the middle-
aged (35–55 years) and older population but the
aetiology is largely unclear. Knowledge about the
potential difference of the effect of arthroscopic meniscus
surgery on patient symptoms between patients with
traumatic and NTT is sparse. Furthermore, little is known
about the natural time course of patient perceived pain,
function and quality of life after meniscus surgery and
factors affecting these outcomes. The aim of this
prospective cohort study is to investigate the natural time
course of patient-reported outcomes in patients
undergoing meniscus surgery, with particular emphasis
on the role of type of symptom onset.
Methods/design: This prospective cohort study enrol
patients assigned for meniscus surgery. At the baseline
(PRE surgery), patient characteristics are assessed using an
email-based questionnaire also comprising several validated
questionnaires assessing general health, knee-specific
characteristics and patient’s expectations of the surgery.
Follow-up will be conducted at 12 and 52 weeks after
meniscus surgery. The major outcomes will be differences
in changes, from before to 52 weeks after surgery, in each
of the five domains on the Knee injury and Osteoarthritis
Outcome Score (KOOS) between patients undergoing
surgery for traumatic compared with non-traumatic
meniscus tears.
Dissemination: The study findings will be disseminated in
peer-reviewed journals and presented at national and
international conferences.
>>>

Trial registration number: ClinicalTrials.gov Identifier:
NCT01871272.

<<<
INTRODUCTION
Meniscus surgery is a high-volume surgery
carried out on 1 million patients annually in
the USA.1 The procedure is conducted on an
outpatient basis and patients leave the hospital
few hours after surgery. Nevertheless, little is
known about the natural time course of
patient perceived pain, function and quality of
life (QOL) after meniscus surgery and which
factors affect these outcomes.2 The general
opinion is that patients recover their muscle
strength fully within 6–12 weeks following
arthroscopic partial meniscectomy.3–5 More
importantly, however, recent studies have
shown substantial patient-reported disability
and pain in patients up to 4 years after
surgery.6–8 One explanation for the poor self-
reported outcomes may be that the loss of
meniscal function triggers other events that
may cause knee pain.9 Complicating the assess-
ment of surgery effectiveness further, surgical
procedures have shown to be associated with
considerable ‘placebo effect’.10 11
A critical limitation of previous studies12–15 is
their failure to account for the type of
symptom onset (ie, injury mechanism).
Meniscus tears can be categorised as either
traumatic or non-traumatic. Traumatic tears
(TT) are usually observed in younger, active
individuals in an otherwise ‘healthy’ meniscus
and joint, and can be attributed to a speciﬁc
incident (eg, sports-related trauma).16 TT’s
are often associated with joint effusion,
ARTICLE SUMMARY
Strengths and limitations of this study
▪ This cohort study collects data on the natural
time course of patient reported outcomes in a
clinical setting on a large group of patients after
arthroscopic meniscus surgery to ensure high
external validity.
▪ As data is collected on a large number of
patients in a clinical setting it was not feasible to
collect standardised imaging data (ie, MRI or
radiographs) on patients, which could have pro-
vided valuable information.
Thorlund JB, Christensen R, Nissen N, et al. BMJ Open 2013;3:e003399. doi:10.1136/bmjopen-2013-003399 1
Open Access Protocol
reduced knee joint range of motion (ROM) together with
catching/locking of the knee. Non-traumatic tears (NTT)
are typically observed in the middle-aged (35–55 years)
and older population.17 These tears are associated with
meniscal calciﬁcation18 and risk factors for these tears
include, presence of Heberdens’s and Bouchard nodes,
knee malalignment19 and occupational kneeling20;
however, the aetiology is largely unclear.16 NTT’s are often
referred to as degenerative tears and have been shown to
be associated with incipient knee osteoarthritis (OA) in
the middle-aged or elderly population.21–23 Evidence from
four well-designed trials demonstrated that arthroscopic
interventions10 24 and meniscectomy25–27 were no better
or provided no additional effect, than the comparator (ie,
sham surgery, physical therapy or a combination of phys-
ical and medical therapy) to relieve pain and improve
function in the middle-aged patients with knee OA or
early signs of knee OA. No corresponding randomised
trials exist speciﬁcally for TT but an observational study
showed that patients with degenerative meniscus lesions
(ie, NTT) self-report worse function and QOL compared
to individuals with TTat follow-up 14 years after meniscec-
tomy.28 Thus, it is conceivable, but currently unproven,
that arthroscopic meniscus surgery is more effective in
resolving symptoms of a meniscus tear of traumatic aeti-
ology compared with non-NTT in the middle-aged
population.
In patients with TT, repair of the meniscus may be an
alternative to resection. In contrast, repair is rarely an
option for middle-aged patients with NTT due to the
degenerative state of the meniscus. A recent retrospect-
ive observational study suggested a reduced risk of later
knee OA and less activity level loss in patients (∼32 years
at time of surgery) undergoing repair compared with
resection (ie, favouring repair).29 This indicates that
patients with TT should be stratiﬁed into subgroups on
the basis of type of arthroscopic intervention (ie, repair
(TTREP) and resection (TTRES)) since this may inﬂu-
ence the patient-perceived outcomes after surgery.
Aims and hypotheses
The primary aims of this observational cohort are to
1. Investigate if improvements in patient self-reported pain,
symptoms, function and QOL differ after arthroscopic
meniscus surgery for non-traumatic meniscus tears in
middle-aged patients, compared with surgery in patients
with traumatic tears (ie, NTT vs TT). We hypothesise
that in middle-aged patients with NTT arthroscopic
surgery is less effective in relieving self-reported pain,
symptoms, function in sports and recreation (Sport/
Rec) and QOL (ie, change in KOOS scores), compared
with younger patients undergoing surgery for TT.
2. Investigate the effect of meniscus repair (TTREP)
compared to meniscus resection (TTRES) on change
in self-reported pain, symptoms, function in Sport/
Rec and QOL in patients with TT. We hypothesise
that arthroscopic surgery is less effective in relieving
pain, symptoms, function in Sport/Rec and QOL (ie,
change in KOOS scores) in patients undergoing
TTRES compared with those undergoing TTREP.

<<<
METHODS
Design
In this prospective cohort study we will assess patient-
reported outcomes (PROs) using email-based question-
naires prior to surgery and at 12 and 52 weeks follow-up
postsurgery (see ﬁgure 1).
Participants
All patients assigned for arthroscopy on suspicion of a
meniscus tear at Lillebælt Hospital (located in the cities
Vejle and Kolding, Denmark) and Odense University
Hospital, Denmark (incl. Svendborg Hospital) from 1
February 2013 to 31 January 2014.
General cohort eligibility criteria
Inclusion criteria: patients ≥18 years of age assigned for
arthroscopy on suspicion of a medial and/or lateral
meniscus tear by the examining orthopaedic surgeon
based on clinical signs and MRI (if available), having an
email address and able to read and understand Danish.
Exclusion criteria: patients who will or previously have
undergone surgical reconstruction of the anterior or
posterior cruciate ligament (ACL or PCL) in either
knee, experienced fracture(s) to the lower extremities
(ie, hip, leg or foot) in either leg within the last
6 months at time of recruitment and patients not
Figure 1 Overview of collection
of outcomes during the first year
in the Knee Arthroscopy Cohort
Southern Denmark.
2 Thorlund JB, Christensen R, Nissen N, et al. BMJ Open 2013;3:e003399. doi:10.1136/bmjopen-2013-003399
Open Access
mentally able to reply the questionnaire. Please refer to
ﬁgure 2 for an overview of the recruitment ﬂow.
The patients with reconstructed ACL and PCL cannot
be included as these patients are being followed in
another cohort study.
Inclusion and exclusion criteria, aim 1 (NTT vs TT)
There is no consensus on how to classify patients as
having a NTT or TT. In this study, patients undergoing
meniscus surgery will be classiﬁed as having either TT or
NTT according to an algorithm based on age, duration
of knee symptoms and a question about injury mechan-
ism (see below). This represents the information that is
available prior to surgery.
Injury mechanism question
‘How did the knee pain/problems for which you are
now having surgery develop (choose the answer that
best match your situation)?’
Response alternatives
A. The pain/problems have slowly evolved over time.
B. As a result of a speciﬁc incident (ie, kneeling, sliding
and/or twisting of the knee or the like).
C. As a result of a violent incident (ie, during sports, a
crash, collision or the like).
TT
Inclusion: all patients between 18 and 34 years and all
patients between 35 and 55 years replying ‘C’ on the
injury mechanism question.
NTT
Inclusion: all patients between 35 and 55 years replying
‘A’ or ‘B’ on the injury mechanism question and having
knee symptoms >6 months.
In addition, the general eligibility criteria also apply.
For aim 1, the upper age limit is set to include patients
with degenerative meniscus tears (ie, NTT) but without
severe features of knee OA.30 Furthermore, patients
whose responses do not ﬁt the TT and NTT criteria will
also be excluded.
Inclusion and exclusion criteria, aim 2 (TTRES vs TTREP)
All patients classiﬁed as TT according to the speciﬁc
Knee Arthroscopy Cohort Southern Denmark (KACS)
eligibility criteria for aim 1 will be further divided in
patients having either meniscus resection (TTRES) or
Figure 2 Overview of the
recruitment flow in the Knee
Arthroscopy Cohort Southern
Denmark.
Thorlund JB, Christensen R, Nissen N, et al. BMJ Open 2013;3:e003399. doi:10.1136/bmjopen-2013-003399 3
Open Access
repair (TTREP) according to the type of surgery they
receive to answer study aim 2.
Patient characteristics
At baseline, self-report information about: educational
level, employment, civil status, smoking habits,
comorbidities,31 physical activity level32 and self-reported
knee and foot alignment33 will be collected together
with information on height and weight. Surgery docu-
mentation will be collected using a modiﬁed version of
the International Society of Arthroscopy, Knee Surgery
and Orthopaedic Sports Medicine (ISAKOS) classiﬁca-
tion of meniscal tears questionnaire,34 which is ﬁlled out
by the operating surgeon. Additional surgery informa-
tion not pertaining to the meniscus is also collected
from surgery reports.
Major outcomes
Knee injury and Osteoarthritis Outcome Score (KOOS)
All 5 domains (ie, subscales) on the KOOS35 36 at the
1-year follow-up. The ﬁve KOOS domains are pain,
symptoms, function during daily activities (ADL), Sport/
Rec function and QOL. The KOOS score is ranging
from 0 to 100 (0 indicating extreme symptoms and 100
indicating no symptoms). The KOOS score has been
validated and previously used to assess self-reported out-
comes in patients undergoing meniscus
surgery.6 8 25 27 35 36 In addition, it has been shown to
perform well in the entire continuum from very early
changes of knee OA to knee arthroplasty.37 All outcomes
included in the study are listed in table 1.
Minor outcomes
Patient Acceptable Symptom State (PASS) and Treatment
Failure (TF)
One question regarding PASS will be used to assess how
many patients consider themselves well after surgery (as
opposed to feeling better).38 PASS is assessed as a
dichotomous outcome (y/n) to the question:
“Considering your knee function, do you feel that your
current state is satisfactory? With knee function you
should take into account all activities you have during
your daily life, Sport/Rec activities, your level of pain
and other symptoms, and also your knee related QOL”.
In addition, patients replying ‘no’ to the PASS ques-
tion will also be asked to answer (y/n) the following
question: “Would you consider your current state as
being so unsatisfactory that you think the treatment has
failed?” Patients replying, ‘yes’ to the second question
will be deﬁned as experiencing ‘treatment failure’ (TF).
Medical outcomes study 36-item short form health survey
(SF-36)
The SF-36 will be used to assess general physical func-
tion. The SF-36 consists of eight subscales: physical func-
tion, role physical, bodily pain, general health, vitality,
social function, role emotional and mental health. The
SF-36 is self-explanatory, takes 10 min to complete and is
scored from 0 to 100 (0 indicating extreme problems
and 100 indicating no problems). The Acute Danish
version of the SF-36 was used.39 40
Exploratory outcomes
▸ Questions regarding patient’s expectations of
surgery.41
▸ Questions concerning knee joint stability/laxity. One
question regarding the frequency of symptoms and
one question about the inﬂuence of symptoms (ie,
sense of instability during daily activities).
▸ Questions regarding postoperative rehabilitation (ie,
participation, type, frequency and degree of
supervision).
▸ Questions regarding global perceived effect (GPE) to
explore minimal clinical important change in PROs.
GPE is evaluated on a seven-step global rating scale after
surgery (ranging from better, an important improve-
ment; somewhat better, but enough to be an important
improvement; very small change, not enough to be an
important improvement; about the same; very small
change, not enough to be an important worsening;
somewhat worse, but enough to be an important wor-
sening; worse, an important worsening). A two-step
change in GPE is considered clinically important.42
Table 1 Collection of patient characteristics, outcome
measures and explanatory variables
Variable PRE Surgery 12 Weeks 52 Weeks
Height X
Weight X X X
Civil status X
Educational
level
X
Employment X
Smoking X
Comorbidities X
Alignment X X
Physical activity
level
X
ISAKOS
questionnaire
X
Knee joint
stability
X X X
Expectations for
surgery
X
SF-36 X X X
KOOS X X X
PASS X X
TF X X
GPE X X
AE X X
AE, adverse events; GPE, global perceived effect; ISAKOS,
International Society of Arthroscopy, Knee Surgery and
Orthopaedic Sports Medicine—classification of meniscal tears
questionnaire; KOOS, knee injury and osteoarthritis outcome
score; PASS, patient acceptable symptom state; SF-36, medical
outcomes study 36—Item Short Form Health Survey;
TF, treatment failure.
4 Thorlund JB, Christensen R, Nissen N, et al. BMJ Open 2013;3:e003399. doi:10.1136/bmjopen-2013-003399
Open Access
Adverse events
Adverse events (not necessarily implying causality to the
surgery), deﬁned as self-reported symptoms after surgery
causing limitations in daily activities, Sport/Rec activities
or work limitations together with symptoms causing
patients to seek medical care or having re-surgery will be
collected by self-report and patient record review.
Data management
All self-reported data are collected using email-based
questionnaires. The participant-submitted responses are
automatically registered in a secured database. At all
data collection points an email reminder is sent to parti-
cipants if they do not answer the email-based question-
naire within 3–4 days. In addition, participants who do
not reply after the reminder will be called by phone to
ensure a high follow-up rate.
Information registered by surgeons on the modiﬁed
ISAKOS questionnaire following surgery will be trans-
ferred from paper format to electronic format using
automated forms processing. This method is a validated
alternative to double entry of data.43

<<<
RESULTS
The cohort will recruit all eligible patients from 1
February 2013 to 31 January 2014. Conservatively esti-
mated we expect to recruit 450 patients to the KACS
cohort within this time frame. For an overview of the
expected distribution of patients recruited between 18
and 55 years, please refer to ﬁgure 3.
The minimal clinically important change on the
KOOS subscale is considered to be 8–10 points.37 Thus,
with the estimated recruitment ﬂow and distribution
(ﬁgure 3); we will have a power of 0.99 for a two-sample
pooled t test of a normal mean difference with a two-
sided signiﬁcance level of 0.05 (p ≤0.05), assuming a
common SD of 15 KOOS points to detect a mean differ-
ence of 8 KOOS points between NTT and TT (primary
study aim 1).
In addition, we will have a power of 0.88 for a two-
sample pooled t test of a normal mean difference with a
two-sided signiﬁcance level of 0.05 (p ≤0.05), assuming
a common SD of 15 KOOS points to detect a mean dif-
ference of 10 KOOS points between TTRES and TTREP
(study aim 2).
If we are not able to reach sufﬁcient numbers within
the 1 year timeframe, recruitment will continue until the
numbers speciﬁed in the a priori sample size calculation
are reached.
Descriptive results will be given as means with SDs (or
medians with IQR) and as percentages. Between-group
comparisons of the KOOS and SF-36 scores at the 52 weeks
of follow-up will be analysed with the use of ANalysis of
COVAriance (ANCOVA), stratiﬁed by site and adjusted for
the preoperative score level, sex, age and body mass index
(BMI). PASS and TF will be analysed using χ2 test. Multiple
logistic regression will be applied to estimate ORs for
dichotomous outcomes. Mixed linear effects models with
patient as random factor and sex, age and BMI as ﬁxed
factors will be used to explore change over time (ie, base-
line, 12 weeks and 52 weeks of follow-up) in KOOS and
SF-36 scores. Results will be presented with 95% CIs. No
interim analysis will be performed. All reported p values
are two-sided and will not be adjusted for multiple compar-
isons. All data analyses will be carried out according to the
pre-established analysis plan. All descriptive statistics and
tests will be reported in accordance to the recommenda-
tions of the ‘Enhancing the QUAlity and Transparency Of
health Research’ (EQUATOR) network: the STROBE
statement.44
Full analysis set
To qualify for the ‘full analysis set’ recruited patients
must reply to the baseline questionnaire and have the
surgery performed to their meniscus. Please refer to
ﬁgure 4, for an overview of the full analysis set. In case
of missing data a non-responder imputation will be
applied (ie, baseline observation carried forward).
Further for sensitivity, the effect that any missing data
might have on results will be assessed through sensitivity
analyses of augmented data sets.
Planned sensitivity analysis
Sensitivity analysis will be conducted to explore whether
the degree of cartilage defects (score: 0–4), and plica
presence (y/n) have any impact on the outcome after
surgery. Furthermore, we will construct a dichotomous
outcome on whole knee OA (y/n) to explore the effect
of presence of knee OA. Whole knee OA will be deﬁned
as; participants with cartilage defects International
Cartilage Repair Society grade >2 in either of the patel-
lofemoral, medial tibiofemoral or lateral tibiofemoral
compartment excluding participants with TT (according
to previous deﬁnition) and symptoms <6 months. In
Figure 3 Expected distribution
per 450 patients recruited, divided
by age, type of tear and type of
surgery. NTT, non-traumatic tear;
TT, traumatic tear; TTRES,
traumatic tear resected; TTREP,
traumatic tear repaired.
Thorlund JB, Christensen R, Nissen N, et al. BMJ Open 2013;3:e003399. doi:10.1136/bmjopen-2013-003399 5
Open Access
addition, the effect of differences in patient characteris-
tics between groups reported in table 1 with a p value
≤0.10 will be tested in a fully adjusted model.
ETHICS AND DISSEMINATION
The Regional Scientiﬁc Ethics Committee of Southern
Denmark has reviewed the outline of this cohort study.
The committee waived the need for ethical approval as
study is the only pertaining questionnaire and register
data. Such studies can be implemented without permis-
sion from the Ethics Committee according to Danish
legislation (Committee Act § 1, paragraph 1).
The study ﬁndings will be disseminated in peer-
reviewed journals and presented at national and inter-
national conferences.

<<<
DISCUSSION
Arthroscopic meniscus surgery is a high-volume surgery.1
Little is known about the natural time course of PROs
after meniscus surgery and which factors affect these
outcomes. This prospective cohort will collect data from
a large number of patients on the natural time course of
PROs prior and following arthroscopic meniscus surgery.
Our results will enable analysis of the dependence of
postsurgery outcome on the type of meniscus tear (ie,
TT vs NTT in middle-aged patients). Further, it will be
possible to investigate the dependence of postsurgery
outcome on the type of surgery in the subgroup of
patients with TT (ie, TTRES vs TTREP). In contrast, other
on-going randomised placebo controlled trials are inves-
tigating the effect of meniscus surgery for patients with
degenerative tears.45 46
In this study a pragmatic clinical approach was chosen to
categorise meniscus tears as either TT or NTT (ie, degen-
erative). The advantages of this approach are that it is
simple, cheap, can be determined prior to surgery (in con-
trast to histology or arthroscopic observation) and feasible
in a routine clinical setting. Thus, this information can be
used to form an algorithm based on information available
prior to surgery to select those patients who beneﬁt most
from surgery, which can be implemented in clinical prac-
tice. The deﬁnition of TT and NTT are similar but not
identical to what has previously been used in other studies.
Camanho et al12 divided the patients into three groups;
traumatic, degenerative and fatigue. In the present study
the NTT group will include degenerative as well as fatigue
as deﬁned by Camanho et al as the focus of this study is on
the traumatic versus non-traumatic initiation of the menis-
cal tear. Others have based their deﬁnition on sports
participation.47
A limitation to this study is that patients are included
based on the main reason for surgery (ie, suspicion of a
meniscus tear). However, meniscus surgery may also be
performed in relation to surgery for other knee patholo-
gies. Those patients will not be included in the KACS.
This should be taken into account when interpreting
the cohort data. On the other hand, this makes it more
likely that patient symptoms in the KACS cohort are pri-
marily caused by the meniscus injury. Furthermore, we
expect the age to be different in the TT compared with
the NTT groups (ie, NTT group being older), thus all
statistical analysis will be adjusted for age. Nevertheless,
this should still be taken into consideration when inter-
preting the results.
Meniscus surgery may not be the answer to improve
patient-perceived pain and function in all patients with
Figure 4 Overview of the full-analysis set for study aims 1 and 2. NTT, non-traumatic tear; TT, traumatic tear; TTRES, traumatic
tear resected; TTREP, traumatic tear repaired.
6 Thorlund JB, Christensen R, Nissen N, et al. BMJ Open 2013;3:e003399. doi:10.1136/bmjopen-2013-003399
Open Access
meniscus tears. Different factors, such as type of tear,
may affect the postoperative outcome. Ultimately the
goal of this study is to improve management of patients
with meniscus tears through identifying factors asso-
ciated with no or limited effect of surgery on PROs.
Author affiliations
1Department of Sports Science and Clinical Biomechanics, University of
Southern Denmark, Odense, Denmark
2Musculoskeletal Statistics Unit, The Parker Institute, Depart of Rheumatology,
Copenhagen University Hospital, Frederiksberg, Copenhagen F, Denmark
3Department of Orthopedics, Lillebaelt Hospital, Kolding, Denmark
4Department of Orthopedics and Traumatology, Odense University Hospital,
Odense, Denmark
5Department of Orthopedics, Lillebaelt Hospital, Vejle, Denmark
6Department of Orthopedics and Traumatology, Odense University Hospital,
Svendborg, Denmark
7Department of Orthopedics, Clinical Sciences Lund, University of Lund,
Lund, Sweden
8Clinical Epidemiology Research and Training Unit, Boston University School
of Medicine, Boston, Massachusetts, USA
Contributors JBT and LSL conceived the study. All authors participated in the
study design. JBT, RC and LSL drafted the manuscript. All authors
participated in critical scrutinising and revision of the manuscript and
approved the final version.
Funding This study is supported by grants from The Danish Council for
Independent Research | Medical Sciences (#12–125457) and the Region of
Southern Denmark (#12/6334). Musculoskeletal Statistics Unit, The Parker
Institute is supported by grants from the Oak Foundation.
Competing interests None.
Patient consent Obtained.
Provenance and peer review Not commissioned; externally peer reviewed.
Open Access This is an Open Access article distributed in accordance with
the Creative Commons Attribution Non Commercial (CC BY-NC 3.0) license,
which permits others to distribute, remix, adapt, build upon this work non-
commercially, and license their derivative works on different terms, provided
the original work is properly cited and the use is non-commercial. See: http://
creativecommons.org/licenses/by-nc/3.0/

<<<
REFERENCES
1. Cullen KA, Hall MJ, Golosinskiy A. Ambulatory surgery in the United
States, 2006. National Health Statistics Reports No. 11 revised.
Hyattsville, MD: National Center for Health Statistics; 2009.
2. Englund M, Roemer FW, Hayashi D, et al. Meniscus pathology,
osteoarthritis and the treatment controversy. Nat Rev Rheumatol
2012;8:412–19.
3. Matthews P, St-Pierre DM. Recovery of muscle strength following
arthroscopic meniscectomy. J Orthop Sports Phys Ther
1996;23:18–26.
4. Moffet H, Richards CL, Malouin F, et al. Impact of knee extensor
strength deficits on stair ascent performance in patients after medial
meniscectomy. Scand J Rehabil Med 1993;25:63–71.
5. St-Pierre DM, Laforest S, Paradis S, et al. Isokinetic rehabilitation
after arthroscopic meniscectomy. Eur J Appl Physiol Occup Physiol
1992;64:437–43.
6. Ericsson YB, Roos EM, Dahlberg L. Muscle strength, functional
performance, and self-reported outcomes four years after
arthroscopic partial meniscectomy in middle-aged patients. Arthritis
Rheum 2006;55:946–52.
7. Roos EM, Roos HP, Ryd L, et al. Substantial disability 3 months
after arthroscopic partial meniscectomy: a prospective study of
patient-relevant outcomes. Arthroscopy 2000;16:619–26.
8. Thorlund JB, Aagaard P, Roos EM. Thigh muscle strength,
functional capacity, and self-reported function in patients at high risk
of knee osteoarthritis compared with controls. Arthritis Care Res
(Hoboken) 2010;62:1244–51.
9. Englund M, Guermazi A, Roemer FW, et al. Meniscal pathology on
MRI increases the risk for both incident and enlarging subchondral
bone marrow lesions of the knee: the MOST Study. Ann Rheum Dis
2010;69:1796–802.
10. Moseley JB, O’Malley K, Petersen NJ, et al. A controlled trial of
arthroscopic surgery for osteoarthritis of the knee. N Engl J Med
2002;347:81–8.
11. Buchbinder R, Osborne RH, Ebeling PR, et al. A randomized trial of
vertebroplasty for painful osteoporotic vertebral fractures. N Engl J
Med 2009;361:557–68.
12. Camanho GL, Hernandez AJ, Bitar AC, et al. Results of
meniscectomy for treatment of isolated meniscal injuries: correlation
between results and etiology of injury. Clinics 2006;61:133–8.
13. Fabricant PD, Rosenberger PH, Jokl P, et al. Predictors of
short-term recovery differ from those of long-term outcome after
arthroscopic partial meniscectomy. Arthroscopy 2008;24:769–78.
14. Paxton ES, Stock MV, Brophy RH. Meniscal repair versus partial
meniscectomy: a systematic review comparing reoperation rates and
clinical outcomes. Arthroscopy 2011;27:1275–88.
15. Meredith DS, Losina E, Mahomed NN, et al. Factors predicting
functional and radiographic outcomes after arthroscopic partial
meniscectomy: a review of the literature. Arthroscopy
2005;21:211–23.
16. Poehling GG, Ruch DS, Chabon SJ. The landscape of meniscal
injuries. Clin Sports Med 1990;9:539–49.
17. Englund M, Guermazi A, Gale D, et al. Incidental meniscal findings
on knee MRI in middle-aged and elderly persons. N Engl J Med
2008;359:1108–15.
18. Sun Y, Mauerhan DR. Meniscal calcification, pathogenesis and
implications. Curr Opin Rheumatol 2012;24:152–7.
19. Englund M, Felson DT, Guermazi A, et al. Risk factors for medial
meniscal pathology on knee MRI in older US adults: a multicentre
prospective cohort study. Ann Rheum Dis 2011;70:1733–9.
20. Rytter S, Jensen LK, Bonde JP, et al. Occupational kneeling and
meniscal tears: a magnetic resonance imaging study in floor layers.
J Rheumatol 2009;36:1512–19.
21. Englund M. The role of the meniscus in osteoarthritis genesis.
Rheum Dis Clin North Am 2008;34:573–79.
22. Lohmander LS, Englund PM, Dahl LL, et al. The long-term
consequence of anterior cruciate ligament and meniscus injuries:
osteoarthritis. Am J Sports Med 2007;35:1756–69.
23. Englund M, Guermazi A, Roemer FW, et al. Meniscal tear in knees
without surgery and the development of radiographic osteoarthritis
among middle-aged and elderly persons: the Multicenter
Osteoarthritis Study. Arthritis Rheum 2009;60:831–9.
24. Kirkley A, Birmingham TB, Litchfield RB, et al. A randomized trial of
arthroscopic surgery for osteoarthritis of the knee. N Engl J Med
2008;359:1097–107.
25. Herrlin S, Hallander M, Wange P, et al. Arthroscopic or conservative
treatment of degenerative medial meniscal tears: a prospective
randomised trial. Knee Surg Sports Traumatol Arthrosc
2007;15:393–401.
26. Katz JN, Brophy RH, Chaisson CE, et al. Surgery versus physical
therapy for a meniscal tear and osteoarthritis. N Engl J Med
2013;368:1675–84.
27. Herrlin SV, Wange PO, Lapidus G, et al. Is arthroscopic surgery
beneficial in treating non-traumatic, degenerative medial meniscal
tears? A five year follow-up. Knee Surg Sports Traumatol Arthrosc
2013;21:358–64.
28. Englund M, Roos EM, Roos HP, et al. Patient-relevant outcomes
fourteen years after meniscectomy: influence of type of meniscal
tear and size of resection. Rheumatology (Oxford) 2001;40:631–9.
29. Stein T, Mehling AP, Welsch F, et al. Long-term outcome after
arthroscopic meniscal repair versus arthroscopic partial meniscectomy
for traumatic meniscal tears. Am J Sports Med 2010;38:1542–8.
30. Peat G, McCarney R, Croft P. Knee pain and osteoarthritis in older
adults: a review of community burden and current use of primary
health care. Ann Rheum Dis 2001;60:91–7.
31. Sangha O, Stucki G, Liang MH, et al. The Self-Administered
Comorbidity Questionnaire: a new method to assess comorbidity for
clinical and health services research. Arthritis Rheum
2003;49:156–63.
32. Saltin B, Grimby G. Physiological analysis of middle-aged and old
former athletes. Comparison with still active athletes of the same
ages. Circulation 1968;38:1104–15.
33. Ingham SL, Moody A, Abhishek A, et al. Development and validation
of self-reported line drawings for assessment of knee malalignment
and foot rotation: a cross-sectional comparative study. BMC Med
Res Methodol 2010;10:57.
34. Anderson AF, Irrgang JJ, Dunn W, et al. Interobserver reliability of
the International Society of Arthroscopy, Knee Surgery and
Thorlund JB, Christensen R, Nissen N, et al. BMJ Open 2013;3:e003399. doi:10.1136/bmjopen-2013-003399 7
Open Access
Orthopaedic Sports Medicine (ISAKOS) classification of meniscal
tears. Am J Sports Med 2011;39:926–32.
35. Roos EM, Roos HP, Ekdahl C, et al. Knee injury and Osteoarthritis
Outcome Score (KOOS)—validation of a Swedish version. Scand J
Med Sci Sports 1998;8:439–48.
36. Roos EM, Roos HP, Lohmander LS, et al. Knee Injury and
Osteoarthritis Outcome Score (KOOS)—development of a
self-administered outcome measure. J Orthop Sports Phys Ther
1998;28:88–96.
37. Roos EM, Lohmander LS. The Knee injury and Osteoarthritis
Outcome Score (KOOS): from joint injury to osteoarthritis. Health
Qual Life Outcomes 2003;1:64.
38. Tubach F, Ravaud P, Baron G, et al. Evaluation of clinically relevant
states in patient reported outcomes in knee and hip osteoarthritis:
the patient acceptable symptom state. Ann Rheum Dis
2005;64:34–7.
39. Bjorner JB, Damsgaard MT, Watt T, et al. Tests of data quality,
scaling assumptions, and reliability of the Danish SF-36. J Clin
Epidemiol 1998;51:1001–11.
40. Bjorner JB, Thunedborg K, Kristensen TS, et al. The Danish SF-36
Health Survey: translation and preliminary validity studies. J Clin
Epidemiol 1998;51:991–9.
41. Nilsdotter AK, Toksvig-Larsen S, Roos EM. Knee arthroplasty: are
patients’ expectations fulfilled? A prospective study of pain and
function in 102 patients with 5-year follow-up. Acta Orthop
2009;80:55–61.
42. Wells G, Beaton D, Shea B, et al. Minimal clinically important
differences: review of methods. J Rheumatol 2001;28:406–12.
43. Paulsen A, Overgaard S, Lauritsen JM. Quality of data entry using
single entry, double entry and automated forms processing–an
example based on a study of patient-reported outcomes. PLoS ONE
2012;7:e35087.
44. Vandenbroucke JP, von Elm E, Altman DG, et al. Strengthening the
Reporting of Observational Studies in Epidemiology (STROBE):
explanation and elaboration. Ann Intern Med 2007;147:W163–94.
45. Hare KB, Lohmander LS, Christensen R, et al. Arthroscopic partial
meniscectomy in middle-aged patients with mild or no knee
osteoarthritis: a protocol for a double-blind, randomized sham-controlled
multi-centre trial. BMC Musculoskelet Disord 2013;14:71.
46. Sihvonen R, Paavola M, Malmivaara A, et al. Finnish Degenerative
Meniscal Lesion Study (FIDELITY): a protocol for a randomised,
placebo surgery controlled trial on the efficacy of arthroscopic partial
meniscectomy for patients with degenerative meniscus injury with a
novel ‘RCT within-a-cohort’ study design. BMJ Open 2013;3:pii:
e002510.
47. Drosos GI, Pozo JL. The causes and mechanisms of meniscal
injuries in the sporting and non-sporting environment in an
unselected population. Knee 2004;11:143–9.
8 Thorlund JB, Christensen R, Nissen N, et al. BMJ Open 2013;3:e003399. doi:10.1136/bmjopen-2013-003399
Open Access
