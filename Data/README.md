`practice_data.tsv`

Practice data provided by the organizers. Contains 1000 article citations. Note: there are some weird encoding issues in this files, when trying to read it in via pandas use code alonge the lines of:

```
file_name = "../../Data/practice_data.tsv"
df = pd.read_csv(file_name, sep = "\t", encoding= 'unicode_escape')
```

`Glove and TF-IDF Pickle Files`

These files have two columns: Unique ID and the embedding matrices for all 3000 training samples. To get the embeddings for just train, test, or val (based on the train.csv, test.csv, and val.csv files) use the following code:

```
train = pd.read_csv(train_name)
trainindices = train["unique_id"].values
embeddings = pd.read_pickle(pkl_filename)
train_embeddings = embeddings['unique_id'].isin(trainindices)
```