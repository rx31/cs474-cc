# CS474 3C Classification Repository
#### Authors: Yasa Baig, Alex Oesterling, Haoyang Yu, Angikar Ghosal, Rui Xin

This repository contains code for Duke Data Science Team's submission for the 3C: Citation Classification Challenge Shared Task @NAACL2021. The aim is given this dataset of article citations and associated metadata: 
 
1. Label the data as `Not Influential` (0) vs `Influential` (1)

2. Assign a classification for usage in the journal from the following classes:

| Label | Meaning            |
|-------|--------------------|
| 0     | BACKGROUND         |
| 1     | COMPARES_CONTRASTS |
| 2     | EXTENSION          |
| 3     | FUTURE             |
| 4     | MOTIVATION         |
| 5     | USES               |


## Repository Structure

1. Data: Training, testing, and validation datasets saved in one location. 
2. Images: Useful figures, visualizations, and other looks at the dataset
3. Models: Saved .pkl files of trained models after training
4. Notebooks: Folder of jupyter notebooks used for exploration, model fitting, and all other general purpose data analysis
        1. EDA: Notebooks for simple exploratory data analysis of the data, no model fitting.
        2. Modeling: Notebooks for exploring model fitting on the dataset.
5. Utilities: Useful functions and shared classes


## Useful Links

A list of useful links for the project:

#### Competition Information

Main webpage for @NAACL2021 shared task:
https://sdproc.org/2021/sharedtasks.html 

Wiki Call for Papers Link:
http://www.wikicfp.com/cfp/servlet/event.showcfp?eventid=120233&copyownerid=76517

#### Data Sources

Practice Test Data:
https://drive.google.com/file/d/1eWG7_8bafsHqidOJxn2cSYnpDBnS6mfE/view

Supplemenet ACL-ARC Data:
http://jurgens.people.si.umich.edu/citation-function/

Kaggle Link:
TBD

#### Previous Work

1. Baseline Feature Analysis and Literature Review for Binary Classification of Incidential vs Influential:

    http://jurgens.people.si.umich.edu/citation-function/


2. Analysis of Usage of Citations Baseline Model with Features:

    https://www.mitpressjournals.org/doi/abs/10.1162/tacl_a_00028


#### Techniques

1. Scientific Word Vector Embedding Techniques:

    https://github.com/allenai/scibert


## How to Contribute

To contribute code, please create a new branch, push any changes to that branch and then submit a PR. Information on the basics of social coding can be found here:

https://www.youtube.com/watch?v=e3bjQX9jIBk
