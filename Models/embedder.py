import numpy as np
import pandas as pd
from nltk.tokenize import word_tokenize
import nltk

class Embedder:
    def __init__(self, file_name):
        self.tokenize(file_name)

    def tokenize(self, file_name):
        self.df = pd.read_csv(file_name)
        # tokenize data

        tokens = [word_tokenize(context) for context in self.df["citation_context"]]
        for i, token in enumerate(tokens):
            tokens[i] = [word.lower() for word in token]

        self.df["tokens"] = tokens

    def assemble_glove(self, glovepath):
        # assemble glove embeddings

        # glovepath = "glove"
        glove_dict = {}
        with open(glovepath + "/glove.6B.50d.txt", 'r', encoding="utf-8") as f:
            for line in f:
                values = line.split()
                word = values[0]
                vector = np.asarray(values[1:], "float32")
                glove_dict[word] = vector

        self.embed_context(glove_dict, "glove")
        self.pad("glove")
        return self.df

    def assemble_naive(self):
        # assemble naive frequency embeddings

        tokenized_corpus = []
        [tokenized_corpus.extend(token) for token in tokens]
        fdist = nltk.FreqDist(tokenized_corpus)

        sorted_keys = sorted(fdist.items(), key=lambda item: item[1])[::-1]
        sorted_freqs = {k: v for k, v in sorted_keys}

        naive_dict = {w: i + 1 for i, w in enumerate(sorted_freqs)}
        intwords = []

        self.embed_context(naive_dict, "naive")
        self.pad("naive")
        return self.df


    def assemble_tfidfs(self):
        # assemble TF-IDF embeddings

        # TF:
        tfidfs = []

        tokens = self.df["tokens"]
        tokenized_corpus = []
        [tokenized_corpus.extend(set(token)) for token in tokens]
        fdist = nltk.FreqDist(tokenized_corpus)
        numdocs = len(self.df["tokens"])
        idfs = {key: np.log(numdocs / value) for key, value in fdist.items()}

        for sentence in tokens:
            tfidf = {}
            fdist = nltk.FreqDist(sentence)
            length = len(sentence)
            for key, val in fdist.items():
                tfidf[key] = (val / length) * idfs[key]
            tfidfs.append(tfidf)

        self.generate_tfidf(tfidfs, "tfidf")
        self.pad("tfidf")
        return self.df


    def embed_context(self, dictionary, tag):
        embedding = []
        for sentence in self.df["tokens"]:
            temp = []
            for word in sentence:
                if word in dictionary.keys():
                    temp.append(dictionary[word])
            embedding.append(np.array(temp))
        #         embedding.append([dictionary[word] for word in sentence])
        self.df[tag] = embedding

    #     print(embedding)

    def generate_tfidf(self, tfidfs, tag):
        embedding = []
        for index, sentence in enumerate(self.df["tokens"]):
            temp = []
            for word in sentence:
                temp.append(tfidfs[index][word])
            embedding.append(np.array(temp))
        self.df[tag] = embedding

    def pad(self, key):
        length = 138

        padding = []

        for i, sentence in enumerate(self.df[key]):
            if len(sentence) <= length:
                sent = np.array(sentence).reshape(len(sentence), -1)
                zeroes = np.zeros((sent.shape[1], length - len(sentence)))
                sent = np.transpose(sent)
                padded = np.concatenate((zeroes, sent), axis=1)
            elif len(sentence) > length:
                padded = sentence[0:length]
            padding.append(padded)

        self.df["padded_" + key] = padding

    def df(self):
        return self.df