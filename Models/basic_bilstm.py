import torch
import torch.nn as nn
from torch.utils.data import DataLoader, TensorDataset
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt

class citationLSTM(nn.Module):

    def __init__(self, arguments):
        """
        Initialize the model by setting up the layers.
        """
        super().__init__()
        self.n_layers = arguments["layers"]
        self.hidden_dim = arguments["hidden_dim"]
        self.input_size = arguments["input_size"]
        # LSTM
        self.lstm = nn.LSTM(self.input_size, self.hidden_dim, self.n_layers,
                            batch_first=True, bidirectional=True)

        # dropout layer
        #         self.dropout = nn.Dropout(0.3)

        # linear and softmax layers for citation context classification
        self.fc = nn.Linear(self.hidden_dim * 2, arguments["multi_output_size"])
        self.soft = nn.Softmax(dim=2)

        # linear and softma layers for citation influence classification
        self.fc2 = nn.Linear(self.hidden_dim*2, arguments["bi_output_size"])
        self.soft2 = nn.Softmax(dim=2)

    def forward(self, x, hidden):
        """
        Perform a forward pass of our model on some input and hidden state.
        """
        batch_size = x.size(0)

        #pass through LSTM
        lstm_out, hidden = self.lstm(x, hidden)
        

        # linear
        multiout = self.fc(lstm_out)
        
        # softmax function
        multisoft = self.soft(multiout)
        multisoft = multisoft[:, -1, :]  # get last timestep of labels
        
        # linear
        biout = self.fc2(lstm_out)

        # softmax function
        bisoft = self.soft2(biout)
        bisoft = bisoft[:, -1, :]  # get last timestep of labels


        # return last sigmoid output and hidden state
        return multisoft, bisoft, hidden

    def init_hidden(self, batch_size):
        ''' Initializes hidden state '''
        # Create two new tensors with sizes n_layers*2 x batch_size x hidden_dim,
        # initialized to zero, for hidden state and cell state of BiLSTM
        weight = next(self.parameters()).data

        if (torch.cuda.is_available()):
            hidden = (weight.new(self.n_layers * 2, batch_size, self.hidden_dim).zero_().cuda(),
                      weight.new(self.n_layers * 2, batch_size, self.hidden_dim).zero_().cuda())
        else:
            hidden = (weight.new(self.n_layers * 2, batch_size, self.hidden_dim).zero_(),
                      weight.new(self.n_layers * 2, batch_size, self.hidden_dim).zero_())

        return hidden


def train_network(net, traindataframe, valdataframe, arguments):
    '''
    Trains Network for X epochs defined in arguments dictionary
    TODO: Add patience model or other validation scheme.
    :param net: The network being trained
    :param traindataframe: A pandas dataframe with the training data
    :param valdataframe: A pandas dataframe with the validation data
    :param arguments: A dictionary with arguments for specifying how network is trained
    :return: Nothing
    '''
    embedding = arguments["embedding"]

    X_train = np.stack(traindataframe[embedding].values, axis=0).astype(np.float64)
    y_train_multi = np.stack(traindataframe["citation_class_label"].values, axis=0)[:, np.newaxis]
    y_train_bi = np.stack(traindataframe["citation_influence_label"].values, axis=0)[:, np.newaxis]
    y_train = np.concatenate((y_train_multi, y_train_bi), axis=1)
    train = TensorDataset(torch.from_numpy(X_train), torch.from_numpy(y_train))
    train_dataloader = DataLoader(train, shuffle=True, batch_size=arguments["batch_size"], drop_last=False)

    X_val = np.stack(valdataframe[embedding].values, axis=0).astype(np.float64)
    y_val_multi = np.stack(valdataframe["citation_class_label"].values, axis=0)[:, np.newaxis]
    y_val_bi = np.stack(valdataframe["citation_influence_label"].values, axis=0)[:, np.newaxis]
    y_val = np.concatenate((y_val_multi, y_val_bi), axis=1)
    val = TensorDataset(torch.from_numpy(X_val), torch.from_numpy(y_val))
    val_dataloader = DataLoader(val, shuffle=True, batch_size=arguments["batch_size"], drop_last=False)

    lr = arguments["lr"]
    epochs = arguments["epochs"]
    DEVICE = "cuda:0"

    if arguments["weigh_loss"]:
        _, multicounts = np.unique(y_train_multi, return_counts=True)
        _, bicounts = np.unique(y_train_bi, return_counts=True)
        multicounts = [i/len(y_train_multi) for i in multicounts]
        bicounts = [i/len(y_train_bi) for i in bicounts]
        multiweight = torch.tensor(multicounts).type(torch.FloatTensor).to(DEVICE)
        biweight = torch.tensor(bicounts).type(torch.FloatTensor).to(DEVICE)
        multi_criterion = nn.CrossEntropyLoss(weight=multiweight)
        bi_criterion = nn.CrossEntropyLoss(weight=biweight)
    else:
        multi_criterion = nn.CrossEntropyLoss()
        bi_criterion = nn.CrossEntropyLoss()

    optimizer = torch.optim.Adam(net.parameters(), lr=lr)

    if (torch.cuda.is_available()):
        print("GPU Enabled Beep Boop")
        net.to(DEVICE)

    net.train()

    # Train network, printing every 100 passes
    losses = []
    accs = []
    val_losses = []
    val_accs = []
    val_index = []
    for e in range(epochs):
        running_loss = 0.
        running_acc = 0.
        for data, labels in tqdm(train_dataloader):
            h = net.init_hidden(data.shape[0])

            if (torch.cuda.is_available()):
                data = data.to(DEVICE)
                labels = labels.to(DEVICE)
                data = data.type(torch.FloatTensor).to(DEVICE)

            else:
                data = data.type(torch.FloatTensor)

            h = tuple([each.data for each in h])

            #reset gradients
            net.zero_grad()

            #forward pass
            multiout, biout, h = net(data, h)

            #compute loss
            loss1 = multi_criterion(multiout.float(), labels[:, 0].long())
            loss2 = bi_criterion(biout.float(), labels[:, 1].long())
            loss = loss1+loss2

            train_acc = sum(torch.argmax(multiout) == labels)
            running_acc += train_acc

            running_loss += loss.item()*data.size(0)
            loss.backward()
            optimizer.step()
        accs.append((running_acc/len(train)).cpu().detach().numpy())
        losses.append(running_loss/len(train))
        if e%10 == 0:
            running_loss_val = 0.
            running_val_acc = 0.
            for data, labels in val_dataloader:
                val_h = net.init_hidden(data.shape[0])

                # Creating new variables for the hidden state, otherwise
                # we'd backprop through the entire training history
                val_h = tuple([each.data for each in val_h])

                if (torch.cuda.is_available()):
                    data, labels = data.to(DEVICE), labels.to(DEVICE)
                    data = data.type(torch.FloatTensor).to(DEVICE)
                else:
                    data = data.type(torch.FloatTensor)

                multiout, biout, val_h = net(data, val_h)

                # y_pred_multi.extend(multiout.cpu().detach().numpy()[np.newaxis, :])
                # y_pred_bi.extend(biout.cpu().detach().numpy()[np.newaxis, :])

                val_loss1 = multi_criterion(multiout.float(), labels[:, 0].long())
                val_loss2 = bi_criterion(biout.float(), labels[:, 1].long())
                val_loss = val_loss1 + val_loss2
                val_acc = sum(torch.argmax(multiout) == labels)
                running_val_acc += val_acc
                running_loss_val += val_loss.item() * data.size(0)
            val_accs.append((running_val_acc/len(val)).cpu().detach().numpy())
            val_index.append(e)
            val_losses.append(running_loss_val/len(val))
        print("Epoch: {}/{}...".format(e + 1, epochs),
        "Loss: {:.6f}...".format(loss.item()))
    plt.figure()
    plt.plot(np.arange(300), losses, label="train")
    plt.plot(val_index, val_losses, label="val")
    plt.legend()
    plt.title("train and val loss")
    plt.figure()
    plt.plot(np.arange(300), accs, label="train")
    plt.plot(val_index, val_accs, label="val")
    plt.legend()


def evaluate_network(net, valdataframe, arguments):
    embedding = arguments["embedding"]
    X_val = np.stack(valdataframe[embedding].values, axis=0).astype(np.float64)
    y_val_multi = np.stack(valdataframe["citation_class_label"].values, axis=0)[:, np.newaxis]
    y_val_bi = np.stack(valdataframe["citation_influence_label"].values, axis=0)[:, np.newaxis]
    y_val = np.concatenate((y_val_multi, y_val_bi), axis=1)
    val = TensorDataset(torch.from_numpy(X_val), torch.from_numpy(y_val))
    val_dataloader = DataLoader(val, shuffle=True, batch_size=arguments["batch_size"], drop_last=False)

    DEVICE = "cuda:0"
    multi_criterion = nn.CrossEntropyLoss()
    bi_criterion = nn.CrossEntropyLoss()
    val_losses = []

    y_pred_multi = []
    y_pred_bi = []
    net.eval()
    for data, labels in val_dataloader:
        val_h = net.init_hidden(data.shape[0])

        # Creating new variables for the hidden state, otherwise
        # we'd backprop through the entire training history
        val_h = tuple([each.data for each in val_h])

        if (torch.cuda.is_available()):
            data, labels = data.to(DEVICE), labels.to(DEVICE)
            data = data.type(torch.FloatTensor).to(DEVICE)
        else:
            data = data.type(torch.FloatTensor)

        multiout, biout, val_h = net(data, val_h)

        y_pred_multi.extend(multiout.cpu().detach().numpy()[np.newaxis, :])
        y_pred_bi.extend(biout.cpu().detach().numpy()[np.newaxis, :])

        val_loss1 = multi_criterion(multiout.float(), labels[:, 0].long())
        val_loss2 = bi_criterion(biout.float(), labels[:, 1].long())
        val_loss = val_loss1+val_loss2
        val_losses.append(val_loss.item())

    print("Val Loss: {:.6f}".format(np.mean(val_losses)))
    y_pred_multi = np.concatenate(y_pred_multi, axis=0)
    y_pred_bi = np.concatenate(y_pred_bi, axis=0)
    return np.concatenate((y_pred_multi, y_pred_bi), axis=1)