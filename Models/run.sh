#!/bin/bash
#SBATCH -t 24:0:00  # time requested in hour:minute:second
#SBATCH --mem=5G
#SBATCH --gres=gpu:1
#SBATCH --partition=compsci-gpu
#SBATCH --job-name=jupyter

~/.local/bin/jupyter-nbconvert --execute --to notebook run.ipynb

# python3 -m jupyter nbconvert --execute --to notebook run.ipynb