## Introduction

You might find out that your notebook is working in this environment, but when you send it to the cluster, it reports package not installed. I believe this machine have already several packages preinstalled, but it's not so in other machines. This readme will guide you to make notebooks work on the cluster, so that you can take full advantage of school's cluster resource. 

## Install New Python Package

In order to install packages, open a new terminal and execute `srun --pty bash -i`. This will connect you to another machine, allowing you to execute commands in the cluster environment, which is different from this `jupyterlab` environment. 

When your terminal starts with "netid@linux??", you can now install any python package using `pip3 install`.

## Run Jupyter Notebook

Execute `sbatch run.sh` in this folder. Be sure to install at least the notebook before debugging your python code:

- `pip3 install notebook`

Also, in case you might want to use PyTorch, use the following command to install it:

- `pip3 install torch torchvision torchaudio`


## jupyter-nbconvert Permission Error Fix

I will assume that you have installed notebook (`pip3 install notebook`). If you encounter permission error in the log, look at the the file that called `.exist()` in backtrace, and put a try-except block around that `.exist()` call using.
